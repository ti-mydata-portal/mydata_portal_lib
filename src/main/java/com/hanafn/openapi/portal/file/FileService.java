package com.hanafn.openapi.portal.file;

import com.hanafn.openapi.portal.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
@Slf4j
public class FileService {

    @Autowired
    MessageSourceAccessor messageSource;

    @Value("${spring.profiles.active}")
    private String thisServer;

    @Value("${file.upload-dir}")
    private String FILE_DIR;

    @Value("${file.upload-photo-dir}")
    private String PHOTO_FILE_DIR;

    private final Path fileLocation;

    @Autowired
    public FileService(FileUploadProperties prop) {
        this.fileLocation = Paths.get(prop.getUploadDir())
                .toAbsolutePath().normalize();
//        log.debug("★"+fileLocation.toString());
//        try {
//            Files.createDirectories(this.fileLocation);
//        }catch(Exception e) {
//            log.error("FileService 업로드 에러 [ " + this.fileLocation +  " ] error:" + e.toString());
//            throw new FileUploadException(messageSource.getMessage("F001"), e);
//        }
    }

    public String storeFile(MultipartFile file) throws IOException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // 파일명에 부적합 문자가 있는지 확인한다.
            if(fileName.contains(".."))
                throw new FileUploadException("파일명에 부적합 문자가 포함되어 있습니다. " + fileName);

            Path targetLocation = this.fileLocation.resolve(fileName);

            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        }catch(IOException ie) {
            throw new IOException("["+fileName+"] 파일 업로드에 실패하였습니다. 다시 시도하십시오.",ie);
        }
    }

    public String fileSave(MultipartFile multipartfile) throws IOException {

        String path = "";
        if (multipartfile != null && !multipartfile.getOriginalFilename().isEmpty()) {
            String fileName = multipartfile.getOriginalFilename();
            String uuid = UUID.randomUUID().toString();
            String subPath = String.format("%s", uuid);
            File tmpDir = new File(getDir(), subPath);
            if(!tmpDir.isDirectory()) {
                tmpDir.mkdirs();
            }

            File target = new File(tmpDir, fileName);

            if (target.exists()) {
                fileName = System.currentTimeMillis() + "_" + fileName;
                target = new File (FILE_DIR + '\\' + fileName);
            }


            byte[] bytes = multipartfile.getBytes();

            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(target);
                fos.write(bytes);
            } finally {
                if(fos != null) {
                    fos.close();
                }
            }

            path = target.getAbsolutePath();
        }

        return path;
    }
    
    public String filePhotoSave(MultipartFile multipartfile) throws IOException {

        String path = "";
        if (multipartfile != null && !multipartfile.getOriginalFilename().isEmpty()) {
            String fileName = multipartfile.getOriginalFilename();
            String uuid = UUID.randomUUID().toString();
            String subPath = String.format("%s", uuid);
            File tmpDir = new File(getPhotoDir(), subPath);
            if(!tmpDir.isDirectory()) {
                tmpDir.mkdirs();
            }

            File target = new File(tmpDir, fileName);

            if (target.exists()) {
                fileName = System.currentTimeMillis() + "_" + fileName;
                target = new File (PHOTO_FILE_DIR + '\\' + fileName);
            }

            byte[] bytes = multipartfile.getBytes();

            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(target);
                fos.write(bytes);
            } finally {
                if(fos != null) {
                    fos.close();
                }
            }

            path = PHOTO_FILE_DIR + '/' + subPath + '/' + fileName;
        }

        return path;
    }

    // 파일 디렉토리 생성
    public File getDir() {
        File f = new File(FILE_DIR);
        if(!f.isDirectory()) {
            f.mkdirs();
        }
        return f;
    }
    
    // 파일 디렉토리 생성
    public File getPhotoDir() {
        File f = new File(PHOTO_FILE_DIR);
        if(!f.isDirectory()) {
            f.mkdirs();
        }
        return f;
    }


    public Resource loadFileAsResource(String fileName) {
        if(fileName.length() == 0){
            log.error("파일 로딩 에러 " + fileName);
            throw new BusinessException("E090",messageSource.getMessage("E090"));
        }
        
        try {
            Path filePath = this.fileLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());

            if(resource.exists()) {
                log.debug("★resource가 존재합니다.");
                return resource;
            }else {
                log.error("파일 resource 존재 하지않습니다." + filePath.toUri());
                throw new BusinessException("E091",messageSource.getMessage("E091"));
            }
        }catch(MalformedURLException e) {
            log.error(e.toString());
            throw new BusinessException("E091",messageSource.getMessage("E091"));
        }
    }
    
    public File loadFile(String fileName) {
        if(fileName.length() == 0){
            log.error("파일 로딩 에러 " + fileName);
            throw new BusinessException("E090",messageSource.getMessage("E090"));
        }
        String conFilePath = fileName.substring(0,fileName.lastIndexOf("/")+1);
        
        Path filePath = this.fileLocation.resolve(fileName).normalize();
        
        log.info("fileDown-------------------------------------------------------------------String");

        File dir = new File(conFilePath);
        File files[] = dir.listFiles();
        File selFile = null;
        
        if(files[0] != null) {
        	selFile = files[0];
        }
        
        log.info("fileDown-------------------------------------------------------------------end");

        if(selFile != null && selFile.exists()) {
            log.debug("★file 존재합니다.");
            return selFile;
        }else {
            log.error("파일 file 존재 하지않습니다." + filePath.toUri());
            throw new BusinessException("E091",messageSource.getMessage("E091"));
        }
    }
    
    public synchronized int fileDelete(String filePath) throws IOException {

        File file = new File(filePath);

        if(file.exists()) {
            if(file.delete()) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return -1;
        }
    }

    public int dirDelete(String filePath) {
        File f = new File(filePath);
        String dirPath = f.getParent();

        File parentDir = new File(dirPath);

        if(parentDir.isDirectory()) {
            if(parentDir.delete()) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return -1;
        }
    }

    /** 사업자 등록증 다운로드 서비스 **/
    public void useorgUploadDownloadService() {
    }

    /** 다운로드 시 한글 깨짐 방지 처리 **/
    public String setDisposition(String fileName, String browser) throws Exception {
        String dispositionPrefix = "attachment; filename=";
        String encodedFilename = null;

        if (browser.equals("MSIE")) {
            encodedFilename = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
        } else if (browser.equals("Trident")) {       // IE11 문자열 깨짐 방지
            encodedFilename = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
        } else if (browser.equals("Firefox")) {
            encodedFilename = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1") + "\"";
            encodedFilename = URLDecoder.decode(encodedFilename);
        } else if (browser.equals("Opera")) {
            encodedFilename = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1") + "\"";
        } else if (browser.equals("Chrome")) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < fileName.length(); i++) {
                char c = fileName.charAt(i);
                if (c > '~') {
                    sb.append(URLEncoder.encode("" + c, "UTF-8"));
                } else {
                    sb.append(c);
                }
            }
            encodedFilename = sb.toString();
        } else if (browser.equals("Safari")){
            encodedFilename = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1")+ "\"";
            encodedFilename = URLDecoder.decode(encodedFilename);
        }
        else {
            encodedFilename = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1")+ "\"";
        }
        return dispositionPrefix + encodedFilename;
    }

    /** 브라우저 정보 가져오기 **/
    public String getBrowserInfo(HttpServletRequest request) {
        String header = request.getHeader("User-Agent");
        if (header.contains("MSIE")) {
            return "MSIE";
        } else if (header.contains("Trident")) {
            return "Trident";
        } else if (header.contains("Chrome")) {
            return "Chrome";
        } else if (header.contains("Opera")) {
            return "Opera";
        } else if (header.contains("Safari")) {
            return "Safari";
        }
        return "Firefox";
    }

    public String setContentType(String browserInfo) {
        if(browserInfo.equals("Opera")) {
            return "application/octet-stream;charset=UTF-8";
        } else {
            return "application/octet-stream";
        }
    }
    
    public String getFileDir() {
    	return FILE_DIR;
    }
    public String getPhotoFileDir() {
    	return PHOTO_FILE_DIR;
    }
}
