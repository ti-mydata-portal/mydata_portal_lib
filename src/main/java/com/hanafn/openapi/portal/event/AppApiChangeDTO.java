package com.hanafn.openapi.portal.event;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.dto.AdminResponse.AppsResponse;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class AppApiChangeDTO {
    List<ApiVO> newApiList;
    List<ApiVO> oldApiList;
    AppsResponse newAppsRsponse;
}
