package com.hanafn.openapi.portal.event;

import java.util.HashMap;

import org.springframework.http.ResponseEntity;

public interface EventHandlerInterface {
    public abstract void eventCreate(String eventType, Object data, String hfnCd);
    public abstract ResponseEntity<?> handleEvent(HashMap<String,Object> data, String url);
}
