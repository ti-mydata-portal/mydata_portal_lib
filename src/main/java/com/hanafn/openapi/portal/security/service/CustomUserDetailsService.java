package com.hanafn.openapi.portal.security.service;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.vo.UserVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.exception.ResourceNotFoundException;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.model.HfnUser;
import com.hanafn.openapi.portal.security.model.Login;
import com.hanafn.openapi.portal.security.model.Useorg;
import com.hanafn.openapi.portal.security.model.User;
import com.hanafn.openapi.portal.security.repository.HfnUserRepository;
import com.hanafn.openapi.portal.security.repository.LoginRepository;
import com.hanafn.openapi.portal.security.repository.SignupRepository;
import com.hanafn.openapi.portal.security.repository.UseorgRepository;
import com.hanafn.openapi.portal.security.repository.UserRepository;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.DateUtil;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    CommonUtil commonUtil;
    @Autowired
    MessageSourceAccessor messageSource;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UseorgRepository useorgRepository;
    @Autowired
    LoginRepository loginRepository;
    @Autowired
    HfnUserRepository hfnUserRepository;
    @Autowired
    SignupRepository signupRepository;
    @Autowired
    AdminRepository adminRepository;

    private String siteCd;

    private UserPrincipal userPrincipal = new UserPrincipal();

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String userNm) throws UsernameNotFoundException {

        if("adminPortal".equals(this.siteCd) ||
                "pvBatch".equals(this.siteCd) ||
                "backoffice".equals(this.siteCd)) {
            HfnUser hfnUser = hfnUserRepository.findByHfnId(userNm)
                    .orElseThrow(() -> new BusinessException("L002",messageSource.getMessage("L002")));

            if(StringUtils.equals("N", hfnUser.getUserStatCd())){
                log.error("[L004] 사용자 상태코드 확인 [" + hfnUser.getUserStatCd() + "]");
                throw new BusinessException("L001",messageSource.getMessage("L001"));
            }

            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
            String ip = adminRepository.hfnLoginIpCheck(userNm);
            String localIp = commonUtil.getIp(request);

            if(localIp != null){
                if(!commonUtil.ipdupCheck(ip, localIp)) {
                    throw new BusinessException("L011",messageSource.getMessage("L011"));
                }
            }
            // TODO:YYJ ㅈㅜㅅㅓㄱㅎㅐㅈㅔ
//            else {
//                throw new BusinessException("L011",messageSource.getMessage("L011"));
//            }

            userPrincipal = UserPrincipal.create(hfnUser, this.siteCd);

        } else {

            Login login = loginRepository.findByUserIdAndUserStatCd(userNm, "APLV")
                    .orElseThrow(() -> new BusinessException("L002",messageSource.getMessage("L002")));

            if (StringUtils.equals("ORGM", login.getUserType())) {
                Useorg useorg = useorgRepository.findByUseorgId(userNm)
                        .orElseThrow(() -> new BusinessException("L005",messageSource.getMessage("L005")));

                String pwHisDate = adminRepository.getPwHisDate(useorg.getUseorgId());

                userPrincipal = UserPrincipal.create(useorg, pwHisDate);

                if(StringUtils.equals(useorg.getUseorgStatCd(), "WAIT")){

                    UserVO user = adminRepository.selectLoginCertMgnt(userPrincipal.getUserKey());

                    // 유효기간검사
                    if (DateUtil.getDayDiff(DateUtil.getCurrentDate(), user.getExpireDttm()) > 0) {
                        log.error("이용기관 상태코드 확인[" + useorg.getUseorgStatCd() + "]");
                        throw new BusinessException("L006",messageSource.getMessage("L006"));
                    } else {
                        log.error("유효기간 경과 : 이용기관 상태코드 확인[" + useorg.getUseorgStatCd() + "]");
                        user.setExpire(false);
                        user.setUserGb(userPrincipal.getUserKey().startsWith("USEORG") ? "G" : "F");
                        throw new BusinessException("L010",messageSource.getMessage("L010"), user);
                    }

                } else if(StringUtils.equals(useorg.getUseorgStatCd(), "CLOSE")){
                    log.error("[L004] 이용기관 상태코드 확인[" + useorg.getUseorgStatCd() + "]");
                    throw new BusinessException("L001",messageSource.getMessage("L001"));
                }
            } else {
                User user = userRepository.findByUserIdAndUserAplvStatNot(userNm, "REJECT")
                    .orElseThrow(() -> new BusinessException("L002",messageSource.getMessage("L002")));

                String pwHisDate = adminRepository.getPwHisDate(user.getUserId());

                userPrincipal = UserPrincipal.create(user, pwHisDate);

                if(!StringUtils.equals("OK", user.getStatCd()) && !StringUtils.equals("WAIT",user.getStatCd())){
                    log.error("[L004] 사용자 상태코드 확인[" + user.getStatCd() + "]");
                    throw new BusinessException("L001",messageSource.getMessage("L001"));
                } else if(StringUtils.equals("WAIT",user.getStatCd())) {
                    log.error("사용자 상태코드 확인[" + user.getStatCd() + "]");
                    throw new BusinessException("L006",messageSource.getMessage("L006"));
                }
            }
        }

        return userPrincipal;
    }

    @Transactional
    public UserDetails loadUserById(String userKey, String userType) {

        if("Hfn".equals(userType)) {
            HfnUser hfnUser = hfnUserRepository.findById(userKey).orElseThrow(
                    () -> new ResourceNotFoundException("HfnUser", "userKey", userKey)
            );
            userPrincipal = UserPrincipal.create(hfnUser);
        } else if ("ORGM".equals(userType)) {
            Useorg useorg = useorgRepository.findById(userKey).orElseThrow(
                    () -> new ResourceNotFoundException("Useorg", "userKey", userKey)
            );

            String pwHisDate = adminRepository.getPwHisDate(useorg.getUseorgId());

            userPrincipal = UserPrincipal.create(useorg, pwHisDate);
        } else {
            User user = userRepository.findById(userKey).orElseThrow(
                    () -> new ResourceNotFoundException("User", "userKey", userKey)
            );

            String pwHisDate = adminRepository.getPwHisDate(user.getUserId());

            userPrincipal = UserPrincipal.create(user, pwHisDate);
        }
        return userPrincipal;
    }

    public void loadBySiteCd(Authentication authentication) {
        this.siteCd = authentication.getDetails().toString();
    }

}
