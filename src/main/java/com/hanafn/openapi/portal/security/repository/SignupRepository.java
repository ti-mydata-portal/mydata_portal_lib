package com.hanafn.openapi.portal.security.repository;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.SndCertMgntRequest;
import com.hanafn.openapi.portal.admin.views.vo.CertMailVO;
import com.hanafn.openapi.portal.security.dto.SignUpRequest;

@Mapper
public interface SignupRepository {
    /*********** 개인 사용자 등록 관련(동사/목적테이블(모델))***********/
    void insertUser(SignUpRequest.UserSingnUpRequest request);
    void insertLogin(SignUpRequest.UserSingnUpRequest request);
    void insertPwHisUser(SignUpRequest.UserSingnUpRequest request);
    void insertUserRoleForUser(SignUpRequest.UserSingnUpRequest request);

    CertMailVO selectCertUserNm(SignUpRequest.UserSingnUpRequest request);
    int checkDropUserOrNot(SignUpRequest.UserSingnUpRequest request);
    void insertSndCertMgntForSelfAuth(SndCertMgntRequest request);
    void updateSndCertMgntForSelfAuth(SndCertMgntRequest request);
    void updateUserBirthDt(SndCertMgntRequest request);

    String selectUser(SignUpRequest.UserSingnUpRequest request);
    int selectCertDupCnt(SndCertMgntRequest request);

}