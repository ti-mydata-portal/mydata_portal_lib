package com.hanafn.openapi.portal.security.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.SndCertMgntRequest;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.repository.SignupRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/auth")
@Slf4j
@RequiredArgsConstructor
public class NiceIdController {

    private final SignupRepository signupRepository;

    @Value("${spring.profiles.active}")
    private String profile;

    @Autowired
    MessageSourceAccessor messageSource;



    @PostMapping(value="/niceFail", produces = "text/html;charset=UTF-8")
    public String niceFail(HttpServletResponse response, @RequestParam(value = "EncodeData")String request) {
        NiceID.Check.CPClient niceCheck = new NiceID.Check.CPClient();

        String sEncodeData = requestReplace(request, "encodeData");

        String sSiteCode = "BP298";                // NICE로부터 부여받은 사이트 코드
        String sSitePassword = "Q48LngZMgFPn";            // NICE로부터 부여받은 사이트 패스워드

        String sCipherTime = "";            // 복호화한 시간
        String sRequestNumber = "";            // 요청 번호
        String sErrorCode = "";                // 인증 결과코드
        String sAuthType = "";                // 인증 수단
        String sMessage = "";
        String sPlainData = "";

        int iReturn = niceCheck.fnDecode(sSiteCode, sSitePassword, sEncodeData);

        if (iReturn == 0) {
            sPlainData = niceCheck.getPlainData();
            sCipherTime = niceCheck.getCipherDateTime();

            // 데이타를 추출합니다.
            java.util.HashMap mapresult = niceCheck.fnParse(sPlainData);

            sRequestNumber = (String) mapresult.get("REQ_SEQ");
            sErrorCode = (String) mapresult.get("ERR_CODE");
            sAuthType = (String) mapresult.get("AUTH_TYPE");
        } else if (iReturn == -1) {
            sMessage = "복호화 시스템 에러입니다.";
        } else if (iReturn == -4) {
            sMessage = "복호화 처리 오류입니다.";
        } else if (iReturn == -5) {
            sMessage = "복호화 해쉬 오류입니다.";
        } else if (iReturn == -6) {
            sMessage = "복호화 데이터 오류입니다.";
        } else if (iReturn == -9) {
            sMessage = "입력 데이터 오류입니다.";
        } else if (iReturn == -12) {
            sMessage = "사이트 패스워드 오류입니다.";
        } else {
            sMessage = "알 수 없는 에러 입니다. iReturn : " + iReturn;
        }

        SndCertMgntRequest sndCertMgntRequest = new SndCertMgntRequest();
        sndCertMgntRequest.setSendCd("S1");		// 본인인증 코드
        sndCertMgntRequest.setSendNo("SELF AUTH FAIL");
        sndCertMgntRequest.setResultCd(sErrorCode);

        try {
            signupRepository.insertSndCertMgntForSelfAuth(sndCertMgntRequest);
        } catch ( BusinessException e ) {
            log.error("본인인증 실패 이력 DB 삽입 중 에러 => " + sndCertMgntRequest);
            throw new BusinessException("N001", messageSource.getMessage("N001"));
        } catch ( Exception e ) {
            log.error("본인인증 실패 이력 DB 삽입 중 에러 => " + sndCertMgntRequest);
            throw new BusinessException("N001", messageSource.getMessage("N001"));
        }

        return "<html><script>alert('인증에 실패하였습니다');" +
                "window.opener.document.getElementById('niceResult').value=\"false\";" +
                "window.close();" +
                "</script></html>";
    }

    @GetMapping(value="/niceFail", produces = "text/html;charset=UTF-8")
    public String niceFailGet(HttpServletResponse response, @RequestParam(value = "EncodeData")String request) {
        NiceID.Check.CPClient niceCheck = new NiceID.Check.CPClient();

        String sEncodeData = requestReplace(request, "encodeData");

        String sSiteCode = "BP298";                // NICE로부터 부여받은 사이트 코드
        String sSitePassword = "Q48LngZMgFPn";            // NICE로부터 부여받은 사이트 패스워드

        String sCipherTime = "";            // 복호화한 시간
        String sRequestNumber = "";            // 요청 번호
        String sErrorCode = "";                // 인증 결과코드
        String sAuthType = "";                // 인증 수단
        String sMessage = "";
        String sPlainData = "";

        int iReturn = niceCheck.fnDecode(sSiteCode, sSitePassword, sEncodeData);

        if (iReturn == 0) {
            sPlainData = niceCheck.getPlainData();
            sCipherTime = niceCheck.getCipherDateTime();

            // 데이타를 추출합니다.
            java.util.HashMap mapresult = niceCheck.fnParse(sPlainData);

            sRequestNumber = (String) mapresult.get("REQ_SEQ");
            sErrorCode = (String) mapresult.get("ERR_CODE");
            sAuthType = (String) mapresult.get("AUTH_TYPE");
        } else if (iReturn == -1) {
            sMessage = "복호화 시스템 에러입니다.";
        } else if (iReturn == -4) {
            sMessage = "복호화 처리 오류입니다.";
        } else if (iReturn == -5) {
            sMessage = "복호화 해쉬 오류입니다.";
        } else if (iReturn == -6) {
            sMessage = "복호화 데이터 오류입니다.";
        } else if (iReturn == -9) {
            sMessage = "입력 데이터 오류입니다.";
        } else if (iReturn == -12) {
            sMessage = "사이트 패스워드 오류입니다.";
        } else {
            sMessage = "알 수 없는 에러 입니다. iReturn : " + iReturn;
        }

        SndCertMgntRequest sndCertMgntRequest = new SndCertMgntRequest();
        sndCertMgntRequest.setSendCd("S1");		// 본인인증 코드
        sndCertMgntRequest.setSendNo("SELF AUTH FAIL");
        sndCertMgntRequest.setResultCd(sErrorCode);

        try {
            signupRepository.insertSndCertMgntForSelfAuth(sndCertMgntRequest);
        } catch ( BusinessException e ) {
            log.error("본인인증 실패 이력 DB 삽입 중 에러 => " + sndCertMgntRequest);
            throw new BusinessException("N001", messageSource.getMessage("N001"));
        } catch ( Exception e ) {
            log.error("본인인증 실패 이력 DB 삽입 중 에러 => " + sndCertMgntRequest);
            throw new BusinessException("N001", messageSource.getMessage("N001"));
        }

        return "<html><script>alert('인증에 실패하였습니다');" +
                "window.opener.document.getElementById('niceResult').value=\"false\";" +
                "window.close();" +
                "</script></html>";
    }

    public String requestReplace (String paramValue, String gubun) {

        String result = "";

        if (paramValue != null) {
            result = paramValue;

            result = result.replaceAll("<", "&lt;").replaceAll(">", "&gt;");

            result = result.replaceAll("\\*", "");
            result = result.replaceAll("\\?", "");
            result = result.replaceAll("\\[", "");
            result = result.replaceAll("\\{", "");
            result = result.replaceAll("\\(", "");
            result = result.replaceAll("\\)", "");
            result = result.replaceAll("\\^", "");
            result = result.replaceAll("\\$", "");
            result = result.replaceAll("'", "");
            result = result.replaceAll("@", "");
            result = result.replaceAll("%", "");
            result = result.replaceAll(";", "");
            result = result.replaceAll(":", "");
            result = result.replaceAll("-", "");
            result = result.replaceAll("#", "");
            result = result.replaceAll("--", "");
            result = result.replaceAll("-", "");
            result = result.replaceAll(",", "");

            if(gubun != "encodeData"){
                result = result.replaceAll("\\+", "");
                result = result.replaceAll("/", "");
                result = result.replaceAll("=", "");
            }
        }
        return result;
    }

}
