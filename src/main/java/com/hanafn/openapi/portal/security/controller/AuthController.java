package com.hanafn.openapi.portal.security.controller;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.service.AdminService;
import com.hanafn.openapi.portal.admin.views.vo.UserPwHisVO;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AdminLoginRequest;
import com.hanafn.openapi.portal.cmct.RedisCommunicater;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.HfnLoginRequest;
import com.hanafn.openapi.portal.security.dto.LoginRequest;
import com.hanafn.openapi.portal.security.dto.LoginResponse;
import com.hanafn.openapi.portal.security.dto.SignUpRequest;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.security.jwt.JwtTokenProvider;
import com.hanafn.openapi.portal.security.service.CustomUserDetailsService;
import com.hanafn.openapi.portal.security.service.SignupService;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.MailUtils;
import com.hanafn.openapi.portal.util.service.ISecurity;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/auth")
@Slf4j
public class AuthController {

    @Autowired
    MessageSourceAccessor messageSource;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    CommonUtil commonUtil;
    @Autowired
    JwtTokenProvider tokenProvider;
    @Autowired
    SignupService signUpService;
    @Autowired
    AdminService settingService;
    @Autowired
    AdminRepository adminRepository;
    @Autowired
    CustomUserDetailsService userDetailService;
    @Autowired
    PasswordEncoder passwordEncoder;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity aes256Util;
    
    @Autowired
    MailUtils mailUtils;
    
    private Map<String, Object> commlogin(HfnLoginRequest requestLogin) throws Exception {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(requestLogin.getUsername(), requestLogin.getPassword());
        authRequest.setDetails(requestLogin.getSiteCd());

        Authentication authentication = authenticationManager.authenticate(authRequest);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserPrincipal currentUser = (UserPrincipal)authentication.getPrincipal();

        Collection<? extends GrantedAuthority> authorities =  currentUser.getAuthorities();
        Iterator<? extends GrantedAuthority> iter = authorities.iterator();

        while(iter.hasNext()) {
            GrantedAuthority role = iter.next();

            if(StringUtils.equals(requestLogin.getSiteCd(), "adminPortal")){
                if(!(StringUtils.equals(role.getAuthority(),"ROLE_SYS_ADMIN") || StringUtils.equals(role.getAuthority(),"ROLE_HFN_ADMIN") ||
                        StringUtils.equals(role.getAuthority(),"ROLE_ORG_ADMIN"))){
                    log.error("사용자 ID[" + currentUser.getUsername() + "]");
                    throw new BusinessException("L001",messageSource.getMessage("L001"));
                }
            }
        }

        String jwt = tokenProvider.generateToken(authentication);
        // Redis 토큰저장
        RedisCommunicater.setToken(jwt);
        
        result.put("jwt", jwt);
        result.put("currentUser", currentUser);
        
        return result;
    }
    
    @PostMapping("/hfnLoginEureka")
    public ResponseEntity<?> hfnLoginEureka(@RequestBody Map<String,String> params) throws Exception{
    	HfnLoginRequest requestLogin = new HfnLoginRequest();
    	
    	requestLogin.setHfnId(params.get("hfnId"));
    	requestLogin.setPassword(params.get("password"));
    	requestLogin.setSiteCd(params.get("siteCd"));
    	requestLogin.setUsername(params.get("username"));
    	
    	Map<String, Object> loginInfo = this.commlogin(requestLogin);
    	
    	String jwt = (String)loginInfo.get("jwt");
    	
    	Map<String, Object> data = new HashMap<String, Object>();
    	
    	data.put("accessToken", jwt);
    	data.put("tokenType", "Bearer");
    	
    	return ResponseEntity.ok(data);
    }

    @PostMapping("/hfnLogin")
    public ResponseEntity<?> authenticateHfnUser(@Valid @RequestBody HfnLoginRequest requestLogin) throws Exception {

    	Map<String, Object> loginInfo = this.commlogin(requestLogin);
    	
    	String jwt = (String)loginInfo.get("jwt");
    	UserPrincipal currentUser = (UserPrincipal)loginInfo.get("currentUser");
    	
        LoginResponse data= new LoginResponse(jwt);
        data.setPortalTosYn(currentUser.getPortalTosYn());
        data.setPrivacyTosYn(currentUser.getPrivacyTosYn());
        data.setTmpPasswordYn(currentUser.getTmpPasswordYn());

        return ResponseEntity.ok(data);
    }

    @PostMapping("/adminLogin")
    public ResponseEntity<?> authenticateAdminUser(@RequestBody AdminLoginRequest requestLogin) {
        requestLogin.setPassword("_BLANK"); //backoffice 요청거래는 password를 사용하지 않음.
        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(requestLogin.getUsername(), requestLogin.getPassword());
        authRequest.setDetails(requestLogin.getSiteCd());

        Authentication authentication = authenticationManager.authenticate(authRequest);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserPrincipal currentUser = (UserPrincipal)authentication.getPrincipal();

        Collection<? extends GrantedAuthority> authorities =  currentUser.getAuthorities();
        Iterator<? extends GrantedAuthority> iter = authorities.iterator();

        while(iter.hasNext()) {
            GrantedAuthority role = iter.next();

            if(StringUtils.equals(requestLogin.getSiteCd(), "adminPortal") ||
                    StringUtils.equals(requestLogin.getSiteCd(), "pvBatch") ||
                    StringUtils.equals(requestLogin.getSiteCd(), "backoffice")){
                if(!(StringUtils.equals(role.getAuthority(),"ROLE_SYS_ADMIN") ||
                        StringUtils.equals(role.getAuthority(),"ROLE_HFN_ADMIN") ||
                        StringUtils.equals(role.getAuthority(),"ROLE_HFN_USER") ||
                        StringUtils.equals(role.getAuthority(),"ROLE_HFN_SALES"))){
                    log.error("사용자 ID[" + currentUser.getUsername() + "]");
                    throw new BusinessException("L001",messageSource.getMessage("L001"));
                }
            }
        }

        String jwt = tokenProvider.generateToken(authentication);
        LoginResponse data= new LoginResponse(jwt);
        data.setPortalTosYn(currentUser.getPortalTosYn());
        data.setPrivacyTosYn(currentUser.getPrivacyTosYn());
        data.setTmpPasswordYn(currentUser.getTmpPasswordYn());
        data.setUserKey(currentUser.getUseorgKey());
        data.setPwdChangeDt(currentUser.getPwdChangeDt());
        data.setUserType(currentUser.getUserType());

        // Redis 토큰저장
        RedisCommunicater.setToken(jwt);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/userLogin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest requestLogin) {


        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(requestLogin.getUsername(), requestLogin.getPassword());
        authRequest.setDetails(requestLogin.getSiteCd());

        Authentication authentication = authenticationManager.authenticate(authRequest);

        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserPrincipal currentUser = (UserPrincipal)authentication.getPrincipal();

        if (StringUtils.equals(currentUser.getUserType(), "USER")
                || StringUtils.equals(currentUser.getUserType(), "ORGD")) {
            // 복호화
            try {
                // 이름
                if (currentUser.getUsername() != null && !"".equals(currentUser.getUsername())) {
                    String decryptedUseorgUserNm = aes256Util.decrypt(currentUser.getUsername());
                    currentUser.setUsername(decryptedUseorgUserNm);
                    log.debug("복호화 - 이름: {}", decryptedUseorgUserNm);
                }
            } catch (UnsupportedEncodingException e) {
                log.error("클라이언트ID 디크립트 에러");
            } catch ( Exception e ) {
                log.error("클라이언트ID 디크립트 에러");
            }
        }

        Collection<? extends GrantedAuthority> authorities =  currentUser.getAuthorities();
        Iterator<? extends GrantedAuthority> iter = authorities.iterator();

        while(iter.hasNext()) {
            GrantedAuthority role = iter.next();

            if(StringUtils.equals(requestLogin.getSiteCd(), "userPortal")){
                if(!(StringUtils.equals(role.getAuthority(),"ROLE_PERSONAL") || StringUtils.equals(role.getAuthority(),"ROLE_ORG_ADMIN") || StringUtils.equals(role.getAuthority(),"ROLE_ORG_USER"))){
                    log.error("사용자 ID[" + currentUser.getUsername() + "]");
                    throw new BusinessException("L001",messageSource.getMessage("L001"));
                }
            }
        }

        String jwt = tokenProvider.generateToken(authentication);
        LoginResponse data= new LoginResponse(jwt);
        data.setPortalTosYn(currentUser.getPortalTosYn());
        data.setPrivacyTosYn(currentUser.getPrivacyTosYn());
        data.setUserType(currentUser.getUserType());
        data.setPwdChangeDt(currentUser.getPwdChangeDt());

        // Redis 토큰저장
        RedisCommunicater.setToken(jwt);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/hfnLogout")
    public ResponseEntity<SignUpResponse> hfnLogout(@CurrentUser UserPrincipal currentUser, HttpServletRequest request, HttpServletResponse response) {
        return ResponseEntity.ok(new SignUpResponse(true, "User registered successfully"));
    }

    @PostMapping("/logoutUser")
    public ResponseEntity<SignUpResponse> logoutUser() {
        return ResponseEntity.ok(new SignUpResponse(true, "User registered successfully"));
    }

    @PostMapping("/userIdDupCheck")
    public ResponseEntity<?> userIdDupCheck(@Valid @RequestBody AdminRequest.UserDupCheckRequest request) {
        int data = signUpService.userIdDupCheck(request);

        return ResponseEntity.ok(data);
    }

//    TODO NICE XX
//    @PostMapping("/hfnGroupCode")
//    public ResponseEntity<?> hfnGropCode() {
//
//        HfnInfoRsponse data = new HfnInfoRsponse();
//        List<HfnInfoVO> list = settingRepository.selectHfnCdList();
//        data.setList(list);
//
//        return ResponseEntity.ok(data);
//    }

    @PostMapping("/checkPwd")
    public ResponseEntity<?> checkPwd(@CurrentUser UserPrincipal currentUser, @RequestBody String userPwd){
        boolean result = commonUtil.compareWithShaStrings(userPwd, currentUser.getPassword());
        return ResponseEntity.ok(result);
    }

    /*********** 개인사용자 회원가입 *************/
    @PostMapping("/signUpUser")
    public ResponseEntity<SignUpResponse> signUpUser(@Valid @RequestBody SignUpRequest.UserSingnUpRequest request) throws Exception {
        request.setRegUser(request.getUserNm());
        request.setUserId(StringUtils.lowerCase(request.getUserId()));

        signUpService.signupUserDupCheck(request);
        signUpService.insertUser(request);

        return ResponseEntity.ok(new SignUpResponse(true, "User registered successfully"));
    }

    /***********  개인회원 비밀번호변경 *************/
    @PostMapping("/newUserPwd")
    public ResponseEntity<?> setUserPwd(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AdminRequest.UserPwdUpdateRequest request) {

        if (currentUser != null) {
            if(!currentUser.getUserId().equals(request.getUserId())) {
                log.error("비밀번호 변경 id 조작 가능성 감지");
                throw new BusinessException("E026","잘못된 요청입니다.");
            }
        }

        // 비밀번호 검증
        List<UserPwHisVO> userList = adminRepository.getUserIdPw(request.getUserId());

        if (userList != null) {
            for (UserPwHisVO userData : userList) {
                if(!commonUtil.compareWithShaStringsPw(request.getUserPwd(), userData.getUserPwd())) {
                    log.error("비밀번호 검증에러 [불일치]");
                    throw new BusinessException("E112", messageSource.getMessage("E112"));
                }
            }

            settingService.setUserPwd(request);
            return ResponseEntity.ok(new SignUpResponse(true, "User_Pwd_Update successfully"));
        } else {
            log.error(messageSource.getMessage("L005"));
            throw new BusinessException("L005", messageSource.getMessage("L005"));
        }
    }

    /***********  관리자 비밀번호변경 *************/
    @PostMapping("/hfnUserPwdUpdate")
    public ResponseEntity<?> hfnUserUpdate(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AdminRequest.HfnUserPwdUpdateRequest request) {

        if(!currentUser.getUserId().equals(request.getHfnId())) {
            log.error("비밀번호 변경 id 조작 가능성 감지");
            throw new BusinessException("잘못된 요청입니다.");
        }

        // 19.11.04 추가 - 자신의 비밀번호만 바꿀 수 있는 로직이므로 강제로 세팅합니다.
        request.setHfnId(currentUser.getUserId());
        request.setUserKey(currentUser.getUserKey());

        // 비밀번호 검증
        List<UserPwHisVO> userList = adminRepository.getHfnIdPw(request);

        for (UserPwHisVO userData : userList) {
            if(!commonUtil.compareWithShaStringsPw(request.getUserPwd(), userData.getUserPwd())) {
                log.error("hfnUserPwdUpdate 비밀번호 검증 에러 [불일치]");
                throw new BusinessException("E112",messageSource.getMessage("E112"));
            }
        }

        if (request.getUserPwd() != null && !StringUtils.equals(request.getUserPwd(), "")) {
            settingService.passwordCheck(request.getUserPwd());
            request.setUserPwd(passwordEncoder.encode(request.getUserPwd()));
        }

        settingService.updateHfnUserPwd(request);

        return ResponseEntity.ok(new SignUpResponse(true, "Hfn User Update successfully"));
    }

    /***********  토큰 시간 갱신 *************/
    @PostMapping("/tokenTimeContinue")
    public ResponseEntity<?> tokenTimeContinue(@Valid @RequestBody LoginRequest.tokenRefesh requestLogin) {
        return ResponseEntity.ok("");
    }
}
