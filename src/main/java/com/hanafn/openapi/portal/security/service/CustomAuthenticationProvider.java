package com.hanafn.openapi.portal.security.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.vo.UserVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.exception.ServerException;
import com.hanafn.openapi.portal.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private final MessageSourceAccessor messageSource;
    private final CustomUserDetailsService customUserDetailsService;
    private final AdminRepository adminRepository;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String username = authentication.getName();
		String password = (String) authentication.getCredentials();
		Collection<? extends GrantedAuthority> authorities;

        if(StringUtils.equals(authentication.getDetails().toString(), "adminPortal") ||
                StringUtils.equals(authentication.getDetails().toString(), "backoffice") ||
                StringUtils.equals(authentication.getDetails().toString(), "pvBatch"))
        {
            AdminRequest.HfnLoginLockCheckRequest hfnLoginLockCheckRequest = new AdminRequest.HfnLoginLockCheckRequest();
            //hfnLoginLockCheckRequest.setHfnCd(username.substring(0,2));
            hfnLoginLockCheckRequest.setHfnId(username);
            //String loginLockYn = settingRepository.hfnLoginLockYn(hfnLoginLockCheckRequest);

            // 2020.07.17
            String loginLockYn = adminRepository.hfnLoginCheck(hfnLoginLockCheckRequest);

            // 유효한 id 여부 체크
            if(loginLockYn == null) {
                log.error(messageSource.getMessage("L002"));
                throw new ServerException("L002",messageSource.getMessage("L002"));
            }

            // UserVO user = settingRepository.getAdminAuth(hfnLoginLockCheckRequest);

            // 로그인 잠금 여부 체크
            if(StringUtils.equals(loginLockYn, "N")){
                try{
                    customUserDetailsService.loadBySiteCd(authentication);
                    UserDetails userDetails = customUserDetailsService.loadUserByUsername(username);

                    //특정 채널은 비밀번호 검증하지 않음.
                    if (StringUtils.equals(authentication.getDetails().toString(), "backoffice") ||
                            StringUtils.equals(authentication.getDetails().toString(), "pvBatch")) {
                        log.info("channel==>" + authentication.getDetails());
                    } else{
                        if (!passwordEncoder().matches(password, userDetails.getPassword())) { // 로그인 실패 시

                            // 로그인 실패 체크
                            int cnt = adminRepository.hfnLoginFailCntCheck(hfnLoginLockCheckRequest);
                            if (cnt < 4) { // 로그인 실패 횟수가 4회 이하면 로그인 실패 횟수 증가
                                hfnLoginLockCheckRequest.setLoginFailCnt(cnt + 1);
                                adminRepository.hfnLoginFailCntSet(hfnLoginLockCheckRequest);
                            } else if (cnt == 4) { // 로그인 실패 횟수가 5이면 해당 계정 로그인 잠금
                                hfnLoginLockCheckRequest.setLoginFailCnt(cnt + 1);
                                adminRepository.hfnLoginFailCntSet(hfnLoginLockCheckRequest);
                                adminRepository.hfnLoginLockChange(hfnLoginLockCheckRequest);
                            }

                            log.error(messageSource.getMessage("L002"));
                            throw new ServerException("L002",messageSource.getMessage("L002"));
                        }
                    }

                    // 로그인 성공시 로그인잠금 관련 세팅 초기화
                    //앞서 사용자정보를 조회했으므로 조회한 값을 설정. 이미 입력값이 null이어도 다음으로 넘어가고 있음
                    hfnLoginLockCheckRequest.setHfnCd(((UserPrincipal)userDetails).getHfnCd());
                    hfnLoginLockCheckRequest.setSiteCd(authentication.getDetails().toString());
                    adminRepository.hfnLoginLockRelease(hfnLoginLockCheckRequest);

                    authorities = userDetails.getAuthorities();
                    return new UsernamePasswordAuthenticationToken(userDetails, password, authorities);

                } catch( BusinessException e) {
                    log.error("관리포털 로그인에러");
                    throw new ServerException(e.getErrorCode(),messageSource.getMessage(e.getErrorCode()));
                } catch (Exception e ) {
                    log.error("관리포털 로그인 로직관련 에러 : " + e.toString());
                    throw new ServerException("E026",messageSource.getMessage("E026"));
                }
            }else{ // 로그인 잠금 상태면 에러 메시지 리턴
                log.error(messageSource.getMessage("E022"));
                throw new ServerException("E022",messageSource.getMessage("E022"));
            }

        }else if(StringUtils.equals(authentication.getDetails().toString(), "userPortal")) // 이용자 포탈 로그인 시
        {
            AdminRequest.UserLoginLockCheckRequest userLoginLockCheckRequest = new AdminRequest.UserLoginLockCheckRequest();
            userLoginLockCheckRequest.setUserId(username);
            String loginLockYn = adminRepository.userLoginLockYn(userLoginLockCheckRequest);

            // 유효한 id 여부 체크
            if(loginLockYn == null)
            {
                log.error(messageSource.getMessage("L002"));
                throw new ServerException("L002",messageSource.getMessage("L002"));
            }

            UserVO user = adminRepository.getUserAuth(userLoginLockCheckRequest);

            // 로그인 잠금 여부 체크
            if(StringUtils.equals(loginLockYn, "N")){
                try{
                    customUserDetailsService.loadBySiteCd(authentication);
                    UserDetails userDetails = customUserDetailsService.loadUserByUsername(username);

                    if (!passwordEncoder().matches(password, userDetails.getPassword())) { // 로그인 실패 시

                        // 로그인 실패 체크
                        int cnt = adminRepository.userLoginFailCntCheck(userLoginLockCheckRequest);
                        if(cnt < 4) { // 로그인 실패 횟수가 4회 이하면 로그인 실패 횟수 증가
                            userLoginLockCheckRequest.setLoginFailCnt(cnt+1);
                            adminRepository.userLoginFailCntSet(userLoginLockCheckRequest);
                        }else if(cnt == 4){ // 로그인 실패 횟수가 5이면 해당 계정 로그인 잠금
                            userLoginLockCheckRequest.setLoginFailCnt(cnt+1);
                            adminRepository.userLoginFailCntSet(userLoginLockCheckRequest);
                            adminRepository.userLoginLockChange(userLoginLockCheckRequest);
                        }

                        log.error(messageSource.getMessage("L002"));
                        throw new ServerException("L002",messageSource.getMessage("L002"));
                    }

                    // 로그인 성공시 로그인잠금 관련 세팅 초기화
                    adminRepository.userLoginLockRelease(userLoginLockCheckRequest);

                    authorities = userDetails.getAuthorities();
                    return new UsernamePasswordAuthenticationToken(userDetails, password, authorities);

                } catch( BusinessException e) {
                    log.error("이용자 포탈 로그인에러");
                    throw new BusinessException(e.getErrorCode(),messageSource.getMessage(e.getErrorCode()), e.getDetailMessage());
                }
                catch (Exception e ) {
                    log.error("이용자 포탈 로그인 잠금 로직관련 에러 : " + e.toString());
                    throw new ServerException("E026",messageSource.getMessage("E026"));
                }
            }else{ // 로그인 잠금 상태면 에러 메시지 리턴
                log.error(messageSource.getMessage("E023"));
                throw new ServerException("E023",messageSource.getMessage("E023"));
            }
        }
        log.error(messageSource.getMessage("L002"));
        throw new ServerException("L002",messageSource.getMessage("L002"));
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}

    public PasswordEncoder passwordEncoder() {
		Map encoders = new HashMap<>();
		encoders.put("sha256", new BCryptPasswordEncoder());

		return new DelegatingPasswordEncoder("sha256", encoders);
    }
}
