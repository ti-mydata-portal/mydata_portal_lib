package com.hanafn.openapi.portal.security.service;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminResponse;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.service.AdminService;
import com.hanafn.openapi.portal.admin.views.vo.CertMailVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.dto.SignUpRequest;
import com.hanafn.openapi.portal.security.model.User;
import com.hanafn.openapi.portal.security.repository.SignupRepository;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.PasswordValidator;
import com.hanafn.openapi.portal.util.service.ISecurity;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class SignupService {

	private static final String FILE_DIR = "/openapi/upload";	// 파일저장경로

	@Autowired
	SignupRepository signupRepository;
	@Autowired
    AdminRepository adminRepository;
	@Autowired
	AdminService adminService;
	@Autowired
	CommonUtil commonUtil;
	@Autowired
	MessageSourceAccessor messageSource;
	@Autowired
	PasswordEncoder passwordEncoder;
    @Autowired
    PasswordValidator passwordValidator;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity aes256Util;

	// 이용자포털 회원ID 중복 체크
	public int userIdDupCheck(AdminRequest.UserDupCheckRequest request) {

		// PORTAL_USER_LOGIN : 개인ID
		AdminRequest.UserDupCheckRequest dupCheckRequest = new AdminRequest.UserDupCheckRequest();
		dupCheckRequest.setUserId(request.getUserId().toLowerCase());

		// 탈퇴 후 이전 아이디 사용 불가
		int idDupCheck = adminRepository.userIdDupCheck(dupCheckRequest);

		return idDupCheck;
	}

	/*********************** 개인 사용자 SignUpService 
	 * @throws Exception 
	 * @throws GeneralSecurityException 
	 * @throws UnsupportedEncodingException ************************/
	public void insertUser(SignUpRequest.UserSingnUpRequest signUpRequest) throws Exception{

		// Creating user's account
		User user = new User(signUpRequest.getUserKey(), signUpRequest.getUserId(), signUpRequest.getUserNm(), signUpRequest.getUserPwd());

		// 비밀번호 검증
        passwordValidator.isValidPassword(signUpRequest.getUserPwd(),signUpRequest.getUserId());

        // 비밀번호 암호화
        try {
            signUpRequest.setUserPwd(passwordEncoder.encode(user.getPassword()));
        } catch (BusinessException e) {
            log.error("이용자포털 회원가입 비밀번호 암호화 에러 : " + e.toString());
            throw new BusinessException("E025", messageSource.getMessage("E025"));
        } catch ( Exception e ) {
            log.error("이용자포털 비밀번호 암호화 에러 : " + e.toString());
            throw new BusinessException("E025", messageSource.getMessage("E025"));
        }

        // 2. 탈퇴유저 유효기간내 재가입방지 Logic
//    		int checkDropUserOrNot = signupRepository.checkDropUserOrNot(signUpRequest);
//    		if(checkDropUserOrNot > 0) {
//    			log.error(messageSource.getMessage("L009"));
//    			throw new BusinessException("L009", messageSource.getMessage("L009"));
//
//    		}

        signUpRequest.setUserStatCd("OK");

        String certUserNm = "";
        String certBirthDt = "";

        CertMailVO certInfo = signupRepository.selectCertUserNm(signUpRequest);

        if(certInfo != null) {
            String sendCtnt = certInfo.getSendCtnt();
            if(sendCtnt != null && !"".equals(sendCtnt)) {
                sendCtnt = aes256Util.decrypt(sendCtnt);
                String[] arrCtnt = sendCtnt.split(":");
                certUserNm = arrCtnt[0];
                certBirthDt = arrCtnt[1];
            }

            String strErrorCode  = "E026";
            String targetUserNm = signUpRequest.getUserNm();
            // 암호화
            try {
                // 이름
                if (signUpRequest.getUserNm() != null && !"".equals(signUpRequest.getUserNm())) {

                    if (certInfo.getSendCd().equals("I") && !certUserNm.equals(signUpRequest.getUserNm())) {
                        log.error("인증정보 조작 감지");
                        strErrorCode = "L011";
                        throw new BusinessException("L011", messageSource.getMessage("L011"));
                    }
                    String encryptedUseorgUserNm = aes256Util.encrypt(signUpRequest.getUserNm());
                    signUpRequest.setUserNm(encryptedUseorgUserNm);
                    log.debug("암호화 - 이름: {}", encryptedUseorgUserNm);
                }
            } catch (UnsupportedEncodingException e) {
                log.error(messageSource.getMessage("E026") + e.getMessage());
                throw new BusinessException(strErrorCode, messageSource.getMessage(strErrorCode));
            } catch ( Exception e ) {
                log.error(messageSource.getMessage("E026") + e.getMessage());
                throw new BusinessException(strErrorCode, messageSource.getMessage(strErrorCode));
            }

            try {
                // 휴대전화번호
                if (signUpRequest.getUserTel() != null && !"".equals(signUpRequest.getUserTel())) {
                    if(certInfo.getSendCd().equals("M") && !certUserNm.equals(targetUserNm + signUpRequest.getUserTel())) {
                        log.error("인증정보 조작 감지");
                        strErrorCode  = "L011";
                        throw new BusinessException("L011", messageSource.getMessage("L011"));
                    }
                    String encryptedUseorgUserTel = aes256Util.encrypt(signUpRequest.getUserTel());
                    signUpRequest.setUserTel(encryptedUseorgUserTel);
                }
            } catch (UnsupportedEncodingException e) {
                log.error(messageSource.getMessage("E026") + e.getMessage());
                throw new BusinessException(strErrorCode, messageSource.getMessage(strErrorCode));
            } catch ( Exception e ) {
                log.error(messageSource.getMessage("E026") + e.getMessage());
                throw new BusinessException(strErrorCode, messageSource.getMessage(strErrorCode));
            }
        }

        try {
            // 생년월일
            if (certBirthDt != null && !"".equals(certBirthDt)) {
                String encryptedBirth = aes256Util.encrypt(certBirthDt);
                certBirthDt = encryptedBirth;
                log.debug("암호화 - 이메일: {}", encryptedBirth);
            }
        } catch ( UnsupportedEncodingException e ) {
            log.error(messageSource.getMessage("E026") + e);
            throw new BusinessException("E026", messageSource.getMessage("E026"));
        } catch ( Exception e ) {
            log.error(messageSource.getMessage("E026") + e);
            throw new BusinessException("E026", messageSource.getMessage("E026"));
        }

        try {
            // 이메일
            if (signUpRequest.getUserEmail() != null && !"".equals(signUpRequest.getUserEmail())) {
                String encryptedUseorgUserEmail = aes256Util.encrypt(signUpRequest.getUserEmail());
                signUpRequest.setUserEmail(encryptedUseorgUserEmail);
                log.debug("암호화 - 이메일: {}", encryptedUseorgUserEmail);
            }
        } catch ( UnsupportedEncodingException e ) {
            log.error(messageSource.getMessage("E026") + e);
            throw new BusinessException("E026", messageSource.getMessage("E026"));
        } catch ( Exception e ) {
            log.error(messageSource.getMessage("E026") + e);
            throw new BusinessException("E026", messageSource.getMessage("E026"));
        }

        try {
            signUpRequest.setUserGb("K");
            signupRepository.insertUser(signUpRequest);
            signupRepository.insertUserRoleForUser(signUpRequest);
            signupRepository.insertLogin(signUpRequest);
            signupRepository.insertPwHisUser(signUpRequest);
        } catch( BusinessException e ) {
            log.error("개인 회원가입 에러 : " + e.toString());
            throw new BusinessException("E025", messageSource.getMessage("E025"));
        } catch( Exception e ) {
            log.error("개인 회원가입 에러 : " + e.toString());
            throw new BusinessException("E025", messageSource.getMessage("E025"));
        }

    	if(StringUtils.equals(signUpRequest.getUserGb(), "K") && StringUtils.isNotBlank(signUpRequest.getUserDi())){
            try {
                AdminRequest.SndCertMgntRequest sndCertMgntRequest = new AdminRequest.SndCertMgntRequest();
                sndCertMgntRequest.setResultCd(signUpRequest.getUserResSeq());
                sndCertMgntRequest.setUserKey(signUpRequest.getUserKey());
                sndCertMgntRequest.setBirthDt(certBirthDt);
                signupRepository.updateUserBirthDt(sndCertMgntRequest);
                signupRepository.updateSndCertMgntForSelfAuth(sndCertMgntRequest);
            } catch (BusinessException e) {
                log.error(e.toString());
                throw new BusinessException("E025", messageSource.getMessage("E026"));
            } catch (Exception e){
                log.error(e.toString());
                throw new BusinessException("E025", messageSource.getMessage("E026"));
            }
     	}
	}

	public void signupUserDupCheck(SignUpRequest.UserSingnUpRequest request){
		AdminRequest.UserDupCheckRequest userDupCheckRequest = new AdminRequest.UserDupCheckRequest();
		userDupCheckRequest.setUserId(request.getUserId());
		userDupCheckRequest.setUserEmail(request.getUserEmail());
		AdminResponse.UserDupCheckResponse userEmailDupCheckResponse = adminService.userEmailDupCheck(userDupCheckRequest);

		int idDupCheck = userIdDupCheck(userDupCheckRequest);
		if(idDupCheck > 0){
			log.error(messageSource.getMessage("E003"));
			throw new BusinessException("E003",messageSource.getMessage("E003"));
		}
		if(StringUtils.equals(userEmailDupCheckResponse.getUserEmailDupYn(), "Y")){
			log.error(messageSource.getMessage("E027"));
			throw new BusinessException("E027",messageSource.getMessage("E027"));
		}
	}

	public void signupUserDupCheckUpdate(SignUpRequest.UserSingnUpRequest request){
		AdminRequest.UserDupCheckRequest userDupCheckRequest = new AdminRequest.UserDupCheckRequest();
		userDupCheckRequest.setUserId(request.getUserId());
		userDupCheckRequest.setUserEmail(request.getUserEmail());
		AdminResponse.UserDupCheckResponse userEmailDupCheckResponse = adminService.userEmailDupCheckUpdate(userDupCheckRequest);

		if(StringUtils.equals(userEmailDupCheckResponse.getUserEmailDupYn(), "Y")){
			log.error(messageSource.getMessage("E027"));
			throw new BusinessException("E027",messageSource.getMessage("E027"));
		}
	}

	/*********************** 법인사용자 SignUpService ************************/
	// 파일저장
	private String fileSave(MultipartFile multipartfile) throws IOException {

		String path = "";

		return path;
	}

	// 파일 디렉토리 생성
	public File getDir() {

		File f = new File(FILE_DIR);
		if(!f.isDirectory()) {
			f.mkdirs();
		}

		return f;
	}

	// 중복인증방지
	public int selectCertDupCnt(AdminRequest.SndCertMgntRequest request) {
		int idDupCheck = signupRepository.selectCertDupCnt(request);
		return idDupCheck;
	}
}