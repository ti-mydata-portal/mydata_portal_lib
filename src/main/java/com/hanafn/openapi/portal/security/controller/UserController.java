package com.hanafn.openapi.portal.security.controller;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.UserRequestDTO;
import com.hanafn.openapi.portal.util.CryptoUtil;
import com.hanafn.openapi.portal.util.service.ISecurity;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
	
	@Autowired
    CryptoUtil aes256Util;

    @PostMapping("/select")
    public ResponseEntity<?> authenticateUser(@CurrentUser UserPrincipal currentUser, @RequestBody UserRequestDTO request) {

        // 복호화
        boolean isEncrypted = true;

        try {
            // 이름
            String decryptedUseorgUserNm = aes256Util.decrypt(currentUser.getUsername());
        } catch (UnsupportedEncodingException e) {
            log.error("복호화 에러 : " + e.toString());
            isEncrypted = false;
        } catch ( Exception e ) {
            log.error("복호화 에러 : " + e.toString());
            isEncrypted = false;
        } finally {
            if (isEncrypted) {
                String decryptedUseorgUserNm = null;
                try {
                    decryptedUseorgUserNm = aes256Util.decrypt(currentUser.getUsername());
                } catch (GeneralSecurityException e) {
                    log.error("GeneralSecurityException",e.toString());
                } catch (UnsupportedEncodingException e) {
                    log.error("UnsupportedEncodingException",e.toString());
                } catch (Exception e) {
                    log.error("Exception",e.toString());
                }
                currentUser.setUsername(decryptedUseorgUserNm);
                log.debug("복호화 - 이름: {}", decryptedUseorgUserNm);
            }
        }
        return ResponseEntity.ok(currentUser);
    }
}
