package com.hanafn.openapi.portal.cmct;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.hanafn.openapi.portal.admin.views.dto.AdminResponse.PolicyResponse;
import com.hanafn.openapi.portal.admin.views.vo.PolicyVO;
import com.hanafn.openapi.portal.exception.BusinessException;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RedisCommunicater {

    private static MessageSourceAccessor messageSource;
    private static String redisAccessControlKey;
    private static String redisEncryptKey;
    private static String redisFileLmtCntKey;
    private static String redisFileCntKey;
    private static String redisFileSizeKey;
    private static String redisTimeLimitedKey;
    private static String redisTimeCntKey;
    private static String redisTxKey;
    private static String getTokenSetKey;               // 토큰 발급 실패 제한 횟수 설정 키
    private static String getTokenCntKey;               // 토큰 발급 실패 횟수 조회 키
    private static String checkTokenSetKey;             // 토큰 검증 실패 제한 횟수 설정 키
    private static String checkTokenCntKey;             // 토큰 검증 실패 횟수 조회 키
    private static String redisAppEchoResponseYNKey;    // APP 대응답 여부 설정 키
    private static String redisApiEchoResponseKey;      // API 대응답 값 설정 키
    private static String redisAppProductLimitedUsageKey; // 앱 상품 접근한도
    private static String redisAppProductCountUsageKey;   //앱 상품 이용건수

    private static String redisMapperKey;               // API정보

    private static String redisApiSuccessCodeKey;       //API 정상응답코드 정의
    private static String redisApiSuccessValueKey;      //API 정상응답코드 value

    private static String redisPrdMppCd1Key;            //상품 mppCd1
    private static String redisPrdSvcCdKey;            //상품 service_code
    		
    private static String redisTimeRangeKey;
    private static String redisTimeRangeCountKey;
    private static String redisTimeLimitedCountConnectionKey;
    private static String redisTimeCountConnectionKey;
    private static String redisConcurrentLimitedCountConnectionKey;
    private static String redisConcurrentCountConnectionKey;
    private static String redisOauthClientKey;
    private static String redisOauthTokenKey;
    private static String redisApiServerUseYnKey;
    
    private static String redisApiInfoKey;
    private static String redisOrgInfoKey;
    private static String redisOrgCertKey;

    private static RedisClient client;
    private static StatefulRedisConnection<String, String> sender;
    private static final Logger logger = LoggerFactory.getLogger(RedisCommunicater.class);

    @Autowired
    private Environment environment;

    @Autowired
    public RedisCommunicater(MessageSourceAccessor messageSource) {
        this.messageSource = messageSource;
    }

    @Value("${spring.redis.host}")
    private String redisHost;

    @Value("${spring.redis.port}")
    private int redisPort;

    @Value("${redis.accessControl.key}")
    public void setRedisAccessControlKey(String redisAccessControlKey) { this.redisAccessControlKey = redisAccessControlKey; }

    @Value("${redis.encrypt.key}")
    public void setRedisEncryptKey(String redisEncryptKey) {
        this.redisEncryptKey = redisEncryptKey;
    }

    @Value("${redis.fileCount.key}")
    public void setRedisFileCntKey(String redisFileGwCntKey) {
        this.redisFileCntKey = redisFileGwCntKey;
    }

    @Value("${redis.fileLmtCount.key}")
    public void setRedisFileLmtCntKey(String redisFileLmtCntKey) {
        this.redisFileLmtCntKey = redisFileLmtCntKey;
    }

    @Value("${redis.fileSize.key}")
    public void setRedisFileSizeKey(String redisFileSizeKey) {
        this.redisFileSizeKey = redisFileSizeKey;
    }

    @Value("${redis.tmLimitedCnt.key}")
    public void setRedisTimeLimitedKey(String redisTimeLimitedKey) {
        this.redisTimeLimitedKey = redisTimeLimitedKey;
    }

    @Value("${redis.timeCnt.key}")
    public void setRedisTimeCntKey(String redisTimeCntKey) {
        this.redisTimeCntKey = redisTimeCntKey;
    }

    @Value("${redis.txRestriction.key}")
    public void setRedisTxKey(String redisTxKey) {
        this.redisTxKey = redisTxKey;
    }

    @Value("${redis.getTokenSet.key}")
    public void setGetTokenSetKey(String getTokenSetKey) {
        this.getTokenSetKey = getTokenSetKey;
    }

    @Value("${redis.getTokenCnt.key}")
    public void setGetTokenCntKey(String getTokenCntKey) {
        this.getTokenCntKey = getTokenCntKey;
    }

    @Value("${redis.checkTokenSet.key}")
    public void setRedisTokenLimitCntKey(String checkTokenSetKey) {
        this.checkTokenSetKey = checkTokenSetKey;
    }

    @Value("${redis.checkTokenCnt.key}")
    public void setRedisTokenFailedCntKey(String checkTokenCntKey) {
        this.checkTokenCntKey = checkTokenCntKey;
    }

    @Value("${redis.app.echoResponseYN.key}")
    public void setAppEchoResponseYNKey(String redisAppEchoResponseYNKey) { this.redisAppEchoResponseYNKey = redisAppEchoResponseYNKey; }

    @Value("${redis.api.echoResponse.key}")
    public void setApiEchoResponseKey(String redisApiEchoResponseKey) { this.redisApiEchoResponseKey = redisApiEchoResponseKey; }

    @Value("${redis.app.productLimitedUsage.key}")
    public void setAppProductLimitedUsage(String redisAppProductLimitedUsageKey) { this.redisAppProductLimitedUsageKey = redisAppProductLimitedUsageKey; }

    @Value("${redis.app.productCountUsageKey.key}")
    public void setAppProductCountUsageKey(String redisAppProductCountUsageKey) { this.redisAppProductCountUsageKey = redisAppProductCountUsageKey; }

    @Value("${redis.mapper.key}")
    public void setMapperKey(String redisMapperKey) { this.redisMapperKey = redisMapperKey; }

    @Value("${redis.api.success.code.key}")
    public void setApiSuccessCodeKey(String redisApiSuccessCodeKey) { this.redisApiSuccessCodeKey = redisApiSuccessCodeKey; }

    @Value("${redis.api.success.value.key}")
    public void setApiSuccessValueKey(String redisApiSuccessValueKey) { this.redisApiSuccessValueKey = redisApiSuccessValueKey; }

    @Value("${redis.prd.mppcd1.key}")
    public void setPrdMppCd1Key(String redisPrdMppCd1Key) { this.redisPrdMppCd1Key = redisPrdMppCd1Key; }

    @Value("${redis.prd.svccd.key}")
    public void setRedisPrdSvcCdKey(String redisPrdSvcCdKey) { this.redisPrdSvcCdKey = redisPrdSvcCdKey; }
    
    @Value("${redis.apiServer.useYn.key}")
    public void setRedisApiServerUseYnKey(String redisApiServerUseYnKey) { this.redisApiServerUseYnKey = redisApiServerUseYnKey; }
//  redis.timeRange.key=gw|timeRange
//	redis.timeLimitedCountConnection.key=gw|timeLimitedCountConnection|
//	redis.timeCountConection.key=gw|timeCountConection|
//	redis.concurrentLimitedCountConnection=gw|concurrentLimitedCountConnection|
//	redis.concurrentCountConnection.key=gw|concurrentCountConnection|
//
//	redis.oauth.client.key=svc|oauthclient|
//	redis.oauth.token.key=svc|oauthtoken|
    @Value("${redis.timeRange.key}")
    public void setRedisTimeRangeKey(String redisTimeRangeKey) {
        this.redisTimeRangeKey = redisTimeRangeKey;
    }
    
    @Value("${redis.timeRangeCount.key}")
    public void setRedisTimeRangeCountKey(String redisTimeRangeCountKey) {
        this.redisTimeRangeCountKey = redisTimeRangeCountKey;
    }
    
    @Value("${redis.timeLimitedCountConnection.key}")
    public void setRedisTimeLimitedCountConnectionKey(String redisTimeLimitedCountConnectionKey) {
        this.redisTimeLimitedCountConnectionKey = redisTimeLimitedCountConnectionKey;
    }
    
    @Value("${redis.timeCountConection.key}")
    public void setRedisTimeCountConnectionKey(String redisTimeCountConnectionKey) {
        this.redisTimeCountConnectionKey = redisTimeCountConnectionKey;
    }
    
    @Value("${redis.concurrentLimitedCountConnection.key}")
    public void setRedisConcurrentLimitedCountConnectionKey(String redisConcurrentLimitedCountConnectionKey) {
        this.redisConcurrentLimitedCountConnectionKey = redisConcurrentLimitedCountConnectionKey;
    }
    
    @Value("${redis.concurrentCountConnection.key}")
    public void setRedisConcurrentCountConnectionKey(String redisConcurrentCountConnectionKey) {
        this.redisConcurrentCountConnectionKey = redisConcurrentCountConnectionKey;
    }
    
    @Value("${redis.oauth.client.key}")
    public void setRedisOauthClientKey(String redisOauthClientKey) {
        this.redisOauthClientKey = redisOauthClientKey;
    }
    
    @Value("${redis.oauth.token.key}")
    public void setRedisOauthTokenKey(String redisOauthTokenKey) {
        this.redisOauthTokenKey = redisOauthTokenKey;
    }
    
    @Value("${redis.api.info.key}")
    public void setRedisApiInfoKey(String redisApiInfoKey) {
        this.redisApiInfoKey = redisApiInfoKey;
    }
    
    @Value("${redis.org.info.key}")
    public void setRedisCorpInfoKey(String redisOrgInfoKey) {
        this.redisOrgInfoKey = redisOrgInfoKey;
    }
    
    @Value("${redis.org.cert.info.key}")
    public void setRedisOrgCertKey(String redisOrgCertKey) {
        this.redisOrgCertKey = redisOrgCertKey;
    }
    
    @Bean
    public RedisClient redisClient() {

        ArrayList<String> sentinelHost = new ArrayList<String>();
        ArrayList<Integer> sentinelPort = new ArrayList<Integer>();

        String sentinelNode = environment.getProperty("spring.redis.sentinel.nodes");
        if (sentinelNode != null) {
            StringTokenizer firstToken = new StringTokenizer(sentinelNode, ",");
            for (int fcnt = 1; firstToken.hasMoreElements(); fcnt++) {
                String firstStr = firstToken.nextToken();
                StringTokenizer secondToken = new StringTokenizer(firstStr, ":");
                for (int scnt = 1; secondToken.hasMoreElements(); scnt++) {
                    String secondStr = secondToken.nextToken();
                    if (scnt % 2 == 0) {
                        sentinelPort.add(Integer.parseInt(secondStr));
                    } else {
                        sentinelHost.add(secondStr);
                    }
                }
            }
            if (sentinelHost.size() > 2) {
                RedisURI redisUri = RedisURI.Builder
                        .sentinel(sentinelHost.get(0), "openapi-master")
                        .withSentinel(sentinelHost.get(0))
                        .withSentinel(sentinelHost.get(1))
                        .build();
                client = RedisClient.create(redisUri);
            } else {
                client = RedisClient.create(RedisURI.Builder.redis(redisHost, redisPort).build());
            }

        } else {
            client = RedisClient.create(RedisURI.Builder.redis(redisHost, redisPort).build());
        }
        return client;
    }

    private static void init() {
        if (sender == null || !sender.isOpen()) {
            sender = client.connect();
        }
    }

    /**
     * Api등록 후 RedisSet와 연동한다.
     *
     * @param url
     * @param value
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void apiRedisSet(String url, String value) throws BusinessException {
        init();

        String key = redisAccessControlKey + url;
        logger.debug("[Redis API SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis API SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("apiRedisSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis API SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis API SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis API SET] Get [" + get + "]");
    }

    /**
     * Api삭제 후 RedisDel와 연동한다.
     *
     * @param url
     * @return 응답 메시지
     * @throws BusinessException
     */

    public static void apiRedisDel(String url) throws BusinessException {
        init();

        String key = redisAccessControlKey + url;
        logger.debug("[Redis API DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis API DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis API DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis API DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis API DEL] Get [" + get + "]");
    }
    
    public static void apiInfoRedisSet(String url, String value) throws BusinessException{
    	init();
    	
    	String key = redisApiInfoKey + url;
    	logger.debug("[Redis API Info SET] Key [" + key + "]");
    	
    	 String result = sender.sync().set(key, value);
         logger.debug("[Redis API Info SET] Result [" + result + "]");
         
         if (!StringUtils.equals(result, "OK")) {
             log.error("apiInfoRedisSet error : " + key + " : " + value);
             throw new BusinessException("E097", messageSource.getMessage("E097"));
         }
         
         String message = "set$" + key + "$" + value;
         logger.debug("[Redis API Info SET] Message [" + message + "]");

         Long publish = sender.sync().publish("channel_openapi_gateway", message);
         logger.debug("[Redis API Info SET] Publish [" + publish + "]");

         String get = sender.sync().get(key);
         logger.debug("[Redis API Info SET] Get [" + get + "]");
    }
    
    /**
     * Api삭제 후 RedisDel와 연동한다.
     *
     * @param url
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void apiInfoRedisDel(String url) throws BusinessException {
        init();

        String key = redisApiInfoKey + url;
        logger.debug("[Redis API Info DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis API Info DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis API Info DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis API Info DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis API Info DEL] Get [" + get + "]");
    }

    public static void orgInfoRedisSet(String orgSerialNum, String value) throws BusinessException{
    	init();
    	String url = StringUtils.replace(orgSerialNum, "-", "_");
    	String key = redisOrgInfoKey + url;
    	logger.debug("[Redis ORG Info SET] Key [" + key + "]");
    	
    	 String result = sender.sync().set(key, value);
         logger.debug("[Redis ORG Info SET] Result [" + result + "]");
         
         if (!StringUtils.equals(result, "OK")) {
             log.error("orgInfoRedisSet error : " + key + " : " + value);
             throw new BusinessException("E097", messageSource.getMessage("E097"));
         }
         
         String message = "set$" + key + "$" + value;
         logger.debug("[Redis ORG Info SET] Message [" + message + "]");

         Long publish = sender.sync().publish("channel_openapi_gateway", message);
         logger.debug("[Redis ORG Info SET] Publish [" + publish + "]");

         String get = sender.sync().get(key);
         logger.debug("[Redis ORG Info SET] Get [" + get + "]");
    }
    
    /**
     * Api삭제 후 RedisDel와 연동한다.
     *
     * @param url
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void orgInfoRedisDel(String orgSerialNum) throws BusinessException {
        init();
        String url = StringUtils.replace(orgSerialNum, "-", "_");
        String key = redisOrgInfoKey + url;
        logger.debug("[Redis ORG Info DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis ORG Info DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis ORG Info DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis ORG Info DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis ORG Info DEL] Get [" + get + "]");
    }
    
    /**
     * Api삭제 후 RedisDel와 연동한다.
     *
     * @param url
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void orgCertInfoRedisSet(String orgCode, String value) throws BusinessException{
    	init();
    	String key = redisOrgCertKey + orgCode;
    	logger.debug("[Redis ORG Cert Info SET] Key [" + key + "]");
    	
    	 String result = sender.sync().set(key, value);
         logger.debug("[Redis ORG Cert Info SET] Result [" + result + "]");
         
         if (!StringUtils.equals(result, "OK")) {
             log.error("orgCertInfoRedisSet error : " + key + " : " + value);
             throw new BusinessException("E097", messageSource.getMessage("E097"));
         }
         
         String message = "set$" + key + "$" + value;
         logger.debug("[Redis ORG Cert Info SET] Message [" + message + "]");

         Long publish = sender.sync().publish("channel_openapi_gateway", message);
         logger.debug("[Redis ORG Cert Info SET] Publish [" + publish + "]");

         String get = sender.sync().get(key);
         logger.debug("[Redis ORG Cert Info SET] Get [" + get + "]");
    }
    
    /**
     * Api삭제 후 RedisDel와 연동한다.
     *
     * @param url
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void orgCertInfoRedisDel(String orgCode) throws BusinessException {
        init();
        String key = redisOrgCertKey + orgCode;
        logger.debug("[Redis ORG Cert Info DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis ORG Cert Info DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis ORG Cert Info DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis ORG Cert Info DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis ORG Cert Info DEL] Get [" + get + "]");
    }
    
    /**
     * App Redis Set와 연동한다.
     *
     * @param clientId
     * @param value
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void appRedisSet(String clientId, String value) throws BusinessException {
        init();

        String url = clientId;
        url = StringUtils.replace(url, "-", "_");
        String key = redisAccessControlKey + url;
        logger.debug("[Redis Client SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis Client SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("appRedisSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis Client SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis Client SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis Client SET] Get [" + get + "]");
    }

    /**
     * App Redis Del와 연동한다.
     *
     * @param clientId
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void appRedisDel(String clientId) throws BusinessException {
        init();

        String url = clientId;
        url = StringUtils.replace(url, "-", "_");
        String key = redisAccessControlKey + url;
        logger.debug("[Redis Client DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis Client DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis Client DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis Client DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis Client DEL] Get [" + get + "]");
    }

    /**
     * App API Redis Set와 연동한다.
     *
     * @param clientId
     * @param url
     * @param value
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void appApiRedisSet(String clientId, String url, String value) throws BusinessException {
        init();

        String str = url;
        str = clientId + "|" + str;
        str = StringUtils.replace(str, "-", "_");

        String key = redisAccessControlKey + str;
        logger.debug("[Redis APP-API SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis APP-API SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("appApiRedisSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis APP-API SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis APP-API SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis APP-API SET] Get [" + get + "]");
    }

    /**
     * App API Redis Del와 연동한다.
     *
     * @param clientId
     * @param url
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void appApiRedisDel(String clientId, String url) throws BusinessException {
        init();

        String str = url;
        str = clientId + "|" + str;
        str = StringUtils.replace(str, "-", "_");

        String key = redisAccessControlKey + str;
        logger.debug("[Redis APP-API DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis APP-API DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis APP-API DEL] Message[" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis APP-API DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis APP-API DEL] Get [" + get + "]");
    }

    /**
     * App IP Redis Set와 연동한다.
     *
     * @param clientId
     * @param ip
     * @param value
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void appIpRedisSet(String clientId, String ip, String value) throws BusinessException {
        init();

        String url = clientId + "|" + ip;
        url = StringUtils.replace(url, "-", "_");
        String key = redisAccessControlKey + url;
        logger.debug("[Redis IP SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis IP SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("appIpRedisSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis IP SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis IP SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis IP SET] Get [" + get + "]");
    }

    /**
     * App IP Redis Del와 연동한다.
     *
     * @param clientId
     * @param ip
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void appIpRedisDel(String clientId, String ip) throws BusinessException {
        init();

        String url = clientId + "|" + ip;
        url = StringUtils.replace(url, "-", "_");
        String key = redisAccessControlKey + url;
        logger.debug("[Redis IP DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis IP DEL] Result [" + result + "]");

        String message = "del$" + key;

        logger.debug("[Redis IP DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis IP DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis IP DEL] Get [" + get + "]");
    }
    
    /**
     * App IP Redis Set와 연동한다.
     * @param clientId
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void appBlackIpRedisSet(String clientId, String value) throws BusinessException {
        init();

        String url =  "black|" + clientId;
        url = StringUtils.replace(url, "-", "_");
        String key = redisAccessControlKey + url;
        logger.debug("[Redis IP SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis IP SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("appBlackIpRedisSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis IP SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis IP SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis IP SET] Get [" + get + "]");
    }

    /**
     * App IP Redis Del와 연동한다.
     *
     * @param clientId
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void appBlackIpRedisDel(String clientId) throws BusinessException {
        init();
        
        String url =  "black|" + clientId;
        url = StringUtils.replace(url, "-", "_");
        String key = redisAccessControlKey + url;
        logger.debug("[Redis IP DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis IP DEL] Result [" + result + "]");

        String message = "del$" + key;

        logger.debug("[Redis IP DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis IP DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis IP DEL] Get [" + get + "]");
    }

    /**
     * exclusionIp Redis Set와 연동한다.
     *
     * @param value
     * @param value
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void exclusionIpRedisSet(String value) throws BusinessException {
        init();

        String url = "exclusionIp";
        url = StringUtils.replace(url, "-", "_");
        String key = redisAccessControlKey + url;
        logger.debug("[Redis Exclusion IP SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis Exclusion IP SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("exclusionIpRedisSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis Exclusion IP SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis Exclusion IP SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis Exclusion IP SET] Get [" + get + "]");
    }

    /**
     * exclusionIp Redis Set와 연동한다.
     *
     * @return getResult
     * @throws BusinessException
     */
    public static String exclusionIpRedisGet() throws BusinessException {
        init();

        String url = "exclusionIp";
        url = StringUtils.replace(url, "-", "_");
        String key = redisAccessControlKey + url;
        logger.debug("[Redis Exclusion IP GET] Key [" + key + "]");

        String getResult = sender.sync().get(key);
        logger.debug("[Redis IP GET] Get [" + getResult + "]");

        return getResult;
    }

    /**
     * 기관코드 별 암호화 키 설정
     */
    public static void encKeyRedisSet(String entrCd, String encKey) throws BusinessException {
        init();

        String url = entrCd;
        url = StringUtils.replace(url, "-", "_");
        String key = redisEncryptKey + url;
        logger.debug("★redisEncryptUrl:" + key);
        logger.debug("[Redis ENC_KEY SET] Key [" + key + "]");

        String result = sender.sync().set(key, encKey);
        logger.debug("[Redis ENC_KEY SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("encKeyRedisSet error : " + key + " : " + encKey);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + encKey;
        logger.debug("[Redis ENC_KEY SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis ENC_KEY SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis ENC_KEY SET] Get [" + get + "]");
    }

    /**
     * 기관코드 별 암호화 키 삭제
     */
    public static void encKeyRedisDel(String entrCd, String encKey) throws BusinessException {
        init();

        String url = entrCd;
        url = StringUtils.replace(url, "-", "_");
        String key = redisEncryptKey + url;
        logger.debug("[Redis ENC_KEY DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis ENC_KEY DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis ENC_KEY DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis ENC_KEY DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis ENC_KEY DEL] Get [" + get + "]");
    }

    /**
     * 파일 동시 접속 수 제한
     *
     * @param url
     * @param value (Long)
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void apiFileCntRedisSet(String url, String value) throws BusinessException {
        init();

        String key = redisFileLmtCntKey + url;
        logger.debug("[Redis FILE COUNT SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis FILE COUNT SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("apiFileCntRedisSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis FILE COUNT SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis FILE COUNT SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis FILE COUNT SET] Get [" + get + "]");
    }

    /**
     * 파일 동시 접속 수 제한 초기화 1
     *
     * @param url
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void apiFileCntRedisDel(String url) throws BusinessException {
        init();

        String key = redisFileLmtCntKey + url;
        logger.debug("[Redis fileLimitedCountConnection DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis fileLimitedCountConnection DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis fileLimitedCountConnection DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis fileLimitedCountConnection DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis fileLimitedCountConnection DEL] Get [" + get + "]");

        apiFileGwCntRedisDel(url);
    }

    /**
     * 파일 동시 접속 수 제한 초기화 2
     *
     * @param url
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void apiFileGwCntRedisDel(String url) throws BusinessException {
        init();

        String key = redisFileCntKey + url;
        logger.debug("[Redis fileCountConnection DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis fileCountConnection DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis fileCountConnection DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis fileCountConnection DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis fileCountConnection DEL] Get [" + get + "]");
    }

    /**
     * 파일 크기 제어
     *
     * @param url
     * @param value (Long)
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void apiFileSizeRedisSet(String url, String value) throws BusinessException {
        init();

        String key = redisFileSizeKey + url;
        logger.debug("[Redis FILE SIZE SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis FILE SIZE SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("apiFileSizeRedisSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis FILE SIZE SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis FILE SIZE SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis FILE SIZE SET] Get [" + get + "]");
    }

    /**
     * 파일 크기 제어 초기화
     *
     * @param url
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void apiFileSizeRedisDel(String url) throws BusinessException {
        init();

        String key = redisFileSizeKey + url;
        logger.debug("[Redis FILE SIZE DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis FILE SIZE DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis FILE SIZE DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis FILE SIZE DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis FILE SIZE DEL] Get [" + get + "]");
    }

    /**
     * API 사용량(사용주기-분,초/할당량) 제어
     *
     * @param url
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void apiTimeLimitedRedisSet(String url, String value) throws BusinessException {
        init();

        logger.debug("[Redis timeLimitedCountConnection SET] value [" + value + "]");

        String key = redisTimeLimitedKey + url;
        logger.debug("[Redis timeLimitedCountConnection SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis timeLimitedCountConnection SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("apiTimeLimitedRedisSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis timeLimitedCountConnection SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis timeLimitedCountConnection SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis timeLimitedCountConnection SET] Get [" + get + "]");

        //count del
        apiTimeCntRedisDel(url);
    }

    /**
     * API 사용량(사용주기-분,초/할당량) 제어 초기화 1
     *
     * @param url
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void apiTimeLimitedRedisDel(String url) throws BusinessException {
        init();

        String key = redisTimeLimitedKey + url;
        logger.debug("[Redis timeLimitedCountConnection DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis timeLimitedCountConnection DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis timeLimitedCountConnection DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis timeLimitedCountConnection DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis timeLimitedCountConnection DEL] Get [" + get + "]");

        apiTimeCntRedisDel(url);
    }

    /**
     * API 사용량(사용주기-분,초/할당량) 제어 초기화 2
     *
     * @param url
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void apiTimeCntRedisDel(String url) throws BusinessException {
        init();

        String key = redisTimeCntKey + url;
        logger.debug("[Redis timeCountConnection DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis timeCountConnection DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis timeCountConnection DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis timeCountConnection DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis timeCountConnection DEL] Get [" + get + "]");
    }

    /**
     * 거래제한 (요일/시간)
     *
     * @param url
     * @param value
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void apiTxRedisSet(String url, String value) throws BusinessException {
        init();

        logger.debug("[Redis transactionRestriction SET] value [" + value + "]");

        String key = redisTxKey + url;
        logger.debug("[Redis transactionRestriction SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis transactionRestriction SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("apiTxRedisSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis transactionRestriction SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis transactionRestriction SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis transactionRestriction SET] Get [" + get + "]");
    }

    /**
     * 거래제한 (요일/시간) 초기화
     *
     * @param url
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void apiTxRedisDel(String url) throws BusinessException {
        init();

        String key = redisTxKey + url;
        logger.debug("[Redis transactionRestriction DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis transactionRestriction DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis transactionRestriction DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis transactionRestriction DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis transactionRestriction DEL] Get [" + get + "]");
    }

    /**
     * APP거래제한 - 토큰 발급 실패 제한 횟수 설정
     *
     * @param clientId
     * @param value    (Long)
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void getTokenSetRedisSet(String clientId, String value) throws BusinessException {
        init();

        String key = getTokenSetKey + StringUtils.replace(clientId, "-", "_");
        logger.debug("[Redis limitedFailedGetToken SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis limitedFailedGetToken SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("limitedFailedGetToken SET error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis limitedFailedGetToken SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis limitedFailedGetToken SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis limitedFailedGetToken SET] Get [" + get + "]");
    }

    /**
     * APP거래제한 - 토큰 발급 실패 횟수 조회
     *
     * @param clientId
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static String getTokenCntRedisGet(String clientId) throws BusinessException {
        init();

        String key = getTokenCntKey + StringUtils.replace(clientId, "-", "_");
        logger.debug("[Redis countFailedGetToken GET] Key [" + key + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis countFailedGetToken GET] Get [" + get + "]");

        return get;
    }

    /**
     * APP거래제한 - 토큰 발급 실패 횟수 초기화
     *
     * @param clientId
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void getTokenCntRedisSet(String clientId, String value) throws BusinessException {

        init();

        String key = getTokenCntKey + StringUtils.replace(clientId, "-", "_");
        logger.debug("[Redis countFailedGetToken SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis countFailedGetToken SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("countFailedGetToken SET error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis countFailedGetToken SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis countFailedGetToken SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis countFailedGetToken SET] Get [" + get + "]");
    }

    /**
     * APP거래제한 - 토큰 발급 제한 키 삭제
     *
     * @param clientId
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void getTokenRedisDel(String clientId) throws BusinessException {
        init();

        // 토큰발급실패제한횟수
        String key = getTokenSetKey + StringUtils.replace(clientId, "-", "_");
        logger.debug("[Redis limitedFailedGetToken DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis limitedFailedGetToken DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis limitedFailedGetToken DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis limitedFailedGetToken DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis limitedFailedGetToken DEL] Get [" + get + "]");

        // 토큰발급실패횟수
        key = getTokenCntKey + clientId;
        logger.debug("[Redis countFailedGetToken DEL] Key [" + key + "]");

        result = sender.sync().del(key);
        logger.debug("[Redis countFailedGetToken DEL] Result [" + result + "]");

        message = "del$" + key;
        logger.debug("[Redis countFailedGetToken DEL] Message [" + message + "]");

        publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis countFailedGetToken DEL] Publish [" + publish + "]");

        get = sender.sync().get(key);
        logger.debug("[Redis countFailedGetToken DEL] Get [" + get + "]");
    }

    /**
     * 이용기관 거래제한 - 토큰 검증 실패 제한 횟수 설정
     *
     * @param entrCd
     * @param value  (Long)
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void checkTokenSetRedisSet(String entrCd, String value) throws BusinessException {
        init();

        String key = checkTokenSetKey + StringUtils.replace(entrCd, "-", "_");
        logger.debug("[Redis limitedFailedCheckToken SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis limitedFailedCheckToken SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("limitedFailedCheckToken SET error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis limitedFailedCheckToken SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis limitedFailedCheckToken SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis limitedFailedCheckToken SET] Get [" + get + "]");
    }

    /**
     * 이용기관 거래제한 - 토큰 검증 실패 횟수 조회
     *
     * @param entrCd
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static String checkTokenCntRedisGet(String entrCd) throws BusinessException {
        init();

        String key = checkTokenCntKey + entrCd;
        logger.debug("[Redis countFailedCheckToken GET] Key [" + key + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis countFailedCheckToken GET] Get [" + get + "]");

        return get;
    }

    /**
     * 이용기관 거래제한 - 토큰 검증 실패 횟수 초기화
     *
     * @param entrCd
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void checkTokenCntRedisSet(String entrCd, String value) throws BusinessException {

        init();

        String key = checkTokenCntKey + entrCd;
        logger.debug("[Redis countFailedCheckToken SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis countFailedCheckToken SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("countFailedGetToken SET error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis countFailedCheckToken SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis countFailedCheckToken SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis countFailedCheckToken SET] Get [" + get + "]");
    }

    /**
     * 이용기관 거래제한 - 토큰 검증 제한 키 삭제
     *
     * @param entrCd
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void checkTokenRedisDel(String entrCd) throws BusinessException {
        init();

        // 토큰검증실패제한횟수
        String key = checkTokenSetKey + entrCd;
        logger.debug("[Redis limitedFailedCheckToken DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis limitedFailedCheckToken DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis limitedFailedCheckToken DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis limitedFailedCheckToken DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis limitedFailedCheckToken DEL] Get [" + get + "]");

        // 토큰검증실패횟수
        key = checkTokenCntKey + entrCd;
        logger.debug("[Redis countFailedCheckToken DEL] Key [" + key + "]");

        result = sender.sync().del(key);
        logger.debug("[Redis countFailedCheckToken DEL] Result [" + result + "]");

        message = "del$" + key;
        logger.debug("[Redis countFailedCheckToken DEL] Message [" + message + "]");

        publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis countFailedCheckToken DEL] Publish [" + publish + "]");

        get = sender.sync().get(key);
        logger.debug("[Redis countFailedCheckToken DEL] Get [" + get + "]");
    }

    /**
     * 로그인 token 저장
     *
     * @param token = token
     */
    public static void setToken(String token) throws BusinessException {
        init();

        String result = sender.sync().setex(token, 1800, "Y");
        logger.debug("[Redis token SET] Result [" + result + "]");

        String message = "set$" + token;
        logger.debug("[Redis token SET] Message [" + message + "]");

        String get = sender.sync().get(token);
        logger.debug("[Redis token SET] Get [" + get + "]");
    }

    /**
     * 로그아웃 token 삭제
     */
    public static void delToken(String token) throws BusinessException {
        init();

        if (token != null) {
            long result = sender.sync().del(token);
            logger.debug("[Redis token DEL] Result [" + result + "]");

            String message = "del$" + token;
            logger.debug("[Redis token DEL] Message [" + message + "]");

            String get = sender.sync().get(token);
            logger.debug("[Redis token DEL] Get [" + get + "]");
        }
    }

    /**
     * token 검증
     */
    public static void validToken(String tokenValue) throws BusinessException {

        /** Redis 연결 **/
        init();

        logger.debug("[Redis validToken] tokenValue [" + tokenValue + "]");
        String token = sender.sync().get(tokenValue);
        logger.debug("[Redis validToken] token [" + token + "]");

        if (token == null || "".equals(token)) {
            log.error("validToken error");
            throw new BusinessException("E029", messageSource.getMessage("E029", Locale.KOREA));
        }
    }

    /**
     * APP 대응답 설정
     *
     * @Params # String clientId = APP의 APP KEY
     * # String clientId = 대응답 사용여부 (true/false)
     */
    public static void setAppEchoResponse(String appKey, String value) throws BusinessException {

        /** Redis 연결 **/
        init();

        /** Redis에 값 세팅 **/
        String key = redisAppEchoResponseYNKey + appKey;
        logger.debug("[Redis App Echo YN SET] Key [" + key + "]");


        String result = sender.sync().set(key, value);
        logger.debug("[Redis App Echo YN SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("setAppEchoResponse error : " + key + " : " + value);
            throw new BusinessException("E180", messageSource.getMessage("E180"));
        }

        /** Redis에서 Publish 하여 GW에게 알려줌 **/
        String msg = "set$" + key + "$" + value;
        logger.debug("[Redis App Echo YN SET] Message [" + msg + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", msg);
        logger.debug("[Redis App Echo YN SET] Publish [" + publish + "]");

        /** Redis에 세팅된 값 확인 **/
        String get = sender.sync().get(key);
        logger.debug("[Redis App Echo YN SET] Get [" + get + "]");
    }

    /**
     * APP 대응답 설정 해제
     */
    public static void delAppEchoResponse(String appKey) throws BusinessException {

        /** Redis 연결 **/
        init();

        /** Redis에 값 세팅 **/
        String key = redisAppEchoResponseYNKey + appKey;
        logger.debug("[Redis App Echo YN DEL] Key [" + key + "]");

        Long result = sender.sync().del(key);
        logger.debug("[Redis App Echo YN DEL] Result [" + result + "]");

        /** Redis에서 Publish 하여 GW에게 알려줌 **/
        String msg = "del$" + key;
        logger.debug("[Redis App Echo YN DEL] Message [" + msg + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", msg);
        logger.debug("[Redis App Echo YN DEL] Publish [" + publish + "]");

        /** Redis에 세팅된 값 확인 **/
        String get = sender.sync().get(key);
        logger.debug("[Redis App Echo YN DEL] Get [" + get + "]");
    }

    /**
     * API 대응답 값 설정
     *
     * @Params # String url = 해당 API의 접속 URL
     * # String value = 해당 API의 대응답 값
     */
    public static void setApiEchoResponse(String url, String value) throws BusinessException {

        /** Redis 연결 **/
        init();

        /** Redis에 값 세팅 **/
        String key = redisApiEchoResponseKey + url;
        logger.debug("[Redis API Echo Response SET] Key [" + key + "]");


        String result = sender.sync().set(key, value);
        logger.debug("[Redis API Echo Response SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("setApiEchoResponse error : " + key + " : " + value);
            throw new BusinessException("E181", messageSource.getMessage("E181"));
        }

        /** Redis에서 Publish 하여 GW에게 알려줌 **/
        String msg = "set$" + key + "$" + value;
        logger.debug("[Redis API Echo Response SET] Message [" + msg + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", msg);
        logger.debug("[Redis API Echo Response SET] Publish [" + publish + "]");

        /** Redis에 세팅된 값 확인 **/
        String get = sender.sync().get(key);
        logger.debug("[Redis API Echo Response SET] Get [" + get + "]");
    }

    /**
     * API 대응답 값 설정
     *
     * @Params # String url = 해당 API의 접속 URL
     * # String value = 해당 API의 대응답 값
     */
    public static void setApiEchoResponse(String url, String searchKey, String searchValue, String value) throws BusinessException {

        /** Redis 연결 **/
        init();

        /** Redis에 값 세팅 **/
        String key = redisApiEchoResponseKey + url + "|" + searchKey + "|" + searchValue;
        logger.debug("[Redis API Echo Response SET] Key [" + key + "]");


        String result = sender.sync().set(key, value);
        logger.debug("[Redis API Echo Response SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("setApiEchoResponse error : " + key + " : " + value);
            throw new BusinessException("E181", messageSource.getMessage("E181"));
        }

        /** Redis에서 Publish 하여 GW에게 알려줌 **/
        String msg = "set$" + key + "$" + value;
        logger.debug("[Redis API Echo Response SET] Message [" + msg + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", msg);
        logger.debug("[Redis API Echo Response SET] Publish [" + publish + "]");

        /** Redis에 세팅된 값 확인 **/
        String get = sender.sync().get(key);
        logger.debug("[Redis API Echo Response SET] Get [" + get + "]");
    }

    /**
     * API 대응답 값 해제
     */
    public static void delApiEchoResponse(String url) throws BusinessException {

        /** Redis 연결 **/
        init();

        /** Redis에 값 세팅 **/
        String key = redisApiEchoResponseKey + url;
        logger.debug("[Redis API Echo Response DEL] Key [" + key + "]");

        Long result = sender.sync().del(key);
        logger.debug("[Redis API Echo Response DEL] Result [" + result + "]");

        /** Redis에서 Publish 하여 GW에게 알려줌 **/
        String msg = "del$" + key;
        logger.debug("[Redis API Echo Response DEL] Message [" + msg + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", msg);
        logger.debug("[Redis API Echo Response DEL] Publish [" + publish + "]");

        /** Redis에 세팅된 값 확인 **/
        String get = sender.sync().get(key);
        logger.debug("[Redis API Echo Response DEL] Get [" + get + "]");
    }

    /**
     * API 대응답 값 해제
     */
    public static void delApiEchoResponse(String url, String searchKey, String searchValue) throws BusinessException {

        /** Redis 연결 **/
        init();

        /** Redis에 값 세팅 **/
        String key = redisApiEchoResponseKey + url + "|" + searchKey + "|" + searchValue;
        logger.debug("[Redis API Echo Response DEL] Key [" + key + "]");

        Long result = sender.sync().del(key);
        logger.debug("[Redis API Echo Response DEL] Result [" + result + "]");

        /** Redis에서 Publish 하여 GW에게 알려줌 **/
        String msg = "del$" + key;
        logger.debug("[Redis API Echo Response DEL] Message [" + msg + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", msg);
        logger.debug("[Redis API Echo Response DEL] Publish [" + publish + "]");

        /** Redis에 세팅된 값 확인 **/
        String get = sender.sync().get(key);
        logger.debug("[Redis API Echo Response DEL] Get [" + get + "]");
    }

//    /**
//     * App Product Redis Set와 연동한다.
//     *
//     * @param clientId
//     * @param prdId
//     * @param value
//     * @return 응답 메시지
//     * @throws BusinessException
//     */
//    @Deprecated
//    public static void appPrdRedisSet(String clientId, String prdId, String value) throws BusinessException {
//        init();
//
//        String str = prdId;
//        str = clientId + "|" + str;
//        str = StringUtils.replace(str, "-", "_");
//
//        String key = redisAccessControlKey + str;
//        logger.debug("[Redis APP-PRD SET] Key [" + key + "]");
//
//        String result = sender.sync().set(key, value);
//        logger.debug("[Redis APP-PRD SET] Result [" + result + "]");
//
//        if (!StringUtils.equals(result, "OK")) {
//            log.error("appPrdRedisSet error : " + key + " : " + value);
//            throw new BusinessException("E097", messageSource.getMessage("E097"));
//        }
//
//        String message = "set$" + key + "$" + value;
//        logger.debug("[Redis APP-PRD SET] Message [" + message + "]");
//
//        Long publish = sender.sync().publish("channel_openapi_gateway", message);
//        logger.debug("[Redis APP-PRD SET] Publish [" + publish + "]");
//
//        String get = sender.sync().get(key);
//        logger.debug("[Redis APP-PRD SET] Get [" + get + "]");
//    }

    /**
     * App Product 한도 Redis Set와 연동한다.
     *
     * @param clientId
     * @param prdId
     * @param limit
     * @param count
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void appPrdLimitRedisSet(String clientId, String prdCd, String limit, String count) throws BusinessException {
        init();

        String str = prdCd;
        str = clientId + "|" + str;
        str = StringUtils.replace(str, "-", "_");

        //한도설정
        String key = redisAppProductLimitedUsageKey + str;
        logger.debug("[Redis APP-PRD LIMIT SET] Key [" + key + "]");

        String result = sender.sync().set(key, limit);
        logger.debug("[Redis APP-PRD LIMIT SET] Result [" + result + "]");
        if (!StringUtils.equals(result, "OK")) {
            log.error("appPrdRedisSet error : " + key + " : " + limit);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + limit;
        logger.debug("[Redis APP-PRD LIMIT SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis APP-PRD LIMIT SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis APP-PRD LIMIT SET] Get [" + get + "]");

        //카운트 설정
        key = redisAppProductCountUsageKey + str;
        logger.debug("[Redis APP-PRD COUNT SET] Key [" + key + "]");

        result = sender.sync().set(key, count);
        logger.debug("[Redis APP-PRD COUNT ET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("appPrdRedisSet error : " + key + " : " + count);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        message = "set$" + key + "$" + count;
        logger.debug("[Redis APP-PRD COUNT SET] Message [" + message + "]");

        publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis APP-PRD COUNT SET] Publish [" + publish + "]");

        get = sender.sync().get(key);
        logger.debug("[Redis APP-PRD COUNT SET] Get [" + get + "]");
    }

    /**
     * App Product 한도 Redis Del와 연동한다.
     *
     * @param clientId
     * @param prdId
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void appPrdLimitRedisDel(String clientId, String prdCd) throws BusinessException {
        init();

        String str = prdCd;
        str = clientId + "|" + str;
        str = StringUtils.replace(str, "-", "_");

        //한도 삭제
        String key = redisAppProductLimitedUsageKey + str;
        logger.debug("[Redis APP-PRD LIMIT SET] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis APP-PRD LIMIT SET] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis APP-PRD DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis APP-PRD DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis APP-PRD DEL] Get [" + get + "]");

        //카운트 삭제
        key = redisAppProductCountUsageKey + str;
        logger.debug("[Redis APP-PRD Count SET] Key [" + key + "]");

        result = sender.sync().del(key);
        logger.debug("[Redis APP-PRD Count SET] Result [" + result + "]");

        message = "del$" + key;
        logger.debug("[Redis APP-PRD DEL] Message [" + message + "]");

        publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis APP-PRD DEL] Publish [" + publish + "]");

        get = sender.sync().get(key);
        logger.debug("[Redis APP-PRD DEL] Get [" + get + "]");
    }

    /**
     * 상품-API Redis Set
     * gw|productCode|/digital/niceid/api/v.1.0/test/nice001$3
     * @param clientId (클라이언드 아이디)
     * @param prdCd (상품코드)
     * @param value true or false
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void productCodeRedisSet(String clientId, String prdCd, String value) throws BusinessException {
        init();

        String key = String.format("%s%s|%s|%s", redisAccessControlKey, "productCode", clientId, prdCd);
        key = StringUtils.replace(key, "-", "_");

        logger.debug("[Redis PRD-API SET] Key [" + key + "]");

        String result = sender.sync().set(key, String.valueOf(value));
        logger.debug("[Redis PRD-API SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("productCodeRedisSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis PRD-API SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis PRD-API SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis PRD-API SET] Get [" + get + "]");
    }

    /**
     * 상품-API Redis Del
     * gw|productCode|/digital/niceid/api/v.1.0/test/nice001$3
     * @param clientId (클라이언드 아이디)
     * @param prdCd (상품코드)
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void productCodeRedisDel(String clientId, String prdCd) throws BusinessException {
        init();

        String key = String.format("%s%s|%s|%s", redisAccessControlKey, "productCode", clientId, prdCd);
        key = StringUtils.replace(key, "-", "_");
        logger.debug("[Redis PRD-API DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis PRD-API DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis PRD-API DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis PRD-API DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis PRD-API DEL] Get [" + get + "]");
    }

    /**
     * API-MAPPER Redis Set
     * svc|mapper|id-svc|URL$value
     * svc|mapper|0001|/digital/niceid/api/v.1.0/test/nice001$mapper
     * @param svc, url
     * @param mapper (mapper)
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void mapperRedisSet(String svc, String apiUrl, String mapper) throws BusinessException {
        init();

        String key = redisMapperKey + svc +"|" + apiUrl;
        logger.debug("[Redis API-MAPPER SET] Key [" + key + "]");

        String result = sender.sync().set(key, mapper);
        logger.debug("[Redis API-MAPPER SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("apiMapperRedisSet error : " + key + " : " + mapper);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + mapper;
        logger.debug("[Redis API-MAPPER SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_service_"+svc, message);
        logger.debug("[Redis API-MAPPER SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis API-MAPPER SET] Get [" + get + "]");
    }

    /**
     * API-MAPPER Redis Del
     * @param apiUrl
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void mapperRedisDel(String svc, String apiUrl) throws BusinessException {
        init();

        String key = redisMapperKey + svc +"|" + apiUrl;
        logger.debug("[Redis API-MAPPER DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis API-MAPPER DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis API-MAPPER DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_service+"+svc, message);
        logger.debug("[Redis API-MAPPER DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis API-MAPPER DEL] Get [" + get + "]");
    }

    /**
     * API-SUCCCODE Redis Set
     * svc|apiRsKey|<0100>|[URL]
     * svc|apiRsValue|<0100>|[URL]
     * @param apiUrl
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void apiResponseCodeSet(String svc, String apiUrl, String rsKey, String rsValue) throws BusinessException {
        init();

        //svc|apiRsKey|<0100>|[URL]
        //svc|apiRsValue|<0100>|[URL]

        //set code
        String key = redisApiSuccessCodeKey + svc +"|" + apiUrl;
        logger.debug("[Redis API-SUCCCODE SET] Key [" + key + "]");

        String result = sender.sync().set(key, rsKey);
        logger.debug("[Redis API-SUCCCODE SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("apiResponseCodeSet error : " + key + " : " + rsKey);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + rsKey;
        logger.debug("[Redis API-SUCCCODE SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_service_"+svc, message);
        logger.debug("[Redis API-SUCCCODE SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis API-SUCCCODE SET] Get [" + get + "]");

        //set value
        key = redisApiSuccessValueKey + svc +"|" + apiUrl;
        logger.debug("[Redis API-SUCCVALUE SET] Key [" + key + "]");

        result = sender.sync().set(key, rsValue);
        logger.debug("[Redis API-SUCCVALUE SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("apiResponseValueSet error : " + key + " : " + rsValue);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        message = "set$" + key + "$" + rsValue;
        logger.debug("[Redis API-SUCCVALUE SET] Message [" + message + "]");

        publish = sender.sync().publish("channel_openapi_service_"+svc, message);
        logger.debug("[Redis API-SUCCVALUE SET] Publish [" + publish + "]");

        get = sender.sync().get(key);
        logger.debug("[Redis API-SUCCVALUE SET] Get [" + get + "]");
    }

    public static void apiResponseCodeDel(String svc, String apiUrl, String rsKey, String rsValue) throws BusinessException {
        init();

        //del code
        String key = redisApiSuccessCodeKey + svc +"|" + apiUrl;
        logger.debug("[Redis API-SUCCCODE DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis API-SUCCCODE DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis API-SUCCCODE DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_service_"+svc, message);
        logger.debug("[Redis API-SUCCCODE DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis API-SUCCCODE SET] Get [" + get + "]");

        //del value
        key = redisApiSuccessValueKey + svc +"|" + apiUrl;
        logger.debug("[Redis API-SUCCVALUE DEL] Key [" + key + "]");

        result = sender.sync().del(key);
        logger.debug("[Redis API-SUCCVALUE DEL] Result [" + result + "]");

        message = "del$" + key;
        logger.debug("[Redis API-SUCCVALUE DEL] Message [" + message + "]");

        publish = sender.sync().publish("channel_openapi_service_"+svc, message);
        logger.debug("[Redis API-SUCCVALUE DEL] Publish [" + publish + "]");

        get = sender.sync().get(key);
        logger.debug("[Redis API-SUCCVALUE DEL] Get [" + get + "]");
    }

    /**
     * API 거래를 위한 전문기관 ID
     * @param clientId
     * @param value
     * @throws BusinessException
     */
    public static void appPrdMppCd1RedisSet(String category, String clientId, String url, String value) throws BusinessException {
        init();

        //private static final String API_MPPCD1 = "svc|mppCd1|"; //0100|<clientId>|<URL>

        String str = category +"|" + clientId + "|" + url;
        str = StringUtils.replace(str, "-", "_");

        String key = redisPrdMppCd1Key + str;
        logger.debug("[Redis APP-PRD-MPP SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis  APP-PRD-MPP SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("appPrdMppCd1RedisSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis APP-PRD-MPP SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_service_"+category, message);
        logger.debug("[Redis APP-PRD-MPP SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis APP-PRD-MPP SET] Get [" + get + "]");
    }

    /**
     * 모든 IP 허용여부를 App Redis Set와 연동한다.
     * @param clientId
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void appAllowAllIpRedisSet(String clientId, String value) throws BusinessException {
        init();

        String allowYn = (value != null && value.equals("Y")) ? "true" : "false";
        if (allowYn.equals("false"))
            appAllowAllIpRedisDel(clientId);

        String keyset =  "allowAllIp|" + clientId;
        keyset = StringUtils.replace(keyset, "-", "_");
        String key = redisAccessControlKey + keyset;
        logger.debug("[Redis ALLOW ALL IP SET] Key [" + key + "]");

        String result = sender.sync().set(key, allowYn);
        logger.debug("[Redis ALLOW ALL IP SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("appAllowAllIpRedisSet error : " + key + " : " + allowYn);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + allowYn;
        logger.debug("[Redis ALLOW ALL IP SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis ALLOW ALL IP SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis ALLOW ALL IP SET] Get [" + get + "]");
    }

    /**
     * App IP Redis Del와 연동한다.
     *
     * @param clientId
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void appAllowAllIpRedisDel(String clientId) throws BusinessException {
        init();

        String keyset =  "allowAllIp|" + clientId;
        keyset = StringUtils.replace(keyset, "-", "_");
        logger.debug("[Redis ALLOW ALL IP DEL] Key [" + keyset + "]");

        long result = sender.sync().del(keyset);
        logger.debug("[Redis ALLOW ALL IP DEL] Result [" + result + "]");

        String message = "del$" + keyset;

        logger.debug("[Redis ALLOW ALL IP DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis ALLOW ALL IP DEL] Publish [" + publish + "]");

        String get = sender.sync().get(keyset);
        logger.debug("[Redis ALLOW ALL IP DEL] Get [" + get + "]");
    }

    /**
     * API 거래를 위한 전문기관 ID
     * @param category
     * @param prdCd
     * @param svcCd
     * @throws BusinessException
     */
    public static void appPrdSvcCdRedisSet(String category, String prdCd, String svcCd) throws BusinessException {
        if (svcCd == null || svcCd.trim().length() == 0)
            return;

        init();

        //private static final String API_MPPCD1 = "svc|mppCd1|"; //0100|<clientId>|<URL>

        String str = category +"|" + prdCd;
        str = StringUtils.replace(str, "-", "_");

        String key = redisPrdSvcCdKey + str;
        logger.debug("[Redis APP-PRD-SVCCD SET] Key [" + key + "]");

        String result = sender.sync().set(key, svcCd);
        logger.debug("[Redis  APP-PRD-SVCCD SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("appPrdMppCd1RedisSet error : " + key + " : " + svcCd);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + svcCd;
        logger.debug("[Redis APP-PRD-SVCCD SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_service_"+category, message);
        logger.debug("[Redis APP-PRD-SVCCD SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis APP-PRD-SVCCD SET] Get [" + get + "]");
    }

    /**
     * APP 별 사용량 제한 정책(랜덤) 등록 후 RedisSet와 연동한다.
     *
     * @param client_id
     * @param value
     * @return 응답 메시지
     * @throws BusinessException
     */
    public static void appUsageRatioRedisSet(String client_id, String value) throws BusinessException {
        init();

        String key = String.format("%s|%s|%s", "gw", "usageRatio", client_id.replace('-', '_'));
        logger.debug("[Redis APP Usage Ratio SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis APP Usage Ratio SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("APP Usage Ratio SET Error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis APP Usage Ratio SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis APP Usage Ratio SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis APP Usage Ratio SET] Get [" + get + "]");
    }

    /**
     * APP 별 사용량 제한 정책(랜덤) 삭제 후 Redisdel과 연동한다.
     *
     * @param client_id
     * @return 응답 메시지
     * @throws BusinessException
     */

    public static void appUsageRatioRedisDel(String client_id) throws BusinessException {
        init();

        String key = String.format("%s|%s|%s", "gw", "usageRatio", client_id.replace('-', '_'));
        logger.debug("[Redis API DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis API DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis API DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis API DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis API DEL] Get [" + get + "]");
    }

    /**
     * 포털 redis key 조회
     *
     * @return 응답 메시지
     * @throws BusinessException+
     */
    public static PolicyResponse getRedis(String pattern) throws BusinessException {
        init();

        pattern = StringUtils.isEmpty(pattern) ? "*" : "*"+pattern+"*";
        List<PolicyVO> list = new ArrayList<>();
        List<String> cmd = sender.sync().keys(pattern);

        for (String key : cmd) {
            PolicyVO vo = new PolicyVO();
            vo.setKey(key);
            vo.setValue(sender.sync().get(key));
            list.add(vo);
        }

        PolicyResponse data = new PolicyResponse();
        data.setRedisList(list);

        return data;
    }
    
    
    
    
    
    
    /**
     * 시간대별 접근제한 횟수 설정
     * @param client_id 클라이언트 아이
     * @param url url
     * @param startTime 시작일자(HH:mm:ss)
     * @param EndTime 종료일자(HH:mm:ss)
     * @param valu 특정시간에 허용할 갯수
     */
    public static void timeRangeSet(String client_id, String url, String startTime, String EndTime, String value) {
    	init();

        String key = redisTimeRangeKey + client_id + "|" + url + "|" + startTime + "|" + EndTime;
        logger.debug("[Redis time range SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis time range SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("timeRangeSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis time range SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis time range SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis time range SET] Get [" + get + "]");
    }
    
    /**
     * 시간대별 접근제한 횟수 삭제
     * @param client_id 클라이언트 아이
     * @param url url
     * @param startTime 시작일자(HH:mm:ss)
     * @param EndTime 종료일자(HH:mm:ss)
     */
    public static void timeRangeDel(String client_id, String url, String startTime, String EndTime) {
        init();

        String key = redisTimeRangeKey + client_id + "|" + url + "|" + startTime + "|" + EndTime;
        logger.debug("[Redis time range DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis time range DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis time range DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis time range DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis time range DEL] Get [" + get + "]");
    }
    
    /**
     * 시간대별 접근제한 횟수 저장 및 조회
     * @param client_id 클라이언트 아이
     * @param url url
     * @param startTime 시작일자(HH:mm:ss)
     * @param EndTime 종료일자(HH:mm:ss)
     * @param valu 특정시간에 허용할 갯수
     */
    public static void timeRangeCntSet(String client_id, String url, String startTime, String EndTime, String value) {
    	init();

        String key = redisTimeRangeCountKey + client_id + "|" + url + "|" + startTime + "|" + EndTime;
        logger.debug("[Redis time range count SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis time range count SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("timeRangeCntSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis time range count SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis time range count SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis time range count SET] Get [" + get + "]");
    }
    
    /**
     * 시간대별 접근제한 횟수 삭제
     * @param client_id 클라이언트 아이
     * @param url url
     * @param startTime 시작일자(HH:mm:ss)
     * @param EndTime 종료일자(HH:mm:ss)
     */
    public static void timeRangeCntDel(String client_id, String url, String startTime, String EndTime) {
        init();

        String key = redisTimeRangeCountKey + client_id + "|" + url + "|" + startTime + "|" + EndTime;
        logger.debug("[Redis time range count DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis time range count DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis time range count DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis time range count DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis time range count DEL] Get [" + get + "]");
    }
    
    /**
     * 공통초당 사용량/기관별 접 제한/ip별 접근 제한/api별 접근제한 횟수설정
     * @param org_code 'common'/org_cd/ip/service url
     * @param value jsonString
     */
    public static void timeLimitCntConnSet(String org_code, String value) {
    	init();
    	
    	if(org_code == null || "".equals(org_code)) {
    		org_code = "common";
    	}

        String key = redisTimeLimitedCountConnectionKey + org_code;
        logger.debug("[Redis redisTimeLimitedCountConnectionKey SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis redisTimeLimitedCountConnectionKey SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("timeLimitCntConnSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis redisTimeLimitedCountConnectionKey SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisTimeLimitedCountConnectionKey SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisTimeLimitedCountConnectionKey SET] Get [" + get + "]");
    }
    
    /**
     * 공통초당 사용량/기관별 접 제한/ip별 접근 제한/api별 접근제한 횟수 삭제
     * @param org_code 'common'/org_cd/ip/service url
     */
    public static void timeLimitCntConnDel(String org_code) {
        init();

        String key = redisTimeLimitedCountConnectionKey + org_code;
        logger.debug("[Redis redisTimeLimitedCountConnectionKey DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis redisTimeLimitedCountConnectionKey DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis redisTimeLimitedCountConnectionKey DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisTimeLimitedCountConnectionKey DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisTimeLimitedCountConnectionKey DEL] Get [" + get + "]");
    }
    
    /**
     * 공통초당 사용량/기관별 접 제한/ip별 접근 제한/api별 접근제한 횟수 저장 및 조회
     * @param org_code 'common'/org_cd/ip/service url
     * @param value 회수제한
     */
    public static void timeCntConnSet(String org_code, String value) {
    	init();
    	
    	if(org_code == null || "".equals(org_code)) {
    		org_code = "common";
    	}

        String key = redisTimeCountConnectionKey + org_code;
        logger.debug("[Redis redisTimeCountConnectionKey SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis redisTimeCountConnectionKey SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("timeCntConnSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis redisTimeCountConnectionKey SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisTimeCountConnectionKey SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisTimeCountConnectionKey SET] Get [" + get + "]");
    }
    
    /**
     * 공통초당 사용량/기관별 접 제한/ip별 접근 제한/api별 접근제한 횟수 삭제
     * @param org_code 'common'/org_cd/ip/service url
     */
    public static void timeCntConnDel(String org_code) {
    	init();

        String key = redisTimeCountConnectionKey + org_code;
        logger.debug("[Redis redisTimeCountConnectionKey DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis redisTimeCountConnectionKey DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis redisTimeCountConnectionKey DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisTimeCountConnectionKey DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisTimeCountConnectionKey DEL] Get [" + get + "]");
    }
    
    /**
     * 기관-api별 접근 / ip-api별 접근제한 회수 설정
     * @param org_code org_cd/ip
     * @param svcUrl service url
     * @param value jsonString
     */
    public static void timeLimitCntConnSet(String org_code, String svcUrl, String value) {
    	init();
    	
    	String key = redisTimeLimitedCountConnectionKey + org_code + "|" + svcUrl;
        logger.debug("[Redis redisTimeLimitedCountConnectionKey SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis redisTimeLimitedCountConnectionKey SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("timeLimitCntConnSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis redisTimeLimitedCountConnectionKey SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisTimeLimitedCountConnectionKey SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisTimeLimitedCountConnectionKey SET] Get [" + get + "]");
    }
    
    /**
     * 기관-api별 접근 / ip-api별 접근제한 회수 삭제
     * @param org_code org_cd/ip
     * @param svcUrl service url
     */
    public static void timeLimitCntConnDel(String org_code, String svcUrl) {
    	init();

        String key = redisTimeLimitedCountConnectionKey + org_code + "|" + svcUrl;
        logger.debug("[Redis redisTimeLimitedCountConnectionKey DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis redisTimeLimitedCountConnectionKey DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis redisTimeLimitedCountConnectionKey DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisTimeLimitedCountConnectionKey DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisTimeLimitedCountConnectionKey DEL] Get [" + get + "]");
    }
    
    /**
     * 기관-api별 접근 / ip-api별 접근제한 횟수 저장 및 조회
     * @param org_code org_cd/ip
     * @param svcUrl service url
     * @param value 회수제한
     */
    public static void timeCntConnSet(String org_code, String svcUrl, String value) {
    	init();

    	String key = redisTimeCountConnectionKey + org_code + "|" + svcUrl;
        logger.debug("[Redis redisTimeCountConnectionKey SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis redisTimeCountConnectionKey SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("timeCntConnSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis redisTimeCountConnectionKey SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisTimeCountConnectionKey SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisTimeCountConnectionKey SET] Get [" + get + "]");
    }
    
    /**
     * 기관-api별 접근 / ip-api별 접근제한 횟수 삭제
     * @param org_code org_cd/ip
     * @param svcUrl service url
     */
    public static void timeCntConnDel(String org_code, String svcUrl) {
    	init();

        String key = redisTimeCountConnectionKey + org_code + "|" + svcUrl;
        logger.debug("[Redis redisTimeCountConnectionKey DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis redisTimeCountConnectionKey DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis redisTimeCountConnectionKey DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisTimeCountConnectionKey DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisTimeCountConnectionKey DEL] Get [" + get + "]");
    }
    
    /**
     * 공통동시접속/기관별동시접속/ip별 동시접속/api별 동시접속 횟수설정
     * @param org_code 'common'/org_cd/ip/service url
     * @param value jsonString
     */
    public static void concurLimitCntConnSet(String org_code, String value) {
    	init();
    	
    	if(org_code == null || "".equals(org_code)) {
    		org_code = "common";
    	}

    	String key = redisConcurrentLimitedCountConnectionKey + org_code;
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("concurLimitCntConnSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey SET] Get [" + get + "]");
    }
    
    /**
     * 공통동시접속/기관별동시접속/ip별 동시접속/api별 동시접속 횟수설정
     * @param org_code 'common'/org_cd/ip/service url
     */
    public static void concurLimitCntConnDel(String org_code) {
    	init();

        String key = redisConcurrentLimitedCountConnectionKey + org_code;
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey DEL] Get [" + get + "]");
    }
    
    /**
     * 공통동시접속/기관별동시접속/ip별 동시접속/api별 동시접속 횟수 저장 및 조회
     * @param org_code 'common'/org_cd/ip/service url
     * @param value 횟수제한
     */
    public static void concurCntConnSet(String org_code, String value) {
    	init();
    	
    	if(org_code == null || "".equals(org_code)) {
    		org_code = "common";
    	}

    	String key = redisConcurrentCountConnectionKey + org_code;
        logger.debug("[Redis redisConcurrentCountConnectionKey SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis redisConcurrentCountConnectionKey SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("concurCntConnSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis redisConcurrentCountConnectionKey SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisConcurrentCountConnectionKey SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisConcurrentCountConnectionKey SET] Get [" + get + "]");
    }
    
    /**
     * 공통동시접속/기관별동시접속/ip별 동시접속/api별 동시접속 횟수 삭제
     * @param org_code 'common'/org_cd/ip/service url
     */
    public static void concurCntConnDel(String org_code) {
    	init();

        String key = redisConcurrentCountConnectionKey + org_code;
        logger.debug("[Redis redisConcurrentCountConnectionKey DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis redisConcurrentCountConnectionKey DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis redisConcurrentCountConnectionKey DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisConcurrentCountConnectionKey DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisConcurrentCountConnectionKey DEL] Get [" + get + "]");
    }
    
    /**
     * 기관-api별 동시접속 / ip-api별 동시접속 횟수 설정
     * @param org_code org_cd/ip
     * @param svcUrl service url
     * @param value 
     */
    public static void concurLimitCntConnSet(String org_code, String svcUrl, String value) {
    	init();
    	
    	String key = redisConcurrentLimitedCountConnectionKey + org_code + "|" + svcUrl;
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("concurLimitCntConnSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey SET] Get [" + get + "]");
    }
    
    /**
     * 기관-api별 동시접속 / ip-api별 동시접속 횟수 삭제
     * @param org_code org_cd/ip
     * @param svcUrl service url
     */
    public static void concurLimitCntConnDel(String org_code, String svcUrl) {
    	init();

        String key = redisConcurrentLimitedCountConnectionKey + org_code + "|" + svcUrl;
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisConcurrentLimitedCountConnectionKey DEL] Get [" + get + "]");
    }
    
    /**
     * 기관-api별 동시접속 / ip-api별 동시접속 횟수 저장 및 조회
     * @param org_code org_cd/ip
     * @param svcUrl service url
     * @param value 횟수제한
     */
    public static void concurCntConnSet(String org_code, String svcUrl, String value) {
    	init();
    	
    	String key = redisConcurrentCountConnectionKey + org_code + "|" + svcUrl;
        logger.debug("[Redis redisConcurrentCountConnectionKey SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis redisConcurrentCountConnectionKey SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("concurCntConnSet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis redisConcurrentCountConnectionKey SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisConcurrentCountConnectionKey SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisConcurrentCountConnectionKey SET] Get [" + get + "]");
    }
    
    /**
     * 기관-api별 동시접속 / ip-api별 동시접속 횟수 삭제
     * @param org_code org_cd/ip
     * @param svcUrl service url
     */
    public static void concurCntConnDel(String org_code, String svcUrl) {
    	init();

        String key = redisConcurrentCountConnectionKey + org_code + "|" + svcUrl;
        logger.debug("[Redis redisConcurrentCountConnectionKey DEL] Key [" + key + "]");

        long result = sender.sync().del(key);
        logger.debug("[Redis redisConcurrentCountConnectionKey DEL] Result [" + result + "]");

        String message = "del$" + key;
        logger.debug("[Redis redisConcurrentCountConnectionKey DEL] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisConcurrentCountConnectionKey DEL] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisConcurrentCountConnectionKey DEL] Get [" + get + "]");
    }
    
    /**
     * 토큰 발급정보
     * @param org_code org_cd
     * @param clienti_id 클라이언트아이
     * @param client_secret 클라이언트시크릿
     */
    public static void oauthClientKeySet(String org_code, String client_id, String client_secret) {
    	init();
    	
    	String value = client_id + ":" + client_secret;

    	String key = redisOauthClientKey + org_code;
        logger.debug("[Redis redisOauthClientKey SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis redisOauthClientKey SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("oauthClientKeySet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis redisOauthClientKey SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_service_0100", message);
        logger.debug("[Redis redisOauthClientKey SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisOauthClientKey SET] Get [" + get + "]");
    }
    
    /**
     * 토큰 등록
     * @param org_code org_cd/ip
     * @param svcUrl service url
     * @param value 횟수제한
     */
    public static void oauthTokenKeySet(String client_id, String value) {
    	init();
    	
    	String key = redisOauthTokenKey + client_id;
        logger.debug("[Redis redisOauthTokenKey SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis redisOauthTokenKey SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("oauthTokenKeySet error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis redisOauthTokenKey SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_service_0100", message);
        logger.debug("[Redis redisOauthTokenKey SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisOauthTokenKey SET] Get [" + get + "]");
    }
    
    public static void apiServerUseYn(String value) {
    	init();
    	
    	String key = redisApiServerUseYnKey;
        logger.debug("[Redis redisApiServerUseYnKey SET] Key [" + key + "]");

        String result = sender.sync().set(key, value);
        logger.debug("[Redis redisApiServerUseYnKey SET] Result [" + result + "]");

        if (!StringUtils.equals(result, "OK")) {
            log.error("apiServerUseYn error : " + key + " : " + value);
            throw new BusinessException("E097", messageSource.getMessage("E097"));
        }

        String message = "set$" + key + "$" + value;
        logger.debug("[Redis redisApiServerUseYnKey SET] Message [" + message + "]");

        Long publish = sender.sync().publish("channel_openapi_gateway", message);
        logger.debug("[Redis redisApiServerUseYnKey SET] Publish [" + publish + "]");

        String get = sender.sync().get(key);
        logger.debug("[Redis redisApiServerUseYnKey SET] Get [" + get + "]");
    	
    }
}