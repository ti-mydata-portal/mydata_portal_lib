package com.hanafn.openapi.portal.cmct;

import org.springframework.http.HttpMethod;

import com.hanafn.openapi.portal.admin.views.dto.AdminResponse.CommonApiResponse;


public interface CommunicatorInterface {
    public CommonApiResponse communicateServer(String url, HttpMethod method, Object data, boolean procError);
}
