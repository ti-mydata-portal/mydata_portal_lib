package com.hanafn.openapi.portal.util;

import java.lang.reflect.Array;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.UserPrincipal;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CommonUtil {
    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    MessageSourceAccessor messageSource;

    public UserPrincipal userKeyChangeIfDeveloper(UserPrincipal currentUser){

        UserPrincipal userPrincipal = currentUser;
        if(StringUtils.equals(currentUser.getUserType(),"ORGD")) {
            currentUser.setUserKey(currentUser.getUseorgKey());
        }

        return userPrincipal;
    }

    public String stackTraceToString(Exception e) {
        Stream<StackTraceElement> stackTraceElementStream = Arrays.stream(e.getStackTrace()).limit(20);
        String stackResult = stackTraceElementStream.map(StackTraceElement::toString).collect(Collectors.joining("\n"));
        return stackResult;
    }

    /** 파일이름만 추출 => 파일이름 없을 시, 빈 문자열 return **/
    public String getOriginalFileName(String fileName) {
        String result = "";
        if(!StringUtils.isBlank(fileName)){
            int pos = fileName.lastIndexOf('\\');
            result = fileName.substring( pos + 1 );
            return result;
        }
        return result;
    }

    public String getUserTypeByRoleCd(String roleCd){
        String userType = null;
        if(StringUtils.equals(roleCd, "30"))
            userType = "ORGM";
        else if(StringUtils.equals(roleCd, "31"))
            userType = "ORGD";
        else if(StringUtils.equals(roleCd, "40"))
            userType = "USER";
        else {
            log.error("처리할 수 없는 roleCd :" + roleCd);
            throw new BusinessException(messageSource.getMessage("E092"));
        }
        return userType;
    }
    public long unixTime(){
        Date currentDate = new Date();
        long unixTime = currentDate.getTime() / 1000;
        return unixTime;
    }
    public Boolean compareWithShaStrings(String string, String shaString) {
        if (!passwordEncoder.matches(string, shaString)) {
            log.error(messageSource.getMessage("L002"));
            throw new BusinessException("L002",messageSource.getMessage("L002"));
        }
        return true;
    }

    public Boolean compareWithShaStringsPw(String string, String shaString) {
        if (passwordEncoder.matches(string, shaString)) {
            return false;
        } else {
            return true;
        }
    }

    public String getIp(HttpServletRequest request) {

        String ip = request.getHeader("X-Forwarded-For");

        //log.debug(">>>> X-FORWARDED-FOR : " + ip);

        if (ip == null) {
            ip = request.getHeader("Proxy-Client-IP");
            //log.debug(">>>> Proxy-Client-IP : " + ip);
        }
        if (ip == null) {
            ip = request.getHeader("WL-Proxy-Client-IP"); // 웹로직
            //log.debug(">>>> WL-Proxy-Client-IP : " + ip);
        }
        if (ip == null) {
            ip = request.getHeader("HTTP_CLIENT_IP");
            //log.debug(">>>> HTTP_CLIENT_IP : " + ip);
        }
        if (ip == null) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            //log.debug(">>>> HTTP_X_FORWARDED_FOR : " + ip);
        }
        if (ip == null) {
            //ip = request.getRemoteAddr();
        }
        return ip;
    }

    public boolean superAdminCheck(UserPrincipal currentUser) {
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() == "ROLE_SYS_ADMIN") {
                return true;
            }
        }
        return false;
    }

    public boolean ipdupCheck(String strIps, String localIp) {

        String ips = strIps;
        if(ips == null){
            ips = "";
        }

        String[] ipList = StringUtils.split(ips, ",");

        if (ipList != null) {
            for (String ipInfo : ipList) {
                if (StringUtils.equals(ipInfo, localIp)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Object type 변수가 비어있는지 체크
     *
     * @param obj
     * @return Boolean : true / false
     */
    public static Boolean empty(Object obj) {
        if (obj instanceof String) return obj == null || "".equals(obj.toString().trim());
        else if (obj instanceof List) return obj == null || ((List) obj).isEmpty();
        else if (obj instanceof Map) return obj == null || ((Map) obj).isEmpty();
        else if (obj instanceof Object[]) return obj == null || Array.getLength(obj) == 0;
        else return obj == null;
    }

    public String menuCnvrs(String code) {
        String menuNm = "";
        switch(code) {
            case "SEL":
                menuNm = "조회";
                break;
            case "INS":
                menuNm = "등록";
                break;
            case "UPD":
                menuNm = "변경";
                break;
            case "APR":
                menuNm = "승인";
                break;
            case "DEL":
                menuNm = "삭제";
                break;
        }
        return menuNm;
    }

    /**
     * maskingName
     * @param str
     * @return String
     */
    public static String maskingName(String str) {
        String replaceString = str;

        String pattern = "";
        if(str.length() == 2) {
            pattern = "^(.)(.+)$";
        } else {
            pattern = "^(.)(.+)(.)$";
        }

        Matcher matcher = Pattern.compile(pattern).matcher(str);

        if(matcher.matches()) {
            replaceString = "";

            for(int i=1;i<=matcher.groupCount();i++) {
                String replaceTarget = matcher.group(i);
                if(i == 2) {
                    char[] c = new char[replaceTarget.length()];
                    Arrays.fill(c, '*');

                    replaceString = replaceString + String.valueOf(c);
                } else {
                    replaceString = replaceString + replaceTarget;
                }

            }
        }
        return replaceString;
    }

    public static String maskingId(String str){
        String replaceString = str;
        replaceString = str.replaceAll("(?<=.{3})." , "*");
        return replaceString;
    }

    /**
     * maskingPhone
     * @param str
     * @return String
     */
    public static String maskingPhone(String str){
        String replaceString = str;

        Matcher matcher = Pattern.compile("^(\\d{3})-?(\\d{3,4})-?(\\d{4})$").matcher(str);

        if(matcher.matches()) {
            replaceString = "";

            boolean isHyphen = false;
            if(str.indexOf("-") > -1) {
                isHyphen = true;
            }

            for(int i=1;i<=matcher.groupCount();i++) {
                String replaceTarget = matcher.group(i);

                if(i != 1) {
                    char[] c = new char[2];
                    Arrays.fill(c, '*');
                    replaceString += replaceTarget.substring(0,2) + String.valueOf(c);
                } else {
                    replaceString += replaceString + replaceTarget;
                }

                if(isHyphen && i < matcher.groupCount()) {
                    replaceString = replaceString + "-";
                }
            }
        }
        return replaceString;
    }

    public static String maskingEmail(String str) {
        String replaceString = str;

        Matcher matcher = Pattern.compile("^(..)(.*)([@]{1})(.*)$").matcher(str);

        if(matcher.matches()) {
            replaceString = "";

            for(int i=1;i<=matcher.groupCount();i++) {
                String replaceTarget = matcher.group(i);
                if(i == 2) {
                    char[] c = new char[replaceTarget.length()];
                    Arrays.fill(c, '*');

                    replaceString = replaceString + String.valueOf(c);
                } else {
                    replaceString = replaceString + replaceTarget;
                }
            }
        }
        return replaceString;
    }

    public static boolean brnCheck (String str) {

        int hap = 0;
        int temp = 0;

        int check[] = {1,3,7,1,3,7,1,3,5};

        if(str.length() != 10) return false;

        for(int i=0; i < 9; i++){
            if(str.charAt(i) < '0' || str.charAt(i) > '9') 	return false;
            hap = hap + (Character.getNumericValue(str.charAt(i)) * check[temp]);
            temp++;
        }

        hap += (Character.getNumericValue(str.charAt(8))*5)/10;

        if ((10 - (hap%10))%10 == Character.getNumericValue(str.charAt(9))) {
            return true;
        } else {
            return false;
        }

    }
    
    public String getGuid() {
		LocalDateTime localDateTime = LocalDateTime.now();
		String date = localDateTime.format(DateTimeFormatter.BASIC_ISO_DATE);
		
		
		String seqNo = localDateTime.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
		String result = "";
		
		result+=date;
		try {
			String hostNm = InetAddress.getLocalHost().getHostName();
			
			if(hostNm != null && !"".equals(hostNm) && hostNm.length() == 8) {
				result+=hostNm;
			}else {
				log.info(hostNm);
				result+="PotTestD";
			}
		} catch (UnknownHostException e) {
			log.error(e.getMessage());
		}
		result+=seqNo.substring(3);
		result+="00";
		
		return result;
	}
    
    /*
     * 로그스테시에서 엘라스특서치로 데이터를 넣을때 생성하는 시간은 GMT 시간 기준으로 우리나라보다 9시간 전이다.
     * 엘라스틱서치에 들어가는 시간이 9시간 전이므로 
     * 파라미터로 받은 시간은 9시간 전으로 수정하여 조회한다.
     */
    public static String chgToGMTTimeFromSeoulTime(String yyyyMMddHHmmss) throws Exception {
    	SimpleDateFormat srcFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    	//SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    	SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    	Date srcDate = srcFormat.parse(yyyyMMddHHmmss);
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(srcDate);
    	cal.add(Calendar.HOUR, -9);
    	return targetFormat.format(cal.getTime());
    }
    public static String chgToTime(String yyyyMMddHHmmss) throws Exception {
    	SimpleDateFormat srcFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    	Date srcDate = srcFormat.parse(yyyyMMddHHmmss);
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(srcDate);
    	cal.add(Calendar.HOUR, -1);
    	return srcFormat.format(cal.getTime());
    }
}