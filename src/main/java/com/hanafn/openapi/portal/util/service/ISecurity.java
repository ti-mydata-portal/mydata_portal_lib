package com.hanafn.openapi.portal.util.service;

import org.springframework.stereotype.Component;

@Component
public interface ISecurity {
	public String encrypt(String str) throws Exception;
	public String decrypt(String str) throws Exception;
	public String encrypt(String key, String str) throws Exception;
	public String decrypt(String key, String str) throws Exception;
}
