package com.hanafn.openapi.portal.util;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value="mydata-batch")
public interface BatchServiceProxy {
	
	@RequestMapping(value = "/batch/statistics", method = RequestMethod.GET)
	public String batchManual(@RequestParam("reqDate") String reqDate, @RequestParam("tmSlot") String tmSlot);
	
}


