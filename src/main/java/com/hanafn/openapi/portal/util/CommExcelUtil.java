
package com.hanafn.openapi.portal.util;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

import com.hanafn.openapi.portal.exception.BusinessException;

import lombok.extern.slf4j.Slf4j;

@Component("commXls")
@Slf4j
public class CommExcelUtil {

    @Autowired
    MessageSourceAccessor messageSource;

    public void excelDown(int sheetCnt, String[] sheetNms, List<String[]> headers, List<List<?>> bodys,List<String[]> exportColumns, HttpServletResponse response, String fileNm) throws Exception {

        // 워크북 생성
        Workbook wb = new HSSFWorkbook();
        
        if(sheetCnt != headers.size() || sheetCnt != bodys.size()) {
        	//시트별 헤더/바디 정보 미일치
        	
        }
        
        for(int i = 0; i < sheetCnt; i++) {
        	String sheetNm = "";
        	if(sheetNms == null || sheetNms.length != sheetCnt) {
        		sheetNm = "sheet"+i;
        	}else {
        		sheetNm = sheetNms[i];
        	}
        	
        	Sheet sheet = wb.createSheet(sheetNm);
        	
        	Row row = null;
            Cell cell = null;
            int rowNo = 0;
            
            // 테이블 헤더용 스타일
            CellStyle headStyle = wb.createCellStyle();
            setHeaderStyle(headStyle);
            
            // 데이터용 스타일
            CellStyle bodyStyle = wb.createCellStyle();
            setBodyStyle(bodyStyle);
            
            // 헤더 생성
            row = sheet.createRow(rowNo++);
            setHeader(row, cell, headStyle, headers.get(i));
            
            List<?> bodyVoList = bodys.get(i);
            String[] exportColumn = exportColumns.get(i);
            
            // 데이터 부분 생성
            for(Object data : bodyVoList) {
                row = sheet.createRow(rowNo++);
                setBody(row, cell, bodyStyle, data, exportColumn);
            }
        }

        // 컨텐츠 타입과 파일명 지정
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename="+URLEncoder.encode(fileNm, "UTF-8")+".xls");

        try {
            // 엑셀 출력
            wb.write(response.getOutputStream());
            wb.close();
        } catch (IOException ie) {
            log.error("ExcelUtils write Error: " + ie.toString());
            throw new BusinessException("ExcelUtils write Error", ie.getCause());
        }catch(Exception e) {
//            e.printStackTrace();
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        }
    }
    
    public void excelDown(int sheetCnt, String[] sheetNms, List<String[]> headers, List<List<?>> bodys,List<String[]> exportColumns, HttpServletResponse response) throws Exception {
    	excelDown(sheetCnt, sheetNms, headers, bodys, exportColumns, response, "APP_USE_LIST");
    }

    // 헤더 스타일
    private void setHeaderStyle(CellStyle headStyle) {
        // 가는 경계선
        headStyle.setBorderTop(BorderStyle.THIN);
        headStyle.setBorderBottom(BorderStyle.THIN);
        headStyle.setBorderLeft(BorderStyle.THIN);
        headStyle.setBorderRight(BorderStyle.THIN);

        // 배경색은 노란색
        headStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.YELLOW.getIndex());
        headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        // 데이터는 가운데 정렬
        headStyle.setAlignment(HorizontalAlignment.CENTER);
    }

    // 헤더 생성
    private void setHeader(Row row, Cell cell, CellStyle headStyle, String[] headerNms) {
    	
    	for(int i = 0; i < headerNms.length; i++) {
    		Cell newCell = row.createCell(i);
    		newCell.setCellStyle(headStyle);
    		newCell.setCellValue(headerNms[i]);
    	}
    }

    // 데이터영역 스타일
    private void setBodyStyle(CellStyle bodyStyle) {
        bodyStyle.setBorderTop(BorderStyle.THIN);
        bodyStyle.setBorderBottom(BorderStyle.THIN);
        bodyStyle.setBorderLeft(BorderStyle.THIN);
        bodyStyle.setBorderRight(BorderStyle.THIN);
    }

    // 데이터영역 생성
    private void setBody(Row row, Cell cell, CellStyle bodyStyle, Object vo, String[] exportColumn) throws Exception {
    	 
    	 Class<?> c = vo.getClass();
    	 int j = 0;
    	 for(int i = 0; i < exportColumn.length; i++) {
    		 String column = exportColumn[i];
    		 column = "get"+column.substring(0,1).toUpperCase() + column.substring(1);
    		 
    		 Method method = c.getMethod(column);
    		 
    		 Object data = method.invoke(vo);
    		 
    		 if(data != null) {
        		 
            	 String type = data.getClass().getName();
            	 
            	 if(!"java.lang.String".equals(type) && !"java.lang.Integer".equals(type)) {
            		 continue;
            	 }
            	 
            	 Cell newCell = row.createCell(i);
            	 newCell.setCellStyle(bodyStyle);
            	 if("java.lang.String".equals(type)) {
            		 newCell.setCellValue((String)data);
            	 }else if("java.lang.Integer".equals(type)) {
            		 newCell.setCellValue((int)data);
            	 }
            	 j++;
        	 }
    	 }
    }
    
    public static Map<String, Object> voToMap(Object vo, String[] arrExceptList) throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();
        BeanInfo info = Introspector.getBeanInfo(vo.getClass());
        for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
            Method reader = pd.getReadMethod();
            if (reader != null) {
                if(arrExceptList != null && arrExceptList.length > 0 && isContain(arrExceptList, pd.getName())) continue;
                result.put(pd.getName(), reader.invoke(vo));
            }
        }
        return result;
    }
    
    public static Boolean isContain(String[] arrList, String name) {
        for (String arr : arrList) {
            if (StringUtils.contains(arr, name))
                return true;
        }
        return false;
    }
}
