package com.hanafn.openapi.portal.util;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.service.ISecurity;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Qualifier("base")
public class CryptoUtilBase implements ISecurity{

    @Value("${client.secret.iv}")
    private static String iv;
    private static String key;

    @Value("${client.secret.iv}")
    public void setIv (String value) {
        this.iv = value;
    }

    @Value("${client.secret.key}")
    public void setKey (String value) {
        this.key = value;
    }
    
    @Override
    public String encrypt(String str) throws GeneralSecurityException, UnsupportedEncodingException, BusinessException {
        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(), "AES");

        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv.getBytes()));
        byte[] encrypted = c.doFinal(str.getBytes("UTF-8"));
        String enStr = new String(Base64.getEncoder().encode(encrypted));

        return enStr;
    }
    
    @Override
    public String encrypt(String key, String str) throws GeneralSecurityException, UnsupportedEncodingException {

        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(), "AES");

        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv.getBytes()));
        byte[] encrypted = c.doFinal(str.getBytes("UTF-8"));
        String enStr = new String(Base64.getEncoder().encode(encrypted));

        return enStr;
    }
    
    @Override
    public String decrypt(String str) throws GeneralSecurityException, UnsupportedEncodingException {
        String rtn = str;
        try {
            SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(), "AES");

            Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
            c.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv.getBytes()));
            byte[] byteStr = Base64.getMimeDecoder().decode(str.getBytes());
            log.debug("★strLength:[" + str.length() + "]\n★str:" + str);
            log.debug("★iv:" + iv);
            rtn = new String(c.doFinal(byteStr), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("aes256 decrypt error..", e);
        } catch (Exception e) {
            log.error("aes256 decrypt error..", e);
        }
        return rtn;
    }

    @Override
    public String decrypt(String key, String str) throws GeneralSecurityException, UnsupportedEncodingException {

        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(), "AES");

        log.debug("★key: " + key);
        log.debug("★iv: " + iv);

        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv.getBytes()));

        byte[] byteStr = Base64.getDecoder().decode(str.getBytes());
        return new String(c.doFinal(byteStr), "UTF-8");
    }
}
