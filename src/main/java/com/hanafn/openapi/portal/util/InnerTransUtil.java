package com.hanafn.openapi.portal.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanafn.openapi.portal.admin.views.repository.CommRepository;
import com.hanafn.openapi.portal.exception.BusinessException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class InnerTransUtil {
	
	@Autowired
    MessageSourceAccessor messageSource;
	
	@Value("${bt.tran.yn}")
   	private String btTranYn;
	
	@Value("${bt.tran.ip}")
   	private String btServerIp;
	
	@Autowired
    CommonUtil commonUtil;
	
	@Autowired
	CommRepository commRepository;
	
	public Map<String, Object> sendBt(String uri, Map<String, Object> sendData) {
		Map<String, Object> resultMap = null;
		
		if("true".equals(btTranYn)) {
			
    		ObjectMapper om = new ObjectMapper();
    		
    		BufferedReader br = null;
			HttpURLConnection hcon = null;
			URL url;
			StringBuffer result = new StringBuffer();
			
	        String conUrl = btServerIp + uri;
			
			try {
				String jsonParam = om.writeValueAsString(sendData);
				url = new URL(conUrl);
				hcon = (HttpURLConnection) url.openConnection();
				
				if(hcon != null) {
					hcon.setRequestMethod("POST");
					hcon.setDoOutput(true);              //항상 갱신된내용을 가져옴.
					hcon.setRequestProperty("Content-Type", "application/json");
					
					String guid = commonUtil.getGuid();
					hcon.setRequestProperty("guid", guid);
					
					log.info("----------------------------------------------------------------------------------------------------------------");
					
					log.info("header start-------------------------------------------------------------------");
					log.info("Content-Type : application/json");
					log.info("guid : " + guid);
					log.info("header end-------------------------------------------------------------------");
					
					OutputStream os;
					log.info("jsonParam start-------------------------------------------------------------------");
					log.info(jsonParam);
					log.info("jsonParam end---------------------------------------------------------------------");

					os = hcon.getOutputStream();
					os.write(jsonParam.getBytes());
					os.flush();

					int code = hcon.getResponseCode();
					String message = hcon.getResponseMessage();
					
					log.info("result start-------------------------------------------------------------------");
					log.info(code + " : " + message);
					log.info("result end---------------------------------------------------------------------");

					br = new BufferedReader(new InputStreamReader(hcon.getInputStream()));

					String temp;
					while ((temp = br.readLine()) != null) {
						result.append(temp);
					}
					
					log.info("result body start-------------------------------------------------------------------");
					log.info(result.toString());
					log.info("result body end---------------------------------------------------------------------");
					
					resultMap = om.readValue(result.toString(), Map.class);
					
					log.info("----------------------------------------------------------------------------------------------------------------");
					if(code != 200) {
						log.error(resultMap.get("rsp_code") + " : " + messageSource.getMessage("E026")+"("+resultMap.get("rsp_code") + ":" + resultMap.get("rsp_msg")+")");
						throw new BusinessException(resultMap.get("rsp_code")+"", messageSource.getMessage("E026")+"("+resultMap.get("rsp_code") + ":" + resultMap.get("rsp_msg")+")");
					}
				}
			} catch (JsonProcessingException e) {
				log.error(e.getMessage());
				throw new BusinessException(e.getMessage());
			} catch (MalformedURLException e) {
				log.error(e.getMessage());
				throw new BusinessException(e.getMessage());
			} catch (IOException e) {
				log.error(e.getMessage());
				throw new BusinessException(e.getMessage());
			}
		}
		return resultMap;
	}
}
