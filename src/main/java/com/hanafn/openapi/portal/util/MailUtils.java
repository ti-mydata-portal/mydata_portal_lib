package com.hanafn.openapi.portal.util;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Logger;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.hanafn.openapi.portal.admin.views.repository.CommRepository;
import com.hanafn.openapi.portal.admin.views.vo.MailSmsVO;
import com.hanafn.openapi.portal.util.service.ISecurity;


@Component
public class MailUtils {

    private MimeMessage message;
    private MimeMessageHelper messageHelper;
    @Value("${header-img-url}")
    private String headerUrl;
    @Value("${footer-img-url}")
    private String footerUrl;

    @Value("${spring.profiles.active}")
    private String thisServer;
    
    @Value("${mail.master.email}")
    private String masterMail;
    
    @Value("${sms.master.number}")
    private String masterSms;
    
    @Autowired
    public JavaMailSender mailSender;
    
    @Autowired
	CommRepository commRepository;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity aes256Util;
    
    private final String DIV_STRING = "||";

    public void initMail() {
//        try {
//            message = this.mailSender.createMimeMessage();
//            messageHelper = new MimeMessageHelper(message, true, "UTF-8");
//        }catch(Exception e) {
//            e.printStackTrace();
//            log.error("MailUtils initMail Error: " + e.toString());
//            throw new RuntimeException("MailUtils initMail Error", e.getCause());
//        }
    }

    public void setSubject(String subject){
//        try {
//            messageHelper.setSubject(subject);
//        }catch(Exception e) {
//            log.error("MailUtils setSubject Error: " + e.toString());
//            throw new RuntimeException("MailUtils setSubject Error", e.getCause());
//        }
    }

    public void setText(String content) {
//        try {
//            messageHelper.setText(content, true);
//        }catch(Exception e) {
//            log.error("MailUtils setText Error: " + e.toString());
//            throw new RuntimeException("MailUtils setText Error", e.getCause());
//        }
    }

    public void setFrom(String email, String name) {
//        try {
//            messageHelper.setFrom(email, name);
//        }catch(Exception e) {
//            log.error("MailUtils setFrom Error: " + e.toString());
//            throw new RuntimeException("MailUtils setFrom Error", e.getCause());
//        }
    }

    public void setTo(String email) {
//        try {
//            String dec = "";
//            if (thisServer.equals("production")) {
//                dec = AES256Util.decrypt(email);
//            }
//
//            messageHelper.setTo(dec);
//        }catch(Exception e) {
//            log.error("MailUtils setTo Error: " + e.toString());
//            throw new RuntimeException("MailUtils setTo Error", e.getCause());
//        }
    }

    public void addInline() {
//        try {
//            FileSystemResource mailHeaderLogo = new FileSystemResource(new File(headerUrl));
//            messageHelper.addInline("mail_logo.png", mailHeaderLogo);
//            FileSystemResource mailFooterLogo = new FileSystemResource(new File(footerUrl));
//            messageHelper.addInline("mail_footer.png", mailFooterLogo);
//        }catch(Exception e) {
//            log.error("MailUtils addInline Error: " + e.toString());
//            throw new RuntimeException("MailUtils addInline Error", e.getCause());
//        }
    }

    public void send() {
//        try {
//            mailSender.send(message);
//        }catch(Exception e) {
//            log.error("MailUtils send Error: " + e.toString());
//            throw new RuntimeException("MailUtils send Error", e.getCause());
//        }
    }

    public String makeAuthNum() {

        Random rand = new Random();
        String numStr = "";

        for(int i=0;i<6;i++) {
            String ran = Integer.toString(rand.nextInt(10));
            numStr += ran;
        }

        return numStr;
    }
    
    public void makeMailData(List<MailSmsVO> list, boolean decodeEmail) throws Exception {
    	Map<String, StringBuffer> mail = new HashMap<String, StringBuffer>();
    	
    	for(MailSmsVO vo : list) {
    		String mailType = vo.getMailType();
    		StringBuffer mailSb = null;
    		if(mail.containsKey(mailType)) {
    			mailSb =  mail.get(mailType);
    		} else {
    			mailSb = new StringBuffer();	
    			mail.put(mailType, mailSb);
    		}
    		
    		if(vo.getTagmap001() == null || "".equals(vo.getTagmap001())) {
    			vo.setTagmap001(masterMail);
    		}
			
			if(vo.getSenddate() == null || "".equals(vo.getSenddate())) {
				String sendDate = DateUtil.formatDateTime("yyyy-MM-dd HH:mm:ss");
				vo.setSenddate(sendDate);
			}
    		
			//mailSb.append(voToString(vo)+"\n");
			voToStrinEmail(vo);
			
			String userEmail = vo.getEmail();
			
			if(decodeEmail) {
				userEmail = aes256Util.encrypt(vo.getEmail());
			}
			
			vo.setEmail(userEmail);
			
			commRepository.insertMailhisLog(vo);
    	}
	    	
//	    	for(String key : mail.keySet()) {
//	    		StringBuffer sb = mail.get(key);
//	    		
//	    		String fileName = this.makeAuthNum()+"_"+key;
//	    		File file = new File(mailFilePath+fileName+".txt");
//	    		
//	    		writer = new BufferedWriter(new FileWriter(file));
//	    	    writer.write(sb.toString());
//	    	}
    }
    
    public void makeSmsData(List<MailSmsVO> list, boolean decodeNumber) throws Exception {
    	Map<String, StringBuffer> sms = new HashMap<String, StringBuffer>();
    	
    	for(MailSmsVO vo : list) {
    		if(vo.getTrCallback() == null || "".equals(vo.getTrCallback())) {
    			vo.setTrCallback(masterSms);
    		}
			
			if(vo.getTrSenddate() == null || "".equals(vo.getTrSenddate())) {
				String sendDate = DateUtil.formatDateTime("yyyy-MM-dd HH:mm:ss");
				vo.setTrSenddate(sendDate);
			}
    		
			voToStrinSms(vo);
			
			String userPhoneNum = vo.getTrPhone();
			
			if(decodeNumber) {
				userPhoneNum = aes256Util.encrypt(userPhoneNum);
			}
			
			vo.setTrPhone(userPhoneNum);
			
			commRepository.insertSmshisLog(vo);
    	}
    }
    
    public void makeSmsData(List<MailSmsVO> list) throws Exception {
    	this.makeSmsData(list, true);
    }
   
    public void makeMailData(List<MailSmsVO> list) throws Exception {
    	this.makeMailData(list, true);
    }
    
    private String voToStrinSms(MailSmsVO vo) throws Exception{
    	String[] columns = {"trType", "trPhone", "trCallback", "trSenddate", "trSubject","trMsg"};
    	String result = this.voToString(vo, columns);
    	return result;
    }
    
    private String voToStrinEmail(MailSmsVO vo) throws Exception{
    	String[] columns = {"trType","mailType", "email", "senddate","tagmap001", "tagmap002", "tagmap003", "tagmap004", "tagmap005", "tagmap006", "tagmap007", "tagmap008", "tagmap009", "tagmap010", "tagmap011", "tagmap012", "tagmap013", "tagmap014", "tagmap015", "tagmap900", "tagmap901"};
    	vo.setTrType("MAIL");
    	String result = this.voToString(vo, columns);
    	return result;
    }
    
    private String voToString(MailSmsVO vo, String[] columns) throws Exception{
    	Logger logger = Logger.getLogger(MailUtils.class.getName()); 
    	String result = "";
    	
    	
    	Class<?> c = vo.getClass();
    	
    	for(int i = 0; i < columns.length; i++) {
    		String column = columns[i];
	   		 column = "get"+column.substring(0,1).toUpperCase() + column.substring(1);
	   		 
	   		 Method method = c.getMethod(column);
	   		 
	   		 Object data = method.invoke(vo);
	   		 
	   		 if(data != null) {
	   			 result += (String)data +DIV_STRING;
	   		 }else {
	   			 result += DIV_STRING;
	   		 }
    	}
    	result = result.substring(0,result.length()-(1+DIV_STRING.length()));
    	
    	logger.info(result);
    	
    	return result;
    }
}
