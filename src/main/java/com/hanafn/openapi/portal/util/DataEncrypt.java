package com.hanafn.openapi.portal.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;

/**
 * nice 결제용 클래스
 * @author wonkison
 *
 */

@Slf4j
public class DataEncrypt {
	MessageDigest md;
	String strSRCData = "";
	String strENCData = "";
	String strOUTData = "";
	
	public DataEncrypt(){ }
	public String encrypt(String strData){
		String passACL = null;
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.reset();
			md.update(strData.getBytes());
			byte[] raw = md.digest();
			passACL = encodeHex(raw);
		} catch (NoSuchAlgorithmException e) {
			log.error("암호화 알고리즘 에러 " + e.toString());
		}catch(Exception e){
//			System.out.print("암호화 에러" + e.toString());
			log.error("암호화 에러 " + e.toString());
		}
		return passACL;
	}
	
	public String encodeHex(byte [] b){
		char [] c = Hex.encodeHex(b);
		return new String(c);
	}
}
