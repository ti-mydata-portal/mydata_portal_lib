package com.hanafn.openapi.portal.util;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.service.ISecurity;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Qualifier("Crypto")
public class CryptoUtil implements ISecurity{

	@Value("${crypto.interface}")
    private String serverDiv;

	@Autowired
	@Qualifier("base")
    ISecurity baseCrypto;
	
	@Autowired
	@Qualifier("other")
    ISecurity otherCrypto;
	
	private ISecurity getBean() {
		if(serverDiv.equals("base")) {
			return baseCrypto;
		}else {
			return otherCrypto;
		}
	}
    
    @Override
    public String encrypt(String str) throws Exception {
        return this.getBean().encrypt(str);
    }
    
    @Override
    public String encrypt(String key, String str) throws Exception {
    	return this.getBean().encrypt(key, str);
    }
    
    @Override
    public String decrypt(String str) throws Exception {
    	return this.getBean().decrypt(str);
    }

    @Override
    public String decrypt(String key, String str) throws Exception {
    	return this.getBean().decrypt(key, str);
    }
}
