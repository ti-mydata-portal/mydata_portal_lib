package com.hanafn.openapi.portal.util;

import com.hanafn.openapi.portal.exception.BusinessException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class PasswordValidator {

    /**
     * 비밀번호 정규식 체크
     *
     * @param newPwd
     * @return
     */
    public void isValidPassword(String newPwd, String userId) {
        boolean chk = true;

        Pattern p = Pattern.compile("^(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!%*#?&])[A-Za-z[0-9]$@$!%*#?&]{8,}$");
        Matcher m = p.matcher(newPwd);

        Pattern p2 = Pattern.compile("(\\w)\\1\\1\\1");
        Matcher m2 = p2.matcher(newPwd);

        Pattern p3 = Pattern.compile("([\\{\\}\\[\\]\\/?.,;:|\\)*~`!^\\-_+<>@\\#$%&\\\\\\=\\(\\'\\\"])\\1\\1\\1");
        Matcher m3 = p3.matcher(newPwd);

        if(space(newPwd)){	                    // 패스워드 공백 문자열 체크
            chk = false;
        }else if(!m.find()){	                // 정규식 이용한 패턴 체크
            chk = false;
        }else if(m2.find() || m3.find()){	    // 동일 문자 4번 입력 시 패턴 체크
            chk = false;
        }else if(continuous(newPwd)){	        // 비밀번호 연속 숫자 4자리 체크
            chk = false;
        } else if(StringUtils.isNotEmpty(userId)){
            if(sameId(newPwd, userId)){
                chk = false;
            }
        }

        if(!chk) {
            throw new BusinessException("비밀번호가 올바르지 않습니다.");
        }
    }

    /**
     * 공백 문자 체크
     *
     * @param spaceCheck
     * @return
     */
    public boolean space(String spaceCheck){
        for(int i = 0 ; i < spaceCheck.length() ; i++) {
            if(spaceCheck.charAt(i) == ' ')
                return true;
        }

        return false;
    }

    /**
     * 연속된 숫자 체크
     *
     * @param pwd
     * @return
     */
    public boolean continuous(String pwd) {
        int o = 0;
        int d = 0;
        int p = 0;
        int n = 0;
        int limit = 4;

        for (int i = 0; i < pwd.length(); i++) {
            char tempVal = pwd.charAt(i);
            if (i > 0 && (p = o - tempVal) > -2 && (n = p == d ? n + 1 : 0) > limit - 3) {
                return true;
            }
            d = p;
            o = tempVal;
        }

        return false;
    }

    /**
     * 아이디와 동일 문자 4자리 체크
     *
     * @param pwd
     * @param id
     * @return
     */
    public boolean sameId(String pwd, String id) {
        for (int i = 0; i < pwd.length() - 3; i++) {
            if (id.contains(pwd.substring(i, i + 4))) {
                return true;
            }
        }
        return false;
    }
}
