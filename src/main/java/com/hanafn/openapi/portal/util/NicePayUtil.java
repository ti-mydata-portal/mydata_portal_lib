package com.hanafn.openapi.portal.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Component;

import com.hanafn.openapi.portal.exception.BusinessException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class NicePayUtil {

    public String getyyyyMMddHHmmss(){
		SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyyMMddHHmmss");
		return yyyyMMddHHmmss.format(new Date());
	}
	
	public String connectToServer(String data, String reqUrl) throws Exception{
		HttpURLConnection conn 		= null;
		BufferedReader resultReader = null;
		PrintWriter pw 				= null;
		URL url 					= null;
		
		int statusCode = 0;
		StringBuffer recvBuffer = new StringBuffer();
		try {
			url = new URL(reqUrl);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(15000);
			conn.setReadTimeout(25000);
			conn.setDoOutput(true);

			pw = new PrintWriter(conn.getOutputStream());
			pw.write(data);
			pw.flush();

			statusCode = conn.getResponseCode();
			resultReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
			for (String temp; (temp = resultReader.readLine()) != null; ) {
				recvBuffer.append(temp).append("\n");
			}

			if (!(statusCode == HttpURLConnection.HTTP_OK)) {
				throw new Exception();
			}

			return recvBuffer.toString().trim();
		} catch (IOException ie) {
			return "9999";
		}catch (Exception e){
			return "9999";
		}finally{
			recvBuffer.setLength(0);
			
			try {
				if (resultReader != null) {
					resultReader.close();
				}
			} catch(NullPointerException e) {
				resultReader = null;
			}catch(Exception ex){
				resultReader = null;
			}
			
			try{
				if(pw != null) {
					pw.close();
				}
			} catch(NullPointerException e) {
				pw = null;
			}catch(Exception ex){
				pw = null;
			}
			
			try{
				if(conn != null) {
					conn.disconnect();
				}
			} catch(NullPointerException e) {
				conn = null;
			}catch(Exception ex){
				conn = null;
			}
		}
	}

	//JSON String -> HashMap 변환
	public static HashMap<?, ?> jsonStringToHashMap(String str) throws Exception{
		HashMap dataMap = new HashMap();
		JSONParser parser = new JSONParser();
		
		try {
			Object obj = parser.parse(str);
			JSONObject jsonObject = (JSONObject) obj;

			Iterator<String> keyStr = jsonObject.keySet().iterator();
			while (keyStr.hasNext()) {
				String key = keyStr.next();
				Object value = jsonObject.get(key);

				dataMap.put(key, value);
			}
		} catch (ParseException e) {
			throw new BusinessException("Json str to Hashmap error >> ", e.toString());
		}catch(Exception e){
			throw new BusinessException("Json str to Hashmap error >> ", e.toString());
		}
		return dataMap;
	}
}