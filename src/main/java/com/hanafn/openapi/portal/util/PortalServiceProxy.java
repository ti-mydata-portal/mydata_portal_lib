package com.hanafn.openapi.portal.util;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value="mydata-portal")
public interface PortalServiceProxy {
	@RequestMapping(value = "/auth/hfnLoginEureka", method = RequestMethod.POST)
	public String hfnLoginEureka(@RequestBody Map<String, String> params);

}


