package com.hanafn.openapi.portal.util;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient(value="oauth")
public interface AuthServiceProxy {

	@RequestMapping(value = "/oauth/check_token", method = RequestMethod.POST)
	public String check_token(@RequestParam("token") String token);
	
	
	@RequestMapping(value = "/oauth/token/revokeById/{token}", method = RequestMethod.POST)
	public void revokeToken(@PathVariable("token") String token);

	@RequestMapping(value = "/oauth/api/client", method = RequestMethod.POST)
	public String checkClient(@RequestBody Map<String, Object> parameters);
	
	@RequestMapping(value = "{url}", method = RequestMethod.POST)
	public String regService(@PathVariable("url") String url, @RequestHeader Map<String,Object> header, @RequestBody Map<String,Object> body);
}


