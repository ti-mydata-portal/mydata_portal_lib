package com.hanafn.openapi.portal.util;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value="md-svc")
public interface ApiServiceProxy {
	@RequestMapping(value = "{url}", method = RequestMethod.POST)
	public String apiCommunicaterPost(@PathVariable("url") String url, @RequestHeader Map<String,Object> header, @RequestBody Map<String,Object> body);
	
	@RequestMapping(value = "{url}", method = RequestMethod.GET)
	public String apiCommunicaterGet(@PathVariable("url") String url, @RequestHeader Map<String,Object> header, @RequestBody Map<String,Object> body);
}


