package com.hanafn.openapi.portal.util;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient(value="internal-gateway")
public interface IGServiceProxy {

	@RequestMapping(value = "{url}", method = RequestMethod.POST)
	public String regService(@PathVariable("url") String url, @RequestHeader Map<String,Object> header, @RequestBody Map<String,Object> body);
}


