package com.hanafn.openapi.portal.util;

import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.commons.lang.StringUtils;

public class LowerKeyMap extends ListOrderedMap {
	public Object put(Object key, Object value) {
        return super.put(StringUtils.lowerCase((String) key), value);
    }
}
