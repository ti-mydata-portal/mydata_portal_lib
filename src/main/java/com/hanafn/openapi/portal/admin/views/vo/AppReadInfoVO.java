package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.List;

@Data
@Alias("appRead")
public class AppReadInfoVO {

	private String appKey;
	private String appNm;
	private String userId;
	private String userKey;
	private String userTel;
	private String userEmail;
	private String userNm;
	private String regDttm;
	private String regUser;
	private String modDttm;
	private String modUser;
}
