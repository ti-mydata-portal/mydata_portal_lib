package com.hanafn.openapi.portal.admin.viewsv2.dto;

import com.hanafn.openapi.portal.admin.viewsv2.vo.PrdAplvListVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.PrdAplvVO;
import lombok.Data;

import java.util.List;

@Data
public class AplvRsponsePaging {
//    private int pageIdx;
//    private int pageSize;

    private String resultCode = "0000";
    private String message = "정상처리되었습니다.";

    private int totCnt;
//    private int selCnt;
    private List<PrdAplvListVO> list;
}
