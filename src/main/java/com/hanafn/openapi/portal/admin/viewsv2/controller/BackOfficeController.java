package com.hanafn.openapi.portal.admin.viewsv2.controller;

import java.io.UnsupportedEncodingException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvApprovalRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRsponsePaging;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoAppPrdAplvRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoAppPrdRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoAppPrdRsponse;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoAppsRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoAppsRsponsePaging;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoPrdApiRsponse;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoPrdRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoPrdRsponse;
import com.hanafn.openapi.portal.admin.viewsv2.dto.CommonResponse;
import com.hanafn.openapi.portal.admin.viewsv2.dto.PortalUserRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.PortalUserRsponse;
import com.hanafn.openapi.portal.admin.viewsv2.service.AplvService;
import com.hanafn.openapi.portal.admin.viewsv2.service.BoAppsService;
import com.hanafn.openapi.portal.admin.viewsv2.service.BoUserService;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.util.UUIDUtils;
import com.hanafn.openapi.portal.util.service.ISecurity;

import lombok.extern.slf4j.Slf4j;

/**
 * 1. 배치에서 가져오는 데이터는 배치가 최종 실행된 이후 데이터 조회.??
 * 2. 배치 데이터가 우선. (포털 통해 정보가 변경되었다고 해도.. 단 1번이 전제가 됨 - 최종 배치구동 시간 이후 기준..)
 */
@RestController
@RequestMapping("/bo/api")
@Slf4j
public class BackOfficeController extends AbstController{
    @Autowired
    BoUserService boUserService;

    @Autowired
    AplvService aplvService;
    @Autowired
    BoAppsService boAppsService;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity aes256Util;

    /**
     * 포털 사용자 목록 조회
     * @param currentUser
     * @param request
     * @return
     * @throws Exception
     */
    @PostMapping("/users")
    public ResponseEntity<?> userList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody PortalUserRequest request) throws Exception {
        //입력값 validation
        if (StringUtils.isEmpty(request.getSearchUserId()) &&
                StringUtils.isEmpty(request.getSearchUserNm())) {
            return ResponseEntity.ok(new CommonResponse("E801", messageSource.getMessage("E801")));
        }

        //기본값 설정
        if(request.getPageIdx() == 0)
            request.setPageIdx(request.getPageIdx() + 1);

        if(request.getPageSize() == 0){
            request.setPageSize(100);
        }

        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());

        if (request.getSearchUserNm() != null && !"".equals(request.getSearchUserNm())) {
            try {
                request.setSearchUserNm(aes256Util.encrypt(request.getSearchUserNm()));
            } catch (UnsupportedEncodingException e) {
                log.error(e.getMessage());
                return ResponseEntity.ok(new CommonResponse("E026",messageSource.getMessage("E026")));
            }
        }

        //서비스 호출
        try {
            PortalUserRsponse data = boUserService.selectUserListPaging(request);
            return ResponseEntity.ok(data);
        } catch (BusinessException bo) {
            return ResponseEntity.ok(new CommonResponse(bo.getErrorCode(), bo.getMessage()));
        } catch (Exception e) {
            log.error("사용자목록 조회 오류", e);
            return ResponseEntity.ok(new CommonResponse("E026",messageSource.getMessage("E026")));
        }
    }

    /**
     * 상품승인내역 조회
     * @param currentUser
     * @param request
     * @return
     */
    @PostMapping("/prdAplvs")
    public ResponseEntity<?> prdAplvs(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AplvRequest request) {
        //0.필수값 체크
        //입력값 validation
        if (StringUtils.isEmpty(request.getSearchFromDate()) ||
                StringUtils.isEmpty(request.getSearchToDate())) {
            return ResponseEntity.ok( new CommonResponse("E802", messageSource.getMessage("E802")));
        }

        try {
            //1. default 값 설정
            request.setSearchPrdCtgy(currentUser.getHfnCd());
            request.setProcId(currentUser.getUseorgKey());
            request.setProcUser(currentUser.getUsername());

            //기본 구분값은 앱-상품승인 대기
            if (StringUtils.isEmpty(request.getSearchAplvDivCd()))
                request.setSearchAplvDivCd("APPPRDREG");
            //기본 상태값은 승인대기
//            if (!StringUtils.isEmpty(request.getSearchAplvStatCd()) && request.getSearchAplvStatCd().equals("1"))
//                request.setSearchAplvStatCd("APPROVAL");
//            else
//                request.setSearchAplvStatCd("WAIT");

            String searchEtc = request.getSearchEtc();
            if (!StringUtils.isEmpty(searchEtc)) {
                try {
                    request.setSearchEtcEncrypted(aes256Util.encrypt(searchEtc));
                } catch (UnsupportedEncodingException e) {
                    log.error("검색조건 암호화 오류.", e);
                } catch (Exception e) {
                    log.error("검색조건 암호화 오류.", e);
                }
            }

            //2. paging 기본값
            if (request.getPageIdx() == 0)
                request.setPageIdx(request.getPageIdx() + 1);

            if (request.getPageSize() == 0) {
                request.setPageSize(100);
            }

            request.setPageOffset((request.getPageIdx() - 1) * request.getPageSize());

            //3.조회 서비스 호출
            AplvRsponsePaging data = aplvService.selectPrdAplvListPaging(request);

            return ResponseEntity.ok(data);
        } catch (BusinessException e) {
            log.error("승인목록 조회 오류", e);
            return ResponseEntity.ok(new CommonResponse("E026",messageSource.getMessage("E026")));
        } catch (Exception e) {
            log.error("승인목록 조회 오류", e);
            return ResponseEntity.ok(new CommonResponse("E026",messageSource.getMessage("E026")));
        }
    }

    /**
     * 앱목록 조회
     * @param currentUser
     * @param request
     * @return
     * @throws Exception
     */
    @PostMapping("/appListSelected")
    public ResponseEntity<?> appListSelected(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody BoAppsRequest request) throws Exception {
        //0.필수값 체크
        //입력값 validation
        if (StringUtils.isEmpty(request.getSearchFromDate()) ||
                StringUtils.isEmpty(request.getSearchToDate())) {
            return ResponseEntity.ok( new CommonResponse("E802", messageSource.getMessage("E802")));
        }

        if(request.getPageIdx() == 0) {
            request.setPageIdx(request.getPageIdx()+1);
        }
        if(request.getPageSize() == 0) {
            request.setPageSize(100);
        }

        request.setPageOffset((request.getPageIdx()-1) * request.getPageSize());
        try {
            BoAppsRsponsePaging data = boAppsService.appListSelected(request);
            return ResponseEntity.ok(data);
        } catch (BusinessException bo) {
            return ResponseEntity.ok(new CommonResponse(bo.getErrorCode(), bo.getMessage()));
        }

    }

    /**
     * 앱상품목록 조회
     * @param currentUser
     * @param request
     * @return
     * @throws Exception
     */
    @PostMapping("/appPrdList")
    public ResponseEntity<?> appPrdList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody BoAppsRequest request) throws Exception {
        try {
            BoAppPrdRsponse data = boAppsService.appPrdListSelected(request);
            return ResponseEntity.ok(data);
        } catch (BusinessException bo) {
            return ResponseEntity.ok(new CommonResponse(bo.getErrorCode(), bo.getMessage()));
        }
    }

    @PostMapping("/prdApiList")
    public ResponseEntity<?> prdApiList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AdminRequest.ProductRequest request) throws Exception {
        request.setPrdCtgy(currentUser.getHfnCd());

        try {
            BoPrdApiRsponse data = boAppsService.selectPrdApiList(request);
            return ResponseEntity.ok(data);
        } catch (BusinessException bo) {
            return ResponseEntity.ok(new CommonResponse(bo.getErrorCode(), bo.getMessage()));
        }
    }

    /**
     * 백오피스용 앱 등록
     * @param currentUser
     * @param request
     * @return
     */
    @PostMapping("/appReg")
    public ResponseEntity<?> appReg (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AdminRequest.AppsRegRequest request) {

        request.setAppKey(UUIDUtils.generateUUID());
        request.setUserKey(request.getUserKey());
        request.setRegUserId(currentUser.getUserKey());
        request.setAppMemo("");
        request.setHfnCd(request.getPrdCtgy()); //나이스 네이밍 맞추기 위한 작업

        try {
            boAppsService.insertApp(request);
            return ResponseEntity.ok(new CommonResponse("0000", "App registered successfully"));
        } catch (BusinessException bo) {
            return ResponseEntity.ok(new CommonResponse(bo.getErrorCode(), bo.getMessage()));
        }
    }

    /**
     * 앱상품추가
     * @param currentUser
     * @param request
     * @return
     */
    @PostMapping("/appPrdAdd")
    public ResponseEntity<?> appPrdAdd (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AdminRequest.AppsRegRequest request) {
        request.setRegUser(currentUser.getUsername());
        request.setRegUserId(currentUser.getUserKey());

        request.setHfnCd(currentUser.getHfnCd());
        try {
            boAppsService.addAppPrd(request);
            return ResponseEntity.ok(new CommonResponse("0000", "정상처리되었습니다."));
        } catch (BusinessException be) {
            return ResponseEntity.ok(new CommonResponse(be.getErrorCode(), be.getMessage()));
        }
    }

    /**
     * 앱상품정보 변경. ㅅ이틐토드, 계약번호, 청구번호, 맴핑처리자 정보 변경
     * @param currentUser
     * @param request
     * @return
     */
    @PostMapping("/appPrdModify")
    public ResponseEntity<?> appPrdModify (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AdminRequest.AppsRegRequest request) {
        request.setRegUser(currentUser.getUsername());
        request.setRegUserId(currentUser.getUserKey());
        request.setModUserId(currentUser.getUserKey());
        try {
            boAppsService.modifyAppPrd(request);
            return ResponseEntity.ok(new CommonResponse("0000", "정상처리되었습니다."));
        } catch (BusinessException be) {
            return ResponseEntity.ok(new CommonResponse(be.getErrorCode(), be.getMessage()));
        }
    }

    @PostMapping("/appPrdStatus")
    public ResponseEntity<?> updateAppPrdStatus(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody BoAppPrdAplvRequest request) {
        request.setRegUser(currentUser.getUsername());
        request.setRegUserId(currentUser.getUserKey());
        request.setModUserId(currentUser.getUserKey());
        try {
            boAppsService.updateAppPrdStatus(request);
            return ResponseEntity.ok(new CommonResponse("0000", "정상처리되었습니다."));
        } catch (BusinessException be) {
            return ResponseEntity.ok(new CommonResponse(be.getErrorCode(), be.getMessage()));
        }
    }

    @PostMapping("/appPrdRemainCnt")
    public ResponseEntity<?> appPrdRemainCnt(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody BoAppPrdRequest request) {
        request.setRegUser(currentUser.getUsername());
        request.setRegUserId(currentUser.getUserKey());
        request.setProdUserId(currentUser.getUserKey());
        try {
            boAppsService.appPrdRemainCnt(request);
            return ResponseEntity.ok(new CommonResponse("0000", "정상처리되었습니다."));
        } catch (BusinessException be) {
            return ResponseEntity.ok(new CommonResponse(be.getErrorCode(), be.getMessage()));
        }
    }

    /**
     * 앱상품목록 조회
     * @param currentUser
     * @param request
     * @return
     * @throws Exception
     */
    @PostMapping("/prdList")
    public ResponseEntity<?> prdList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody BoPrdRequest request) throws Exception {
        if (StringUtils.isEmpty(request.getSearchPrdCtgy())) {
            return ResponseEntity.ok(new CommonResponse("E803", messageSource.getMessage("E803")));
        }

        BoPrdRsponse data = boAppsService.prdList(request);
        return ResponseEntity.ok(data);
    }

    // 결재자가 승인버튼 클릭시 처리
    @PostMapping("/prodAplvApproval")
    public ResponseEntity<?> aplvApproval(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AplvApprovalRequest request) throws Exception {

        request.setProcUser(currentUser.getUsername());
        request.setProcId(currentUser.getUserKey());
        request.setPrdCtgy(currentUser.getHfnCd());

        try {
            aplvService.aplvApproval(request);
            return ResponseEntity.ok(new CommonResponse("0000", "정상처리되었습니다."));
        } catch (BusinessException be) {
            return ResponseEntity.ok(new CommonResponse(be.getErrorCode(), be.getMessage()));
        }
    }

    @PostMapping("/prodAplvReject")
    public ResponseEntity<?> aplvReject(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AplvApprovalRequest request) throws Exception {

        request.setProcUser(currentUser.getUsername());
        request.setProcId(currentUser.getUserKey());
        request.setPrdCtgy(currentUser.getHfnCd());
        try {
            aplvService.aplvReject(request);
            return ResponseEntity.ok(new CommonResponse("0000", "정상처리되었습니다."));
        } catch (BusinessException be) {
            return ResponseEntity.ok(new CommonResponse(be.getErrorCode(), be.getMessage()));
        }
    }


//    public static void main (String[] args) {
//        try {
////            System.out.println(AES256Util.encrypt("김지환"));
//
//
//
//
//System.out.println(genRandomHexString(16));
//            System.out.println(genRandomHexString(32));
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    public static String genRandomHexString(int len) {
//        Random random = new Random();
//        StringBuilder sb = new StringBuilder();
//        while (sb.length() < len) {//32byte
//            sb.append(Integer.toHexString(random.nextInt()));
//        }
//        return sb.toString();
//    }
}
