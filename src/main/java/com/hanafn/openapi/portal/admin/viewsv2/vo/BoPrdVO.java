package com.hanafn.openapi.portal.admin.viewsv2.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("boprd")
public class BoPrdVO {
    private String prdId;   //상품코드
    private String prdCtgy; //카테고리
    private String prdKind; //상품종류
    private String prdNm;   //상품명
    private String serviceCd;   //서비스코드
}