package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.List;

@Data
@Alias("api")
public class ApiVO {

	private String apiId;
	private String apiNm;
	private String apiStatCd;
	private String scope;
	private String apiType;
	private String apiIndustry; 
	private String ctgrCd;
	private String subCtgrCd;
	private String ctgrNm;
	private String apiSvc;
	private String apiVer;
	private String apiVerYn;
	private String apiUri;
	private String apiUrl;
	private String apiMthd;
	private String apiCtnt;
	private String apiHtml;
	private String apiPubYn;
	private String apiEncYn;
	private String apiProcType;
	private String apiAuthType;
	private String apiContentProvider;
	private String cnsntIngrpNm;
	private String cnsntIngrpCd;
	private String cnsntIntypCd;
	private String gwType;
	private String msgType;
	private String encodingType;
	private String hfnSvcCd;
	private String apiProcUrl;
	private String apiTosUrl;
	private String userKey;
	private String useorgNm;
	private String appUseCnt;
	private String useorgUseCnt;
	private String dlyTermDiv;
	private String dlyTermDt;
	private String dlyTermTm;
	private String useFl;
	private String cnlKey;
	private String hfnCd;
	private String baseRateYn;
	private String apiRscdYn;
	private String apiRsKey;
	private String apiRsValue;
	private String regDttm;
	private String regUser;
	private String procDttm;
	private String procUser;
	private String parentId;
    private String httpPrtcl;
    private String httpMethod;
    private String httpUrl;
    private String httpCtout;
    private String httpRtOut;
    private String tcpIp;
    private String tcpType;
    private String tcpPort;
    private String tcpSvc;
    private String tcpCtOut;
    private String tcpRtOut;
    private String tcpEnc;
    private String checkSchema;

	private String commCdId;
	private String cdId;
	private String cdNm;
	private String sampleResult;
	private String apiBtId;
	private String apiTransCycle;
	private String contentType;

	private String prdCd;

	private List<ApiTagVO> apiTagList;
}
