package com.hanafn.openapi.portal.admin.viewsv2.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AplvRequest {
    private String searchHfnCd;
    private String searchFromDate;
    private String searchToDate;
    private String searchPrdCtgy;       //서비스카테고리
    private String searchPrdKind;       //상품구분 (10:선납자동승인, 11: 선납백오피스승인, 21: 후납상품)
    private String searchCopNm;         //사업자명
    private String searchCopRegNo;      //사업자번호
    private String searchUserId;        //등록자ID
    private String searchAppNm;         //앱명
    private String searchServiceCd;     //서비스코드
    private String searchMppCd1;        //사이트코드
    private String searchMppCd2;        //계약번호
    private String searchMppCd3;        //청구번호
    private String searchMppUser;       //매핑처리담당자
    private String searchAplvDivCd;     //분류
    private String searchAplvStatCd;    //승인상태
    private String searchEtc;           //상품명/요청자
    private String searchEtcEncrypted;  //암호화 검색조건
    private String searchUseFl;			//사용여부

    protected int pageIdx = 0;
    protected int pageSize = 0;
    protected int pageOffset = 0;

    protected String regUser = "";
    protected String procUser = "";
    protected String procId = "";

    protected String appKey = "";
    protected String trnscCall = "";      //거래호출구분
}
