package com.hanafn.openapi.portal.admin.views.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.QnaRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminResponse;
import com.hanafn.openapi.portal.admin.views.dto.AdminResponse.QnaResponse;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.vo.HfnUserVO;
import com.hanafn.openapi.portal.admin.views.vo.QnaVO;
import com.hanafn.openapi.portal.admin.views.vo.UserPwHisVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.service.ISecurity;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class AdminService {
	
	@Autowired
    MessageSourceAccessor messageSource;
	
	@Autowired
    AdminRepository adminRepository;
	
	@Autowired
    CommonUtil commonUtil;
	
	@Autowired
    PasswordEncoder passwordEncoder;
	
	@Autowired
	@Qualifier("Crypto")
    ISecurity aes256Util;
	
	/*******************************유효성 검사*******************************/
	public void passwordCheck(String password){

		if(!password.matches(".*[a-zA-Z].*")){
			throw new BusinessException("E006",messageSource.getMessage("E006"));
		}

		if(!password.matches(".*[0-9].*")){
			throw new BusinessException("E007",messageSource.getMessage("E007"));
		}

		if(password.length() < 8){
			throw new BusinessException("E008",messageSource.getMessage("E008"));
		}
	}
	
	public void updateHfnUserPwd(AdminRequest.HfnUserPwdUpdateRequest request) {
		adminRepository.updateHfnUserPwd(request);
		adminRepository.setHfnUserPwdHis(request);
	}
	
	/*** QNA 관련 service ***/
	public QnaResponse selectQnaList(QnaRequest qnaRequest) {

		if(qnaRequest.getPageIdx() == 0) {
			qnaRequest.setPageIdx(qnaRequest.getPageIdx()+1);
		}

		if(qnaRequest.getPageSize() == 0) {
			qnaRequest.setPageSize(20);
		}

		qnaRequest.setPageOffset((qnaRequest.getPageIdx()-1) * qnaRequest.getPageSize());

		int totCnt = adminRepository.countQna(qnaRequest);
		List<QnaVO> qnaList = adminRepository.selectQnaList(qnaRequest);

		// 복호화
		for (QnaVO qnaVO : qnaList) {
			try {
				// 이름
				if (qnaVO.getUserNm() != null && !"".equals(qnaVO.getUserNm())) {
					String decrypedUserNm = aes256Util.decrypt(qnaVO.getUserNm());
					qnaVO.setUserNm(decrypedUserNm);
					log.debug("복호화 - 이름: {}", decrypedUserNm);
				}
			} catch (UnsupportedEncodingException e) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch ( Exception e ) {
				log.error(e.toString());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}

			try {
				// 전화번호
				if (qnaVO.getUserTel() != null && !"".equals(qnaVO.getUserTel())) {
					String decrypedUserTel = aes256Util.decrypt(qnaVO.getUserTel());
					qnaVO.setUserTel(decrypedUserTel);
					log.debug("복호화 - 전화번호: {}", decrypedUserTel);
				}
			} catch (UnsupportedEncodingException e) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch ( Exception e ) {
				log.error(e.toString());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}
		}

		QnaResponse qnaResponse = new QnaResponse();
		qnaResponse.setTotCnt(totCnt);
		qnaResponse.setSelCnt(qnaList.size());
		qnaResponse.setPageIdx(qnaRequest.getPageIdx());
		qnaResponse.setPageSize(qnaRequest.getPageSize());
		qnaResponse.setQnaList(qnaList);
		return qnaResponse;
	}
	
	public void updateHfnUser(AdminRequest.HfnUserUpdateRequest request){

		AdminRequest.HfnUserRegistRequest hfnUserRegistRequest = new AdminRequest.HfnUserRegistRequest();
        hfnUserRegistRequest.setHfnId(request.getHfnId());
        hfnUserRegistRequest.setHfnCd(request.getHfnCd());
        hfnUserRegistRequest.setRegUserId(request.getRegUser());

        HfnUserVO hfnUserInfo = adminRepository.selectHfnUserForId(hfnUserRegistRequest);
        request.setUserKey(hfnUserInfo.getUserKey());

        if (request.getNewUserPwd() != null && !StringUtils.equals(request.getNewUserPwd(), "")) {
            passwordCheck(request.getUserPwd());
            passwordCheck(request.getNewUserPwd());

            // 비밀번호 검증
            List<UserPwHisVO> userList = adminRepository.getUserIdPw(request.getUserKey());

            if (userList != null) {
                for (UserPwHisVO userData : userList) {
                    if(!commonUtil.compareWithShaStringsPw(request.getNewUserPwd(), userData.getUserPwd())) {
                        log.error("이전에 사용했던 비밀번호는 다시 사용할 수 없습니다.");
                        throw new BusinessException("E112", messageSource.getMessage("E112"));
                    }
                }
            } else {
                log.error(messageSource.getMessage("L005"));
                throw new BusinessException("L005", messageSource.getMessage("L005"));
            }

            hfnUserRegistRequest.setUserKey(request.getUserKey());
            hfnUserRegistRequest.setNewUserPwd(passwordEncoder.encode(request.getNewUserPwd()));
            request.setNewUserPwd(passwordEncoder.encode(request.getNewUserPwd()));
            adminRepository.insertHfnUserPwHis(hfnUserRegistRequest);
        }
        adminRepository.updateHfnUser(request);
        adminRepository.updateHfnUserRole(request);
    }
	
	/*********** 개인회원 비밀번호변경 *************/
    public void setUserPwd(AdminRequest.UserPwdUpdateRequest userPwdUpdateRequest){
        userPwdUpdateRequest.setUserPwd(passwordEncoder.encode(userPwdUpdateRequest.getUserPwd()));

        adminRepository.setUserPwd(userPwdUpdateRequest);
        adminRepository.setUserPwdLogin(userPwdUpdateRequest);
        adminRepository.setUserPwHis(userPwdUpdateRequest);

		// 비밀번호 변경시 로그인잠금 관련 세팅 초기화
		AdminRequest.UserLoginLockCheckRequest userLoginLockCheckRequest = new AdminRequest.UserLoginLockCheckRequest();
		userLoginLockCheckRequest.setUserId(userPwdUpdateRequest.getUserId());
		adminRepository.userLoginLockRelease(userLoginLockCheckRequest);
    }
    
    public AdminResponse.UserDupCheckResponse userEmailDupCheck(AdminRequest.UserDupCheckRequest userDupCheckRequest){
		try {
			if (userDupCheckRequest.getUserEmail() != null && !"".equals(userDupCheckRequest.getUserEmail())) {
				String encryptedUseorgUserEmail = aes256Util.encrypt(userDupCheckRequest.getUserEmail());
				userDupCheckRequest.setUserEmail(encryptedUseorgUserEmail);
				log.debug("암호화 - 이메일: {}", encryptedUseorgUserEmail);
			}
		} catch ( UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		} catch ( Exception e ) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		}

		int userEmailDupCheck = adminRepository.userEmailDupCheck(userDupCheckRequest);

		String userEmailDupYn = "N";

		if(userEmailDupCheck > 0){
			userEmailDupYn = "Y";
		}

		AdminResponse.UserDupCheckResponse data = new AdminResponse.UserDupCheckResponse();
		data.setUserEmailDupYn(userEmailDupYn);

		return data;
	}
    
    public AdminResponse.UserDupCheckResponse userEmailDupCheckUpdate(AdminRequest.UserDupCheckRequest userDupCheckRequest){
        try {
			if (userDupCheckRequest.getUserEmail() != null && !"".equals(userDupCheckRequest.getUserEmail())) {
				String encryptedUserEmail = aes256Util.encrypt(userDupCheckRequest.getUserEmail());
				userDupCheckRequest.setUserEmail(encryptedUserEmail);
				log.debug("암호화 - 이메일: {}", encryptedUserEmail);
			}
		} catch ( UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
        } catch ( Exception e ) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
        }

		int userEmailDupCheck = adminRepository.userEmailDupCheckUpdate(userDupCheckRequest);

		String userEmailDupYn = "N";

		if(userEmailDupCheck > 0){
			userEmailDupYn = "Y";
		}

		AdminResponse.UserDupCheckResponse data = new AdminResponse.UserDupCheckResponse();
		data.setUserEmailDupYn(userEmailDupYn);

		return data;
	}
}
