package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("mailSms")
public class MailSmsVO {

	private int seqNo; //자동생
	private String mailType;
	private String email;
	private String senddate;
	private String tagmap001;
	private String tagmap002;
	private String tagmap003;
	private String tagmap004;
	private String tagmap005;
	private String tagmap006;
	private String tagmap007;
	private String tagmap008;
	private String tagmap009;
	private String tagmap010;
	private String tagmap011;
	private String tagmap012;
	private String tagmap013;
	private String tagmap014;
	private String tagmap015;
	private String tagmap900;
	private String tagmap901;
	private String sendYn; //자동생
	
	private String trType;
	private String trPhone;
	private String trCallback;
	private String trSenddate;
	private String trSubject;
	private String trMsg;
	
	
}
