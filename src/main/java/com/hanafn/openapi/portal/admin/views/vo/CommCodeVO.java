package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("commCodeList")
public class CommCodeVO {

	private String commCdId;
	private String cdId;
	private String cdNm;
	private String cdEng;
	private int cdSort;
	private String cdDesc;
	private String cdUseNm;
	private String cdEtc;
	private String uprCdId;
	private String cdUseYn;
	private String conn1Cd;
	private String conn2Cd;
	private String conn3Cd;
	private String conn4Cd;
	private String conn5Cd;
	private String status;
	
}
