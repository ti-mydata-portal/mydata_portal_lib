package com.hanafn.openapi.portal.admin.views.dto;

import java.util.List;

import lombok.Data;

@Data
public class CommCodeRequest {
    private String commCdId;
    private String cdId;
    private String cdNm;
    private String cdEng;
    private String cdUseYn;
    private String uprCdId;
    private String conn1Cd;
    private String conn2Cd;
    private String conn3Cd;
    private String conn4Cd;
    private String conn5Cd;
    
    private List<CommInfoInsert> commInfoList;
    private List<CommCodeInsert> commCodeList;
    
    @Data
    public static class CommInfoInsert{
    	private String commCdId;
        private String commCdNm;
        private String commCdEng;
        private String cdUseYn;
        private String uprCdId;
        private String conn1Cd;
        private String conn2Cd;
        private String conn3Cd;
        private String conn4Cd;
        private String conn5Cd;
        private String cdDesc;
        private int cdSort;
        private String status;
    }
    
    @Data
    public static class CommCodeInsert{
    	private String commCdId;
        private String cdId;
        private String cdNm;
        private String cdEng;
        private String cdUseYn;
        private String uprCdId;
        private String conn1Cd;
        private String conn2Cd;
        private String conn3Cd;
        private String conn4Cd;
        private String conn5Cd;
        private String cdDesc;
        private int cdSort;
        private String status;
    }
    
    @Data
    public static class CommPrdReqeust {
    	private String prdCtgy;
    	private String prdCd;
    	private String prdNm;
    	private String openYn;
    	private String persYn;
    	private String serviceCd;
    	private String prdKind;
    	private String limitYn;
    }
    
    @Data
    public static class CommAppRequest {
    	private String appKey;
    	private String appNm;
    	private String hfnCd;
    	private String prdKind;
    	private String appSvcNm;
    	private String appSvcUrl;
    	private String copRegNo;
    	private String copNm;
    }
}
