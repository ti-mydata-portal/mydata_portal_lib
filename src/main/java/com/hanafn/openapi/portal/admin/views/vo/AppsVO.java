package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("apps")
public class AppsVO {

    /**
     * OPENAPI_PORTAL_APP_INFO 앱정보
     */

    private String appKey;
    private String appClientId;
    private String newAppClientId;
    private String appNm;
    private String appStatCd;
    private String appAplvStatCd;
    private String appScr;
    private String newAppScr;
    private String appSvcStDt;
    private String appSvcEnDt;
    private String appCtnt;
    private String userNm;
    private String userId;
    private String useorgNm;
    // TODO NICE XX
    // private String useorgId;
    private String userKey;
    private String dlDttm;
    private String termEtdYn;
    private String appScrReisuYn;
    private String regDttm;
    private String regUser;
    private String regUserNm;
    private String regId;
    private String modDttm;
    private String modUser;
    private String modUserNm;
    private String modId;
    private String appScrVldDttm;
    private String dispStatCd;
    private String appMemo;
    private String appAnyCnlYn;

    private String hfnCd;
    private String hfnNm;
    private String entrCd;
    private String encKey;
    private String regUserKey;

    private String userNmEncrypted;
    private String regUserNmEncrypted;
    private String modUserNmEncrypted;
    
    private String prdCtgyNm;
    private String prdKind;
    
    private String appSvcNm;
    private String appSvcUrl;
    
    private String copRegNo;
    private String copNm;
    private String ownNm;
    private String copAddr1;
    private String copAddr2;
    private String copAddrNo;
    private String bizType;
    private String bizCdtn;
    private String dirNm;
    private String dirTelNo1;
    private String dirTelNo2;
    private String dirEmail;
    private String dirDomain;
    
    private String managerNm;
    private String managerId;
    
    private String appDiv;
    
    private int totCnt;
    private int selCnt;
    private int pageIdx;
    private int pageSize;

}