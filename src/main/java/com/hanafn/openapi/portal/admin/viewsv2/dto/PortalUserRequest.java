package com.hanafn.openapi.portal.admin.viewsv2.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class PortalUserRequest {
    private String searchUserId;
    private String searchUserNm;

    private int pageIdx = 0;
    private int pageSize = 0;
    private int pageOffset = 0;
}
