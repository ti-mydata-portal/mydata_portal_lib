package com.hanafn.openapi.portal.admin.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.CommCodeVO;
import com.hanafn.openapi.portal.admin.views.vo.CommInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;

import lombok.Data;

@Data
public class CommCodeResponse {
    private int selCnt;
	private List<CommCodeVO> codeList;
	
	@Data
    public static class CommPrdResponse {
		private List<ProductVO> codeList;
    }
	
	@Data
    public static class CommAppResponse {
		private List<AppsVO> codeList;
    }
	
	@Data
	public static class CommInforesponse {
		private List<CommInfoVO> infoList;
	}
}