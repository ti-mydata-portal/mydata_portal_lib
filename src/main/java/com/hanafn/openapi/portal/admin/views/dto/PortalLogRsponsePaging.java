package com.hanafn.openapi.portal.admin.views.dto;

import lombok.Data;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.PortalLogVO;

@Data
public class PortalLogRsponsePaging {
    private int pageIdx;
    private int pageSize;
    
    private int totCnt;
    private int selCnt;
    private List<PortalLogVO> list;
}
