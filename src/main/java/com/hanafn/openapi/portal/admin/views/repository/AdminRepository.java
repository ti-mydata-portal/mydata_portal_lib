package com.hanafn.openapi.portal.admin.views.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ApiRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.StatsRequest;
import com.hanafn.openapi.portal.admin.views.dto.MenuRequest;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.HfnUserVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.admin.views.vo.QnaVO;
import com.hanafn.openapi.portal.admin.views.vo.StatsDayInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.UserPwHisVO;
import com.hanafn.openapi.portal.admin.views.vo.UserVO;

@Mapper
public interface AdminRepository {
    void insertWebPageAccessLog(MenuRequest menuRequest);
    
    HfnUserVO selectHfnUserForId(AdminRequest.HfnUserRegistRequest hfnUserRegistRequest);
    List<UserPwHisVO> getUserIdPw(String userId);
    
    List<UserPwHisVO> getHfnIdPw(AdminRequest.HfnUserPwdUpdateRequest request);
    
    UserVO selectHfnUserPwd(AdminRequest.UserDetailRequest userDetailRequest);
    void updateHfnUserPwd(AdminRequest.HfnUserPwdUpdateRequest request);
	void setHfnUserPwdHis(AdminRequest.HfnUserPwdUpdateRequest request);
	
	void insertHfnUserPwHis(AdminRequest.HfnUserRegistRequest hfnUserRegistRequest);
	void updateHfnUser(AdminRequest.HfnUserUpdateRequest hfnUserUpdateRequest);
	void updateHfnUserRole(AdminRequest.HfnUserUpdateRequest hfnUserUpdateRequest);
	
	List<QnaVO> selectQnaList(AdminRequest.QnaRequest qnaRequest);
	int countQna(AdminRequest.QnaRequest qnaRequest);
	
	List<ApiVO> selectApiList(AdminRequest.PrdApiRequest request);
	ProductVO selectPrdDetail(AdminRequest.ProductRequest request);
	AppsVO selectAppDetail(AdminRequest.AppsRequest appsRequest);
	
	void insertAppPrdInfo(ProductVO vo);
	void insertAppCopInfo(AdminRequest.AppsRegRequest appsRegRequest);
	
	void updateAppCnlInfo(AdminRequest.AppsCnlKeyRequest appsRequest);
	List<ApiVO> selectAppApiList(AdminRequest.ApiDetailRequest apiDetailRequest);
	
	void backupAppInfo(AdminRequest.AppsRegRequest appsRegRequest);
	void backupAppCopInfo(AdminRequest.AppsRegRequest appsRegRequest);
	void backupAppOwnerInfo(AdminRequest.AppsRegRequest appsRegRequest);
	
	void insertAplv(AdminRequest.AplvRegistRequest aplvRegistRequest);
	void insertAplv2(AdminRequest.AplvRegistRequest aplvRegistRequest);
	
	void insertAppCnlKey(AdminRequest.AppsCnlKeyRequest appsCnlKeyRequest);
	void insertAppInfoNiceAdmin(AdminRequest.AppsRegRequest appsRegRequest);
	
	void insertAppNewScrHis(AdminRequest.AppsScrRequest appsScrRequest);
	void appStatChange(AdminRequest.AppsRegRequest appsRegRequest);
	
	List<ApiVO> selectApiByHfnSvcCd(ApiRequest apiRequest);
	StatsDayInfoVO getStatsDayInfoVO(StatsRequest request);
	
	/** 개인회원 비밀번호 변경 **/
	void setUserPwd(AdminRequest.UserPwdUpdateRequest userPwdUpdateRequest);
	/** 개인회원 비밀번호 변경 **/
	void setUserPwdLogin(AdminRequest.UserPwdUpdateRequest userPwdUpdateRequest);
	void setUserPwHis(AdminRequest.UserPwdUpdateRequest request);
	void userLoginLockRelease(AdminRequest.UserLoginLockCheckRequest userLoginLockCheckRequest);
	int userIdDupCheck(AdminRequest.UserDupCheckRequest userDupCheckRequest);
	int userEmailDupCheck(AdminRequest.UserDupCheckRequest userDupCheckRequest);
	int userEmailDupCheckUpdate(AdminRequest.UserDupCheckRequest userDupCheckRequest);
	String hfnLoginCheck(AdminRequest.HfnLoginLockCheckRequest hfnLoginLockCheckRequest);
	int hfnLoginFailCntCheck(AdminRequest.HfnLoginLockCheckRequest hfnLoginLockCheckRequest);
	void hfnLoginFailCntSet(AdminRequest.HfnLoginLockCheckRequest hfnLoginLockCheckRequest);// 관리자 포탈 로그인 실패횟수 카운트 리턴
	void hfnLoginLockChange(AdminRequest.HfnLoginLockCheckRequest hfnLoginLockCheckRequest);
	void hfnLoginLockRelease(AdminRequest.HfnLoginLockCheckRequest hfnLoginLockCheckRequest);
	String userLoginLockYn(AdminRequest.UserLoginLockCheckRequest userLoginLockCheckRequest);
	UserVO getUserAuth(AdminRequest.UserLoginLockCheckRequest request);
	int userLoginFailCntCheck(AdminRequest.UserLoginLockCheckRequest userLoginLockCheckRequest);	// 이용자 포탈 로그인 실패횟수 카운트 리턴
	void userLoginFailCntSet(AdminRequest.UserLoginLockCheckRequest userLoginLockCheckRequest);		// 이용자 포탈 로그인 실패 시 카운트 세팅
	void userLoginLockChange(AdminRequest.UserLoginLockCheckRequest userLoginLockCheckRequest);
	
	String hfnLoginIpCheck(String hfnID);
	/** 로그인 - 인증정보조회 **/
	UserVO selectLoginCertMgnt(String userKey);
	String getPwHisDate(String userId);
}