package com.hanafn.openapi.portal.admin.viewsv2.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.core.GrantedAuthority;

import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.UserPrincipal;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstController {
    @Autowired
    protected MessageSourceAccessor messageSource;
    protected void isSuperAdmin(UserPrincipal currentUser) throws BusinessException {
        boolean isAdmin = false;
        //승인권한이 있는 사용자인지 확인.
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority().equals("ROLE_SYS_ADMIN")) {
                isAdmin = true;
                break;
            }
        }

        if (isAdmin == false)
            throw new BusinessException("C003",messageSource.getMessage("C003"));
    }

    protected void isAdmin(UserPrincipal currentUser) throws BusinessException {
        boolean isAdmin = false;
        //승인권한이 있는 사용자인지 확인.
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority().contains("ROLE_SYS_ADMIN") || ga.getAuthority().contains("ROLE_HFN_ADMIN")){
                isAdmin = true;
                break;
            }
        }

        if (isAdmin == false)
            throw new BusinessException("C003",messageSource.getMessage("C003"));
    }

}
