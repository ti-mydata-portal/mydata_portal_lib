package com.hanafn.openapi.portal.admin.viewsv2.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AplvHisRegistRequest {
        private String seqNo;
        private String rejectCtnt;

        @NotNull
        private String aplvSeqNo;
        @NotBlank
        private String aplvReqCd;
        @NotBlank
        private String regUserName;
        @NotBlank
        private String regUserId;
        @NotBlank
        private String hfnCd;
        @NotBlank
        private String aplvStatCd;

        protected String regUser = "";
        protected String procUser = "";
        protected String procId = "";
}
