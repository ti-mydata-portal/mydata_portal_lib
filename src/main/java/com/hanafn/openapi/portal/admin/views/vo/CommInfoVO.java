package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("commInfo")
public class CommInfoVO {

	private String commCdId;
	private String commCdNm;
	private String commCdEng;
	private int cdSort;
	private String cdDesc;
	private String cdUseYn;
	private String cdEtc;
	private String conn1Cd;
	private String conn2Cd;
	private String conn3Cd;
	private String conn4Cd;
	private String conn5Cd;
	private String status;
}
