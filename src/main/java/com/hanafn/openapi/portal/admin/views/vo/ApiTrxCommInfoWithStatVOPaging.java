package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;

import java.util.List;

@Data
public class ApiTrxCommInfoWithStatVOPaging {
    private int pageIdx;
    private int pageSize;

    private int totCnt;
    private int selCnt;
    private List<ApiTrxCommInfoWithStatVO> list;
}
