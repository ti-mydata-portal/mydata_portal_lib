package com.hanafn.openapi.portal.admin.viewsv2.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("portalUser")
public class PortalUserVO {
	private String userKey;
	private String userId;
	private String userNm;
	private String userEmail;
	private String userTel;
	private String userStatCd;
	private String userAppCnt;
	private String joinDate;
}
