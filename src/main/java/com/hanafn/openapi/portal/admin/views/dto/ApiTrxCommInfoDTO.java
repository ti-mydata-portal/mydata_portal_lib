package com.hanafn.openapi.portal.admin.views.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

public class ApiTrxCommInfoDTO {
    @Data
    public static class getDTO {
        private String seqNo;
    }

    @Data
    public static class getListDTO {
        private String searchHfnCd;
        private String searchHfnSvcCd;
        private String searchEntrCd;
        private String searchAppkey;
        private String searchStDt;
        private String searchEnDt;

        private int pageIdx = 0;
        private int pageSize = 20;
        private int pageOffset = 0;
    }

    @Data
    public static class getListWithStatDTO {
        private int pageIdx = 0;
        private int pageSize = 20;
        private int pageOffset = 0;

        private String searchHfnCd;
        private String searchHfnSvcCd;
        private String searchUseorgNm;
        private String searchAppNm;
        private String searchStDt;
        private String searchEnDt;
    }
}
