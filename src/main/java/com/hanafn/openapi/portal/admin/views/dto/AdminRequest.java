package com.hanafn.openapi.portal.admin.views.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.AppReadInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductApiInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;

import lombok.Data;

@Data
public class AdminRequest {
	
	@Data
    public static class HfnUserPwdUpdateRequest{
        private String userKey;
        @NotBlank
        private String userPwd;
        private String regUserName;
        private String regUserId;

        private String seq;
        private String newUserPwd;
        private String hfnCd;
        private String hfnId;
    }
	
	@Data
    public static class HfnUserUpdateRequest{
        @NotNull
        private String userKey;
        @NotBlank
        private String userNm;
        private String userPwd;
        private String newUserPwd;
        @NotBlank
        private String userStatCd;
        private String accessCd;
        private String deptNm;
        private String jobNm;
        private String signUserYn;
        private String userTel;
        private String ipAddr;
        private String regUserName;
        private String memo;

        // 사용자 유형
        private String roleCd;

        //결재선정보
        private String hfnCd;
        private String hfnId;
        private String altYn;

        //대직정보
        private String altId;
        private String altUserNm;
        private String signLevel;
        private String altStDate;
        private String altEnDate;
        private String regUser;
        private String exAltId;
        private String exAltStDate;
        private String exAltEnDate;

        private String seq;
        private String userEmail;
    }
	
	@Data
	public static class QnaRequest {
	    private String seqNo;
	    private String compNm;
	    private String statCd;
	    private String qnaType;
	    private String userKey;
	    private String userNm;
	    private String userTel;
	    private String apiNm;
	    private String hfnCd;
	    private String reqTitle;
	    private String reqCtnt;
	    private String uploadFile01;
	    private String uploadFile02;
	    private String userId;
	    private String regDttm;
	    private String answerYn;
	    private String answer;
	    private String modId;
	    private String modDttm;
	    private String prdCtgy;
	    private String searchUserId;
	    private int pageIdx;
	    private int pageSize;
	    private int pageOffset;

	    private String siteCd;
	}
	
	@Data
    public static class UserDetailRequest{
        @NotNull
        private String userKey;
        private String hfnCd;
        private String hfnId;
    }
	
	@Data
    public static class HfnUserRegistRequest{

        private String userKey;
        @NotBlank
        private String hfnId;
        private String hfnCd;
        @NotBlank
        private String userNm;
        //@NotBlank
        private String accessCd;
        @NotBlank
        private String userPwd;
        @NotBlank
        private String roleCd;    // 따로 관리필요 user_role_info 테이블(= roleCd)
        private String tmpPwd;
        private String jobNm;
        private String deptNm;
        private String signLevel;
        private String signUserYn;
        private String userTel;
        private String userEmail;
        private String ipAddr;
        private String regUserName;
        private String regUserId;

        private String seq;
        private String newUserPwd;
        private String memo;
    }
	
	@Data
	public static class ProductRequest {
	    private int prdId;
	    private String prdCd;
	    private String prdNm;
	    private String prdCtgy;
	    private String prdDesc;
	    private boolean iconChange;
	    private boolean fileChange;
	    private String iconPath;
	    private String filePath;
	    private String htmlCtnt;
	    private String persYn;
	    private String openYn;
	    private String scope;
	    private String grantType;
	    private String devHtmlCtnt;
	    private String prdKind;
	    private String serviceCd;
	    private int feeAmount;
	    private String applDoc;
	    private String limitYn;
	    private String prdMemo;
	    private String ctgrCd;
	    private String prdOrder;
	    private String regUser;
	    private String regDttm;
	    private String modUser;
	    private String modDttm;
	    private String siteCd;
	    private String batchMailYn;
	    
	    private String searchNm;

	    private int pageIdx = 0;
	    private int pageSize = 20;
	    private int pageOffset = 0;
	    private String searchPrd;

	    private List<ProductVO> prdUseList;
	    private List<ProductApiInfoVO> apiList;
	    private List<ProductApiInfoVO> exstnApiList;
	}
	
	@Data
	public static class AppsCnlKeyRequest {
	    private String seqNo;
	    private String cnlKey;
	    private String appKey;
	    private String regDttm;
	    private String regUser;
	    private String regUserId;
	    private String modDttm;
	    private String modUser;
	    private String modUserId;
	    private String useFl;
	}
	
	@Data
	public static class AppsBlockKeyRequest {
		private String seqNo;
		private String cnlKey;
		private String appKey;
		private String useFl;
		private String regUser;
		private String modUser;
	}
	
	@Data
	public static class AppsApiInfoRequest {

	    private String seqNo;
	    private String appKey;
	    private String apiId;
	    private String hfnCd;
	    private String regDttm;
	    private String regUser;
	    private String regUserId;
	    private String modDttm;
	    private String modUser;
	    private String modUserId;
	    private String useFl;
	    private String isExist;
	}
	
	@Data
	public static class AppsRegRequest {

	    private String seqNo; // 시퀀스 넘버 - backup 시에만 자료를 담음

	    private String appKey;                          // 앱KEY
	    private String appNm;                           // 앱명
	    private String appScr;                          // 앱Secret
	    private String newAppScr;                       // 신규 앱Secret
	    private String appStatCd;                       // 앱상태코드
	    private String appAplvStatCd;                   // 앱 승인상태 코드
	    private String appClientId;                     // 앱 CLIENT 아이디
	    private String newAppClientId;                  // 신규 앱 CLIENT 아이디
	    private String appSvcStDt;                      // 앱서비스시작일자
	    private String appSvcEnDt;                      // 앱서비스종료일자
	    private String appCtnt;                         // 앱설명
	    private String hfnCd;                           // 관계사 코드
	    private String prdCtgy;                         // 서비스카테고리
	    private String userKey;                         // 이용기관코드
	    private String regUser;                         // 등록사용자
	    private String regUserId;                       // 등록사용자id
	    private String modUser;                         // 변경사용자
	    private String modUserId;                       // 변경사용자id
	    private String appScrVldDttm;                   // 앱 Secret 유효 일시
	    private String appScrReisuYn;                   // 앱 Secret 재발급 여부
	    private String aplvSeqNo;
	    private String appMemo;
	    private String appAnyCnlYn;
	    
	    
	    //nice 추가분
	    private String prdKind;
	    private String appSvcNm;
	    private String appSvcUrl;
	    private String copRegNo;
	    private String brn1;
	    private String brn2;
	    private String brn3;
	    private String copNm;
	    private String ownNm;
	    private String bizType;
	    private String bizCdtn;
	    private String copAddrNo;
	    private String copAddr1;
	    private String copAddr2;
	    private String managerId;
	    private String managerNm;
	    private String dirNm;
	    private String dirTelNo1;
	    private String dirTelNo2;
	    private String dirEmail;
	    private String dirDomain;
	    private List<ProductVO> prdList;
	    private List<AppReadInfoVO> appReadInfo;

	    private String termEtdYn;                       // 기간 연장 여부
	    private String regDttm;                         // 등록 일자
	    private String modDttm;                         // 변경 일자

	    private List<AppsCnlKeyRequest> cnlKeyList;     // 앱 채널 정보
	    private List<AppsCnlKeyRequest> exCnlKeyList;   // 앱 채널 정보 (기존에 등록된 IP정보.업데이트시 필요)
	    private List<AppsBlockKeyRequest> blockKeyList;
	    private List<AppsBlockKeyRequest> exBlockKeyList;   // 앱 채널 정보 (기존에 등록된 IP정보.업데이트시 필요)

	    private List<AppsApiInfoRequest> apiList;       // 앱 API 정보
	    private List<AppsApiInfoRequest> exApiList;     // 앱 API 정보 (기존에 등록된 API정보.업데이트시 필요)
	    private List<ApiVO> redisApiList;
	    private List<ApiVO> redisExApiList;

	    private String useFl;                           // 앱 채널정보, api정보 사용여부 플래그  ('Y','N','WAIT')
	}
	
	@Data
    public static class AplvRegistRequest{
        private String aplvSeqNo;
        @NotBlank
        private String aplvReqCd;
        @NotBlank
        private String aplvDivCd;
        @NotBlank
        private String aplvReqCtnt;
        private String regUserName;
        private String regUserId;
        private String hfnCd;
    }
	
	@Data
	public static class AppsRequest {

	    private int pageIdx     = 0;
	    private int pageSize    = 20;
	    private int pageOffset  = 0;

	    private String searchNm;        // 앱명
	    private String searchCopRegNo;  // 사업자등록번호
	    private String searchUserKey;   // 이용자
	    private String searchHfnCd;
	    private String searchStat;      // 앱상태코드  ('WAIT','OK','CLOSE','EXPIRE')
	    private String searchAplvStat;  // 앱승상태코드 ('REQ','APLV','CANCEL','REJECT')
	    private String appKey;
	    private String appClientId;
	    private String newAppClientId;
	    private String appStatCd;
	    private String appAplvStatCd;
	    private String appSvcEnDt;
	    private String modUserId;
	    private String modUser;
	    private String seqNo;
	    private String aplvSeqNo;
	    private String grantType;
	    private String scope;
	    private String useFl;
	    private String apiId;
	    private String pubKey;
	    private String page;
	}
	
	@Data
	public static class PrdApiRequest {

	    private String apiId;
	    private int prdId;
	    private String clmReqDiv;
	}
	
	@Data
    public static class ApiDetailRequest{
        private String appKey;
        private String useFl;
        private String apiId;
        private String aplvSeqNo;
    }
	
	@Data
	public static class AppsScrRequest {
	    private String seqNo;                  // 앱KEY
	    private String appKey;                  // 앱KEY
	    private String appClientId;             // 앱 Client ID
	    private String appScr;                  // 앱 Secret
	    private String appScrVldDttm;           // 앱 Secret 유효 일시
	    private String regUser;                 // 등록 사용자
	    private String regUserId;               // 등록 사용자 ID
	    private String modUser;                 // 수정사용자
	    private String modUserId;
	    private String pubKey;
	}
	
	@Data
	public static class ApiRequest {

	    private String apiId;
	    private String apiStatCd;
	    private String regUserName;
	    private String regUserId;
	    private String roleCd;

	    private String apiNm;
	    private String ctgrCd;
	    private String stDt;
	    private String edDt;
	    private String msDate;
	    private String appKey;
	    private String enDt;

	    private String targetYear;
	    private String targetMonth;

	    private int pageIdx = 0;
	    private int pageSize = 20;
	    private int pageOffset = 0;
	}
	
	@Data
	public static class StatsRequest {

	    private int pageIdx;
	    private int pageSize;
	    private int pageOffset = 0;
	    private String searchType;
	    private String searchApiId;
	    private String searchUserKey;
	    private String searchUserId;
	    private String searchHfnCd;
	    private String searchCtgrCd;
	    private String searchAppKey;
	    private String searchPrdCd;
	    private String searchCopRegNo;
	    private String searchTrxDt;
	    @NotBlank
	    private String searchStDt;
	    private String searchEnDt;
	}
	
	@Data
    public static class UserPwdUpdateRequest{
        private String userKey;

        @NotBlank
        private String userPwd;

        private String regUserName;
        private String userId;
        private String seq;
        private String authNum;
    }
	
	@Data
    public static class UserDupCheckRequest{
        private String userId;
        private String useorgId;
        private String userEmail;
    }
	
	@Data
    public static class UserLoginLockCheckRequest{

        @NotNull
        private String userId;

        private String  loginDttm;
        private String  loginLock;
        private String  loginLockTime;
        private int     loginFailCnt;
    }
	
	@Data
	public static class SndCertMgntRequest {
	    private String seq;     // SEQ
	    private String userKey;     // 유저key
	    private String recvInfo;    // 수신자 정보(이메일)
	    private String sendCd;      // 발송유형
	    private String sendCtnt;    // 발송 내용
	    private String sendNo;      // 발송 인증번호
	    private String certValidTm; // 인증유효시간
	    private String resultCd;    // 결과코드
	    private String retryCnt;    // 재전송 시도횟수
	    private String birthDt;
	    private String site;         // 사이트구분(ADMIN:관리포털, USER:이용자포털)

	    private String userId;
	    private int pageIdx;
	    private int pageSize;
	    private int pageOffset;
	}
	
	@Data
    public static class HfnLoginLockCheckRequest{
        private String hfnCd;
        @NotNull
        private String hfnId;

        private String  loginDttm;
        private String  loginLock;
        private String  loginLockTime;
        private int     loginFailCnt;
        private String  ipAddr;
        private String  siteCd;
    }
	
	@Data
    public static class apiTestRequest {
    	private String bodyJson;
    	private String apiId;
    	private String appKey;
    	private String prdId;
    }
	
	@Data
    public static class ApiLogDetailRequest {
        @NotNull
        private String trxId;
        private String directoryNm;
    }
	
	@Data
    public static class PortalLogDetailRequest {
        @NotNull
        private String trxId;
    }
	
	@Data
    public static class ApiColumnListRequest{
		private String seqNo;
        private String clmListCd;
        private String clmCd;
        private String apiId;
        private String clmNm;
        private String clmReqDiv;
        private Long clmOrd;
        private String clmType;
        private String required;
        private String clmCtnt;
        private String clmDefRes;
        private String clmValue;
        private String sqlInjection;
        private String logVisible;
        private String masking;
        private String nonidYn;
        private String clmLength;
        private String hideYn;
        private String regUserId;
        private List<AdminRequest.ApiColumnListRequest> apiColumnList;
    }

    @Data
    public static class ApiAllListRequest{
    }

    @Data
    public static class CtgrApiAllListRequest{
        private String searchCtgrCd;
        private String searchHfnCd;
    }
    
    @Data
    public static class apiCodeListRequest{
        private String apiId;
        private String prdId;
        private String prdCd;
        private String useFl;
        private String siteCode;
        private String apiNm;
        private String ctgrCd;
        private String hfnCd;
    }
    
    @Data
    public static class apiRealTimeRequest{
        private String searchType;
    	private String trxDt;
    	private String appKey;
    	private String prdCtgy;
    	private String apiUrl;
        private String prdId;
        private String prdCd;
        private String apiId;
        private String userId;
        private String copRegNo;
        private int pageIdx;
        private int pageSize;
        private int pageOffset = 0;
    }

    @Data
    public static class SwaggerRequest{
        @NotNull
        private String apiSvc;
    }

    @Data
    public static class SwaggerInfoRequest{
        @NotNull
        private String apiSvc;
        @NotNull
        private String apiVer;
        @NotNull
        private String apiUri;
        @NotNull
        private String apiMthd;
    }
    
    @Data
    public static class AppDetailStatsRequest{
        @NotBlank
        private String appKey;
        private String searchUserKey;
        private String searchApiId;
        private String searchAppKey;
        @NotBlank
        private String searchStDt;
        @NotBlank
        private String searchEnDt;
    }
    
    @Data
    public static class AppsIssueRequest{
        private String appKey;
        private String pubKey;
        private String userId;
        private String userKey;
        private String reqServerIp;
        private String appClientId;
        private String newAppClientId;
        private String appScr;
        private String newAppScr;
    }

    @Data
    public static class AppApiRequest {
        private String statisticYear;
        private String statisticMnt;
        private String statisticDay;
        private String statisticDate;

        private String apiSvc;
        private String apiVer;
        private String apiUri;
        private String appKey;
        private String clientId;
        private String apiId;
        private String userKey;
    }

    @Data
    public static class AppDefresRequest {
        private String appKey;
        private String useDefresYn;
    }

    @Data
    public static class AppScrRequest {
    	private String appKey;
    	private String userId;
    	private String password;
    	private String userKey;
    }
    
    @Data
    public static class AppReadUserRequest {
        private String reqId;
    	private String appKey;
    	private String userKey;
    	private String userId;
    	private String userNm;
    	private String userTel;
    	private String userEmail;
    	private String regUser;
    	private String seqNo;
    }

    @Data
    public static class AppReadUserDeleteRequest {
    	private List<String> seqList;
    }

    @Data
    public static class AppUsageRatioRequest {
        private String seqNo;
        private String appKey;
        private String statCd;
        private int ratio;
        private String regUser;
        private String regDttm;
        private String modUser;
        private String modDttm;
    }
    
    @Data
    public static class AplvPrdReqeust {
    	private String appKey;
    	private String prdId;
    }
    
    @Data
    public static class PrdPayReqeust {
    	private String seqNo;
    	private String userKey;
    	private String appKey;
    	private String prdId;
    	private String moid;
    	private int useCnt;
    	private String amtGbCd;
    	private int prdAmt;
    	private String payConfirmYn;
    	private String payAgreeYn;
    	private String creditReportYn;
    	private String regUser;
    }
    
    @Data
    public static class PrdSettReqeust {
    	private String appKey;
    	private String userKey;
    }
    
    @Data
    public static class QnaDeleteRequest {
        private List<String> seqList;
    }
    
    @Data
    public static class QnaInsertRequest {
    	private String seqNo;
    	private String idIndex;
    	private String qnaType;
    	private String statCd;
    	private String userKey;
    	private String userNm;
    	private String reqTitle;
    	private String reqCtnt;
    	private String prdCtgy;
    }
}
