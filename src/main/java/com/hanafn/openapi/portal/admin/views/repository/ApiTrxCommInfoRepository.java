package com.hanafn.openapi.portal.admin.views.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hanafn.openapi.portal.admin.views.dto.ApiTrxCommInfo;


@Repository
public interface ApiTrxCommInfoRepository extends JpaRepository<ApiTrxCommInfo, String> {
    Optional<ApiTrxCommInfo> findBySeqNo(String seqNo);
}
