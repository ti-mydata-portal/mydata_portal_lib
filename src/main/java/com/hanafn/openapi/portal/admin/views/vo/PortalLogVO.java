package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("portalLog")
public class PortalLogVO {
	private String trxId;
	private String regDttm;
	private String trxCd;
	private String trxNm;
	private String userId;
	private String userNm;
    private String siteCd;
    private String roleCd;
    private String procStatCd;
    private String inputCtnt;
    private String outputCtnt;
    private String regUser;
    private String hfnCd;
    private String hfnId;
    private String ipAddress;
    private String menuFunc;
    private String pinfoIncl;
    private String cnt;
}
