package com.hanafn.openapi.portal.admin.batch.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class BatchUserRequest {

    @Data
    public static class HfnUserRequest{

        private String userKey;

        @NotBlank
        private String hfnId;


        @NotBlank
        private String userNm;

        private String accessCd;

        @NotBlank
        private String userPwd;
    // 따로 관리필요 user_role_info 테이블(= roleCd)

        private String tmpPwd;

        private String jobNm;

        private String signLevel;
        private String signUserYn;

        private String userTel;

        private String regUserName;
        private String regUserId;

        @NotBlank
        private String hfnCd;       // 서비스카테고리
        private String ipAddr;      // ip주소
        private String userStatCd;  // 계정상태
        private String deptNm;      // 부서명
        @NotBlank
        private String roleCd;      // 권한

    }

}
