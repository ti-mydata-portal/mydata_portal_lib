package com.hanafn.openapi.portal.admin.batch;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class JobIamAccountSync {

    @Autowired
    IamAccountSyncService iamAccountSyncService;

    public void batchIamAccountSync() {
        log.info("Nice 관리자 DB Sync 시작 ");
        iamAccountSyncService.batchIamAccountSync();
        log.info("Nice 관리자 DB Sync 종료 ");
    }
}
