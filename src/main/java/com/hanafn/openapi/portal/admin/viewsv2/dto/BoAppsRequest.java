package com.hanafn.openapi.portal.admin.viewsv2.dto;

import lombok.Data;

import java.util.List;

@Data
public class BoAppsRequest {

    private int pageIdx     = 0;
    private int pageSize    = 0;
    private int pageOffset  = 0;

    private String searchFromDate;
    private String searchToDate;

    private String searchNm;        // 앱명
    private String searchOrg;       // 이용기관명
    private String searchStat;      // 앱상태코드  ('WAIT','OK','CLOSE','EXPIRE')
    private String searchAplvStat;  // 앱승상태코드 ('REQ','APLV','CANCEL','REJECT')
    private String searchPrdCtgy;   // service category
    private String searchUserKey;   // 소유자ID
    private String searchUserId;    // 소유자명
    private String searchKind;   // 상품종류

    private String appKey;          //앱키
}
