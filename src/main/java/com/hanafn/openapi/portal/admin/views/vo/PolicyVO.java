package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("policy")
public class PolicyVO {
	private String key;
	private String value;
}
