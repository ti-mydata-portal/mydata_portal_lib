package com.hanafn.openapi.portal.admin.viewsv2.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("boappprd")
public class BoAppPrdVO {
    private String seqNo; //순번
    private String appKey;  //앱키
    private String prdId;   //상품코드
    private String prdCtgy; //카테고리
    private String prdKind; //상품종류
    private String prdNm;   //상품명
    private String serviceCd;   //서비스코드
    private String mppCd1;  //서비스연계코드1
    private String mppCd2;  //서비스연계코드2
    private String mppCd3;  //서비스연계코드3
    private String prdStatus;   //상태코드 (10:승인, 20:중단)
    private String mppUser; //매핑처리자
    private String useEndDate;
    private String restCnt;

    private String endDate; //만료일
    private String remainCount; //결제시점의 잔여건수

    private String useFl; //Y,N,WAIT

    private String modUserId;
}