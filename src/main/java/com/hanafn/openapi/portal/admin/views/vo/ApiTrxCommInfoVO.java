package com.hanafn.openapi.portal.admin.views.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("apiTrxCommInfo")
public class ApiTrxCommInfoVO {

    private String seqNo;
    private String fileTrxDt;
    private String fileHfnCd;
    private String fileEntrCd;
    private String fileAppKey;
    private String fileHfnSvcCd;
    private String fileApiTrxCnt;
    private String regDttm;

    // DB외에 JOIN 통하여 얻은 정보
    private String appNm;
    private String useorgNm;
}
