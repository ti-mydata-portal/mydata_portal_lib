package com.hanafn.openapi.portal.admin.viewsv2.dto;

import com.hanafn.openapi.portal.admin.viewsv2.vo.BoAppsVO;
import lombok.Data;

import java.util.List;

@Data
public class BoAppsRsponsePaging {

//    private int pageIdx;
//    private int pageSize;
    private int totCnt;
//    private int selCnt;

    private String resultCode = "0000";
    private String message = "정상처리되었습니다.";

    private List<BoAppsVO> list;
}
