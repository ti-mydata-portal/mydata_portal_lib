package com.hanafn.openapi.portal.admin.batch;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.admin.batch.dto.BatchUserRequest;
import com.hanafn.openapi.portal.admin.batch.vo.IamAccountVO;
import com.hanafn.openapi.portal.admin.views.vo.HfnUserVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.CommonUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class IamAccountSyncService {

    @Autowired
    IamRepository iamRepository;

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    CommonUtil commonUtil;

    public void batchIamAccountSync() {
        try {
            // niceiam.account 에서 update 날짜가 '오늘' 인 전체 사용자 목록 조회
            List<IamAccountVO> list = iamRepository.selectIamAccount();

            //1건씩 가져와서.
            for (IamAccountVO iamAccountVO : list) {
                //hfn_user_info 테이블 조회.

                BatchUserRequest.HfnUserRequest hfnUserDupCheckRequest = new BatchUserRequest.HfnUserRequest();
                hfnUserDupCheckRequest.setHfnId(iamAccountVO.getUserId());
                hfnUserDupCheckRequest.setHfnCd(iamAccountVO.getEtc1());
                List<HfnUserVO> lstHfnUser = iamRepository.selectHfnUser(hfnUserDupCheckRequest);

                if (lstHfnUser.size() == 0) {   // 기존에 등록된 계정이 없으면 insert
                    insertHfnUser(iamAccountVO);
                } else {    // 기존에 등록된 계정이 있으면 update
                    updateHfnUser(lstHfnUser.get(0).getUserKey(), iamAccountVO);
                }
            }
        } catch(BusinessException e) {
            log.error(commonUtil.stackTraceToString(e));
        } catch (Exception e) {
            log.error(commonUtil.stackTraceToString(e));
        } finally {
        }
    }

    /**
     * 사용자 추가
     * @param iamAccountVO
     * @throws Exception
     */
    private void insertHfnUser(IamAccountVO iamAccountVO) throws Exception  {
        //조회결과 없으면 insert + 상태. (정보 중 퇴사한 경우 기타정보도 필요한 경우 blank처리
        BatchUserRequest.HfnUserRequest request = new BatchUserRequest.HfnUserRequest();
        request.setHfnId(iamAccountVO.getUserId());
        request.setUserNm(iamAccountVO.getUserName());
        request.setAccessCd("10");
        request.setUserPwd(passwordEncoder.encode("Nice12#$"));
        request.setTmpPwd(passwordEncoder.encode("Nice12#$"));
        request.setJobNm(iamAccountVO.getPosiNm());
        request.setSignLevel("10");
        request.setSignUserYn("N");
        request.setUserTel("");
        request.setRegUserName(iamAccountVO.getUserName());
        request.setRegUserId(iamAccountVO.getUserId());

        request.setIpAddr(iamAccountVO.getIpAddr());    // ip주소
        request.setHfnCd(iamAccountVO.getEtc1());       // 서비스카테고리
        request.setDeptNm(iamAccountVO.getDeptNm());    // 부서명
        request.setUserStatCd(iamAccountVO.getUseYn()); // 계정상태

        iamRepository.insertHfnUserInfo(request);

        // user_role_info에 id,role_Cd 정보 담기
        request.setUserKey(iamRepository.selectHfnUser(request).get(0).getUserKey());
        request.setRoleCd(iamAccountVO.getEtc2());      // 권한
        iamRepository.insertHfnUserRole(request);
    }

    /**
     * 사용자 상태 변경
     * @param userKey
     * @param iamAccountVO
     * @throws Exception
     */
    private void updateHfnUser(String userKey, IamAccountVO iamAccountVO) throws Exception {
        //조회결과가 존재하면 업데이트
        BatchUserRequest.HfnUserRequest request = new BatchUserRequest.HfnUserRequest();

        request.setUserKey(userKey);
        request.setRegUserId(iamAccountVO.getUserId());

        request.setIpAddr(iamAccountVO.getIpAddr());    // ip주소
        request.setHfnCd(iamAccountVO.getEtc1());       // 서비스카테고리
        request.setDeptNm(iamAccountVO.getDeptNm());    // 부서명
        request.setUserStatCd(iamAccountVO.getUseYn()); // 계정상태
        request.setRoleCd(iamAccountVO.getEtc2());      // 권한

        // 유저 정보 업데이트
        iamRepository.updateHfnUserInfo(request);
        // 유저 권한 업데이트
        iamRepository.updateHfnUserRole(request);
    }
}