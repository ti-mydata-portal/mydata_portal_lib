package com.hanafn.openapi.portal.admin.viewsv2.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.MailSmsVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvApprovalRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvHisRegistRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRsponsePaging;
import com.hanafn.openapi.portal.admin.viewsv2.dto.PrdAplvDetailResponse;
import com.hanafn.openapi.portal.admin.viewsv2.dto.ServiceCategoryEnum;
import com.hanafn.openapi.portal.admin.viewsv2.repository.AplvRepository;
import com.hanafn.openapi.portal.admin.viewsv2.vo.BoAuthVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.PrdAplvListVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.PrdAplvVO;
import com.hanafn.openapi.portal.cmct.RedisCommunicater;
import com.hanafn.openapi.portal.enums.etcEnum;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.MailUtils;
import com.hanafn.openapi.portal.util.service.ISecurity;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class AplvService {
	private static final String APLV_APPROVAL_STAT_CD = "APPROVAL";
	private static final String APLV_REJECT_STAT_CD = "REJECT";

    @Autowired
	AplvRepository aplvRepository;
    @Autowired
    AdminRepository adminRepository;
	@Autowired
	MessageSourceAccessor messageSource;
    @Autowired
    MailUtils mailUtils;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity aes256Util;

	public AplvRsponsePaging selectPrdAplvListPaging(AplvRequest request){
        int totCnt = aplvRepository.countAplvList(request);
        List<PrdAplvListVO> list = aplvRepository.selectAplvList(request);

		/**
		 * 앱 등록 후 자동 승인시에는 T4.PRD_KIND 가 10(선납자동승인)만 찾아야 함
         * > 현재 승인목록조회에선 반대로 자동승인은 조회하면 안되기 때문에 같은 거래에 조건이 상충됨
		 */

		com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRsponsePaging pagingData;
		try {
			// 복호화: 요청자
			String regUser;
			for (PrdAplvListVO vo : list) {
				regUser = vo.getRegUser();
				if (!StringUtils.isEmpty(regUser)) {
					vo.setRegUser(aes256Util.decrypt(regUser));
				}
			}

			pagingData = new com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRsponsePaging();
//			pagingData.setPageIdx(request.getPageIdx());
//			pagingData.setPageSize(request.getPageSize());
			pagingData.setList(list);
			pagingData.setTotCnt(totCnt);
//			pagingData.setSelCnt(list.size());
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026", messageSource.getMessage("E026"));
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new BusinessException("E026", messageSource.getMessage("E026"));
		}
		return pagingData;
	}

	/**
	 * 승인요청정보 상세조회
	 * @param aplvDetailRequest
	 * @return
	 * @throws BusinessException
	 */
	public PrdAplvDetailResponse selectAplvOne(AplvApprovalRequest aplvDetailRequest) throws BusinessException{
		PrdAplvDetailResponse response = new PrdAplvDetailResponse();

		// 승인정보
		PrdAplvVO prdAplvInfo = aplvRepository.selectAplvOne(aplvDetailRequest);

        AdminRequest.PrdApiRequest request = new AdminRequest.PrdApiRequest();
        request.setPrdId(prdAplvInfo.getPrdId());
        List<ApiVO> apiList = adminRepository.selectApiList(request);

		if (prdAplvInfo == null) {
			log.error("승인정보 is null");
			throw new BusinessException("E026", messageSource.getMessage("E026"));
		}

		// 복호화
		try {
			// 요청자 이름
			if (!StringUtils.isEmpty(prdAplvInfo.getRegUser())) {
				prdAplvInfo.setRegUser(aes256Util.decrypt(prdAplvInfo.getRegUser()));
			}

			// 대표자
			if (!StringUtils.isEmpty(prdAplvInfo.getOwnNm())) {
				prdAplvInfo.setOwnNm(aes256Util.decrypt(prdAplvInfo.getOwnNm()));
			}

			// 정산담당자
            if (!StringUtils.isEmpty(prdAplvInfo.getDirNm())) {
                prdAplvInfo.setDirNm(aes256Util.decrypt(prdAplvInfo.getDirNm()));
            }

            // 정산담당자 이메일
            if (!StringUtils.isEmpty(prdAplvInfo.getDirEmail())) {
                prdAplvInfo.setDirEmail(aes256Util.decrypt(prdAplvInfo.getDirEmail()));
            }

            // 정산담당자 연락처
            if (!StringUtils.isEmpty(prdAplvInfo.getDirTelNo2())) {
                prdAplvInfo.setDirTelNo2(aes256Util.decrypt(prdAplvInfo.getDirTelNo2()));
            }

		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		} catch ( Exception e ) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		}

		response.setInfo(prdAplvInfo);
		response.setApiList(apiList);
		return response;
	}

	/**
	 * 앱 상품 승인요청 승인
	 *
     * 1) 앱 등록 후 자동승인 처리에서 사용 2021.01.11
     *
	 * @param aplvApprovalRequest
	 * @throws Exception
	 */
	public void aplvApproval(AplvApprovalRequest aplvApprovalRequest) throws Exception {
		//승인정보 조회
		aplvApprovalRequest.setAplvStatCd("WAITORREJECT"); //이미 승인된것은 불가.
		PrdAplvVO prdAplvVo = aplvRepository.selectAplvOne(aplvApprovalRequest);
		// 앱 최초 등록시 PORTAL_APLV_INFO 상태
        // APLV_DIV_CD = 'APPPRDREG' , APLV_STAT_CD = 'WAIT'
        // APLV_DIV_CD = 'APP , APLV_STAT_CD = 'APPROVAL'

		if (prdAplvVo == null) {
			throw new BusinessException("E026", "승인 처리할 수 없는 상태입니다. 관리자에게 문의 바랍니다.");
		}

		//승인정보 업데이트
		aplvApprovalRequest.setAplvStatCd(APLV_APPROVAL_STAT_CD);
		aplvRepository.updateAplvInfo(aplvApprovalRequest);

		//APP PRD 정보 업데이트
		aplvApprovalRequest.setUseFl("Y");
		aplvApprovalRequest.setSeqNo(prdAplvVo.getAplvReqCd());
		aplvRepository.updateAppPrdInfoForWait(aplvApprovalRequest);

		//승인이력 추가
		AplvHisRegistRequest aplvHisRegistRequest = new AplvHisRegistRequest();
		aplvHisRegistRequest.setAplvSeqNo(aplvApprovalRequest.getAplvSeqNo());
		aplvHisRegistRequest.setHfnCd(aplvApprovalRequest.getPrdCtgy());
		aplvHisRegistRequest.setAplvStatCd(APLV_APPROVAL_STAT_CD);
		aplvHisRegistRequest.setRegUserId(aplvApprovalRequest.getProcUser());
		aplvHisRegistRequest.setProcId(aplvApprovalRequest.getProcUser());
		aplvRepository.insertAplvHis(aplvHisRegistRequest);

		//oauth info update
		//get product authorize_grant_type, scope
		AdminRequest.ProductRequest productRequest = new AdminRequest.ProductRequest();
		productRequest.setPrdId(prdAplvVo.getPrdId());
		ProductVO productVO = adminRepository.selectPrdDetail(productRequest);
		BoAuthVO boAuthVO = aplvRepository.selectOAuthClientDetail(prdAplvVo);

        String productGrantType = productVO.getGrantType();
        String productScope = productVO.getScope();
        TreeSet<String> tr = new TreeSet<String>();
        if (productGrantType != null && !productGrantType.equals("")) {
            if(!productGrantType.equals("")) {
                tr.addAll(Arrays.asList(productGrantType.split(",")));
            }

			if(!boAuthVO.getAuthorizedGrantTypes().equals("")) {
				tr.addAll(Arrays.asList(boAuthVO.getAuthorizedGrantTypes().split(",")));
			}
		}
		String addGrantType = String.join(",",tr);

		if (productScope != null && !productScope.equals("")) {
			tr = new TreeSet<String>();
			if(!productScope.equals("")) {
				tr.addAll(Arrays.asList(productScope.split(",")));
			}
			if(!boAuthVO.getScope().equals("")) {
				tr.addAll(Arrays.asList(boAuthVO.getScope().split(",")));
			}
		}
		String addScope = String.join(",", tr);
		AdminRequest.AppsRequest appsRequest = new AdminRequest.AppsRequest();
		appsRequest.setAppClientId(prdAplvVo.getAppClientId());
		appsRequest.setGrantType(addGrantType);
		appsRequest.setScope(addScope);
		aplvRepository.updateOauthClient(appsRequest);

		//redis set
		RedisCommunicater.productCodeRedisSet(prdAplvVo.getAppClientId(), prdAplvVo.getPrdCd(), "true");

        // 승인완료 메일 전송
        sendMail(aplvApprovalRequest);
	}


	/**
	 * 앱 상품 승인요청 반려
	 */
	public void aplvReject(AplvApprovalRequest aplvRejectRequest) throws Exception {
		//승인정보 조회
		aplvRejectRequest.setAplvStatCd(aplvRejectRequest.getAplvStatCd());
		PrdAplvVO prdAplvVo = aplvRepository.selectAplvOne(aplvRejectRequest);

		if (prdAplvVo == null) {
			throw new BusinessException("E026", "반려 처리할 수 없는 상태입니다. 관리자에게 문의 바랍니다.");
		}

		//승인정보 업데이트
		aplvRejectRequest.setAplvStatCd(APLV_REJECT_STAT_CD);
		aplvRepository.updateAplvInfo(aplvRejectRequest);

		//APP PRD 정보 업데이트
		aplvRejectRequest.setUseFl("N");
		aplvRejectRequest.setSeqNo(prdAplvVo.getAplvReqCd());
		aplvRepository.updateAppPrdInfoForWait(aplvRejectRequest);

		//승인이력 추가
		AplvHisRegistRequest aplvHisRegistRequest = new AplvHisRegistRequest();
		aplvHisRegistRequest.setAplvSeqNo(aplvRejectRequest.getAplvSeqNo());
		aplvHisRegistRequest.setHfnCd(aplvRejectRequest.getPrdCtgy());
		aplvHisRegistRequest.setAplvStatCd(APLV_REJECT_STAT_CD);
		aplvHisRegistRequest.setRejectCtnt(aplvRejectRequest.getRejectCtnt());
		aplvHisRegistRequest.setRegUserId(aplvRejectRequest.getProcUser());
		aplvHisRegistRequest.setProcId(aplvRejectRequest.getProcUser());
		aplvRepository.insertAplvHis(aplvHisRegistRequest);

		// 승인반려 메일 발송
        sendMail(aplvRejectRequest);
	}

	// 승인(반려) 메일 발송
	private void sendMail(AplvApprovalRequest aplvRequest) throws Exception {

		if (!(aplvRequest.getAplvStatCd().equals(APLV_REJECT_STAT_CD) ||
		aplvRequest.getAplvStatCd().equals(APLV_APPROVAL_STAT_CD)))
			throw new Exception("Unknown Approve Stat Cd..");

        PrdAplvVO prdAplvInfo = aplvRepository.selectAplvEmail(aplvRequest);

        List<MailSmsVO> list = new ArrayList<>();
        MailSmsVO vo = new MailSmsVO();
        vo.setMailType("API_002");
        vo.setEmail(aes256Util.decrypt(prdAplvInfo.getUserEmail()));
        vo.setSenddate(prdAplvInfo.getProcDttm());
        vo.setTagmap001("admin@mail.niceapi.co.kr");
        vo.setTagmap002(prdAplvInfo.getAppKey());
        vo.setTagmap003(prdAplvInfo.getAppNm());
        vo.setTagmap006(String.valueOf(prdAplvInfo.getPrdId()));
        vo.setTagmap007(prdAplvInfo.getPrdNm());

        if(aplvRequest.getAplvStatCd().equals(APLV_APPROVAL_STAT_CD)) {
            vo.setTagmap004(etcEnum.resolveByCd(prdAplvInfo.getPrdKind()).getName());
        } else {
            vo.setTagmap004(ServiceCategoryEnum.resolve(prdAplvInfo.getPrdCtgy()).getName());
            vo.setTagmap005(etcEnum.resolveByCd(prdAplvInfo.getPrdKind()).getName());
            vo.setTagmap011(prdAplvInfo.getRejectCtnt());
        }

        list.add(vo);
        mailUtils.makeMailData(list, false);
    }
}