package com.hanafn.openapi.portal.admin.views.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.CommCodeRequest;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeRequest.CommCodeInsert;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeRequest.CommInfoInsert;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.CommCodeVO;
import com.hanafn.openapi.portal.admin.views.vo.CommInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.MailSmsVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;

@Mapper
public interface CommRepository {
	List<CommCodeVO> selectCommCode(CommCodeRequest commCodeRequest);
	List<AppsVO> selectAppCode(CommCodeRequest.CommAppRequest request);
	List<ProductVO> selectPrdCode(CommCodeRequest.CommPrdReqeust prdCodeRequest);
	void insertMailhisLog(MailSmsVO vo);
	void insertSmshisLog(MailSmsVO vo);
	List<CommInfoVO> selectCommInfo(CommCodeRequest commCodeRequest);
	
	void insertCommInfo(CommInfoInsert data);
	void updateCommInfo(CommInfoInsert data);
	void deleteCommInfo(CommCodeRequest data);
	
	void insertCommCode(CommCodeInsert data);
	void updateCommCode(CommCodeInsert data);
	void deleteCommCode(CommCodeRequest data);
	
	String getSysdate();
}