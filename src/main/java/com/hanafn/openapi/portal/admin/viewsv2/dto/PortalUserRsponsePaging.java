package com.hanafn.openapi.portal.admin.viewsv2.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.viewsv2.vo.PortalUserVO;

import lombok.Data;

@Data
public class PortalUserRsponsePaging {
    private int pageIdx;
    private int pageSize;

    private String resultCode = "0000";
    private String message = "정상처리되었습니다.";

    private int totCnt;
    private int selCnt;
    private List<PortalUserVO> list;
}
