package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Builder;
import lombok.Data;
import org.apache.ibatis.type.Alias;

@Builder
@Data
public class ApiTrxCommDetailWithStatVO {
    // ApiTrxCommInfo - seq와 1:N
    private String seqNo;

    // HFN_SVC_CD + TRX_DT로 얻을 수 있는 정보들 (JOIN API_INFO)
    private String apiId;
    private String apiNm;

    // ( JOIN APP_INFO )
    private String appNm;

    // API_ID + TRX_DT로 얻을 수 있는 정보들 (STAT_DAY_INFO)
    private String trxDt;
    private String apiTrxCnt;
    private String gwErrorCnt;
    private String apiErrorCnt;
    private String svcErrorCnt;
    private String platformApiTrxCnt;
}
