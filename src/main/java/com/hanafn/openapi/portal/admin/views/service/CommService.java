package com.hanafn.openapi.portal.admin.views.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.admin.views.dto.CommCodeRequest;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeRequest.CommCodeInsert;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeRequest.CommInfoInsert;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeResponse;
import com.hanafn.openapi.portal.admin.views.repository.CommRepository;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.CommCodeVO;
import com.hanafn.openapi.portal.admin.views.vo.CommInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class CommService {

	@Autowired
	CommRepository commRepository;
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public CommCodeResponse selectCommCode(CommCodeRequest commCodeRequest){
		List<CommCodeVO> list = commRepository.selectCommCode(commCodeRequest);
		
		CommCodeResponse commCodeResponse = new CommCodeResponse();
		commCodeResponse.setCodeList(list);
		
		return commCodeResponse;
	}
	
	/*
	 * ******************************Code App+AppCop******************************
	 * */
	public CommCodeResponse.CommAppResponse selectAppCodeList(CommCodeRequest.CommAppRequest request){
		List<AppsVO> list = commRepository.selectAppCode(request);
		
		CommCodeResponse.CommAppResponse commCodeResponse = new CommCodeResponse.CommAppResponse();
		commCodeResponse.setCodeList(list);
		
		return commCodeResponse;
	}
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public CommCodeResponse.CommPrdResponse selectPrdCodeList(CommCodeRequest.CommPrdReqeust prdCodeRequest){
		List<ProductVO> list = commRepository.selectPrdCode(prdCodeRequest);
		
		CommCodeResponse.CommPrdResponse commCodeResponse = new CommCodeResponse.CommPrdResponse();
		commCodeResponse.setCodeList(list);
		
		return commCodeResponse;
	}
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public CommCodeResponse.CommInforesponse selectCommInfo(CommCodeRequest commCodeRequest){
		List<CommInfoVO> list = commRepository.selectCommInfo(commCodeRequest);
		
		CommCodeResponse.CommInforesponse commCodeResponse = new CommCodeResponse.CommInforesponse();
		commCodeResponse.setInfoList(list);
		
		return commCodeResponse;
	}
	
	public void saveCommInfo(CommCodeRequest request) {
		List<CommInfoInsert> saveList = request.getCommInfoList();
		
		for(CommInfoInsert data : saveList) {
			String status = data.getStatus();
			
			if(status != null && !"".equals(status) && "N".equals(status)) {
				commRepository.insertCommInfo(data);
			}else if(status != null && !"".equals(status) && "U".equals(status)) {
				commRepository.updateCommInfo(data);
			}
		}
	}
	
	public void deleteCommInfo(CommCodeRequest request) {
		commRepository.deleteCommCode(request);
		commRepository.deleteCommInfo(request);
	}
	
	public void saveCommCode(CommCodeRequest request) {
		List<CommCodeInsert> saveList = request.getCommCodeList();
		
		for(CommCodeInsert data : saveList) {
			String status = data.getStatus();
			
			if(status != null && !"".equals(status) && "N".equals(status)) {
				commRepository.insertCommCode(data);
			}else if(status != null && !"".equals(status) && "U".equals(status)) {
				commRepository.updateCommCode(data);
			}
		}
	}
	
	public void deleteCommCode(CommCodeRequest request) {
		commRepository.deleteCommCode(request);
	}
}