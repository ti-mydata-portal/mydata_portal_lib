package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("qna")
public class QnaVO {
    private String seqNo;
    private String idIndex;
    private String compNm;
    private String statCd;
    private String qnaType;
    private String userKey;
    private String userNm;
    private String userTel;
    private String apiNm;
    private String hfnCd;
    private String reqTitle;
    private String reqCtnt;
    private String uploadFile01;
    private String uploadFile02;
    private String userId;
    private String regDttm;
    private String answerYn;
    private String answer;
    private String modId;
    private String modDttm;
    private String regUser;
    private String modUser;
    private String hfnUserNm;
    private String prdCtgy;
}

