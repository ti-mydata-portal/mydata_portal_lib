package com.hanafn.openapi.portal.admin.viewsv2.repository;

import com.hanafn.openapi.portal.admin.viewsv2.dto.PortalUserRequest;
import com.hanafn.openapi.portal.admin.viewsv2.vo.PortalUserVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PortalUserRepository {
	int countUserList(PortalUserRequest userRequest);
	List<PortalUserVO> selectUserList(PortalUserRequest userRequest);
}