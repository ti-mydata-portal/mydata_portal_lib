package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("statsDayInfo")
public class StatsDayInfoVO {
    private String apiTrxCnt;
    private String gwErrorCnt;
    private String apiErrorCnt;
    private String svcErrorCnt;
}
