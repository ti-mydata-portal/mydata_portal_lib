package com.hanafn.openapi.portal.admin.viewsv2.controller;

import java.io.UnsupportedEncodingException;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvApprovalRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRsponsePaging;
import com.hanafn.openapi.portal.admin.viewsv2.dto.PrdAplvDetailResponse;
import com.hanafn.openapi.portal.admin.viewsv2.service.AplvService;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.util.service.ISecurity;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/admin")
@Slf4j
public class AplvController extends AbstController{

    @Autowired
    AplvService aplvService;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity aes256Util;
//    @Autowired
//    SignupService signUpService;
//    @Autowired
//    SettingRepository settingRepository;
//    @Autowired
//    AppsRepository appsRepository;
//    @Autowired
//    PasswordEncoder passwordEncoder;
//    @Autowired
//    RoleRepository roleRepository;
//    @Autowired
//    CommonUtil commonUtil;
//    @Autowired
//    FileService fileService;

    @PostMapping("/approval/prdAplvList")
    public ResponseEntity<?> prdAplvList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AplvRequest request) {
        if (StringUtils.isEmpty(request.getSearchFromDate()) ||
                StringUtils.isEmpty(request.getSearchToDate())) {
            throw new BusinessException("E802", messageSource.getMessage("E802"));
        }

        try {
            //0.필수값 체크
            //1. default 값 설정
            Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
            for (GrantedAuthority ga : authorities) {
                if (ga.getAuthority() != "ROLE_SYS_ADMIN") {
                    request.setSearchPrdCtgy(currentUser.getHfnCd());
                    break;
                }
            }

            request.setProcId(currentUser.getUseorgKey());
            request.setProcUser(currentUser.getUsername());

            //기본 구분값은 앱-상품승인 대기
            if (StringUtils.isEmpty(request.getSearchAplvDivCd()))
                request.setSearchAplvDivCd("APPPRDREG");

            String searchEtc = request.getSearchEtc();
            if (!StringUtils.isEmpty(searchEtc)) {
                try {
                    request.setSearchEtcEncrypted(aes256Util.encrypt(searchEtc));
                } catch (UnsupportedEncodingException e) {
                    log.error("검색조건 암호화 오류.", e);
                } catch (Exception e) {
                    log.error("검색조건 암호화 오류.", e);
                }
            }

            //2. paging 기본값
            if (request.getPageIdx() == 0)
                request.setPageIdx(request.getPageIdx() + 1);

            if (request.getPageSize() == 0) {
                request.setPageSize(20);
            }

            request.setPageOffset((request.getPageIdx() - 1) * request.getPageSize());

            //3.조회 서비스 호출
            AplvRsponsePaging data = aplvService.selectPrdAplvListPaging(request);

            return ResponseEntity.ok(data);
        } catch (BusinessException e) {
            log.error("승인목록 조회 오류", e);
            throw new BusinessException("E026",messageSource.getMessage("E026"));
        } catch (Exception e) {
            log.error("승인목록 조회 오류", e);
            throw new BusinessException("E026",messageSource.getMessage("E026"));
        }
    }

    @PostMapping("/approval/prdAplvDetail")
    public ResponseEntity<?> prdAplvDetail(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AplvApprovalRequest request) {
        //0.필수값 체크
        if (StringUtils.isEmpty(request.getAplvSeqNo())) {
            log.error("APLV_SEQ_NO is empty..");
            throw new BusinessException("E020", messageSource.getMessage("E020"));
        }

        //권한체크. superuser 외 자기 자신것만.
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() != "ROLE_SYS_ADMIN") {
                request.setPrdCtgy(currentUser.getHfnCd());
                break;
            }
        }

        request.setProcId(currentUser.getUserKey());
        request.setProcUser(currentUser.getUsername());
        PrdAplvDetailResponse data = aplvService.selectAplvOne(request);

        return ResponseEntity.ok(data);
    }

    // 결재자가 승인버튼 클릭시 처리
    @PostMapping("/approval/prodAplvApproval")
    public ResponseEntity<?> aplvApproval(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AplvApprovalRequest request) throws Exception {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() != "ROLE_SYS_ADMIN") {
                request.setPrdCtgy(currentUser.getHfnCd());
                break;
            }
        }

        request.setProcUser(currentUser.getUsername());
        request.setProcId(currentUser.getUserKey());
        aplvService.aplvApproval(request);

        return ResponseEntity.ok(new SignUpResponse(true, "Aplv Approval successfully"));
    }

    @PostMapping("/approval/prodAplvReject")
    public ResponseEntity<?> aplvReject(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AplvApprovalRequest request) throws Exception {

        request.setProcUser(currentUser.getUsername());
        request.setProcId(currentUser.getUserKey());
        request.setPrdCtgy(currentUser.getHfnCd());
        aplvService.aplvReject(request);

        return ResponseEntity.ok(new SignUpResponse(true, "Aplv Reject successfully"));
    }
}
