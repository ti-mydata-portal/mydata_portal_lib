package com.hanafn.openapi.portal.admin.views.dto;

import org.apache.commons.lang3.StringUtils;
import reactor.util.annotation.Nullable;

public enum RoleEnum {
    ROLE_SYS_ADMIN("10","ROLE_SYS_ADMIN", "포털관리자"),
    ROLE_HFN_ADMIN("20","ROLE_HFN_ADMIN", "API운영자"),
    ROLE_HFN_USER("21","ROLE_HFN_USER", "개발담당자"),
    ROLE_HFN_SALES("22", "ROLE_HFN_SALES", "영업담당자"),
    ROLE_ORG_ADMIN("30","ROLE_ORG_ADMIN", ""),
    ROLE_ORG_USER("31", "ROLE_ORG_USER", ""),
    ROLE_PERSONAL("40", "ROLE_PERSONAL", "");

    private String value;
    private String name;
    private String kor;

    RoleEnum(String value, String name, String kor ) {
        this.name = name;
        this.value = value;
        this.kor = kor;
    }

    /**
     * Return the String value of HfnCompanyInfo
     */
    public String value() {
        return this.value;
    }
    public String getName() { return this.name; }
    public String getKor() { return this.kor; }

    @Nullable
    public static RoleEnum resolve(String name) {
        for (RoleEnum roleInfo : RoleEnum.values()) {
            if (StringUtils.equals(name, roleInfo.getName())) {
                return roleInfo;
            }
        }
        return null;
    }

    @Nullable
    public static RoleEnum resolveByCd (String value) {
        for (RoleEnum roleInfo : RoleEnum.values()) {
            if (StringUtils.equals(value, roleInfo.value())) {
                return roleInfo;
            }
        }
        return null;
    }
}
