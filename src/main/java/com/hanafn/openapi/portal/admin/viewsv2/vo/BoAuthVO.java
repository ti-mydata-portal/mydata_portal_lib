package com.hanafn.openapi.portal.admin.viewsv2.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("boOAuthClient")
public class BoAuthVO {
    private String clientId;
    private String authorizedGrantTypes;
    private String scope;
}