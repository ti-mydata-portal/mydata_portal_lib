package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("prdApiInfo")
public class ProductApiInfoVO {
	private String seqNo;
	private int prdId;
	private String apiId;
	private String apiUrl;
	private String useFl;
	private String aplvSeqNo;
	private String siteCode;
	private String regUser;
	private String regDttm;
	private String modDttm;
	private String modUser;
}
