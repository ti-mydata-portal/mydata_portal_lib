package com.hanafn.openapi.portal.admin.viewsv2.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("prdAplvList")
public class PrdAplvListVO {
	private String aplvSeqNo; 	//(승인일련번호)
	private String aplvDivCd; 	//(분류)
	private String appNm; 		//(앱명)
	private String prdCtgy; 	//(서비스카테고리)
	private String prdNm; 		//(상품명)
	private String prdKind; 	//(상품종류)
	private String aplvStatCd; 	//(승인상태)
	private String regUser; 	//(요청자)
	private String regId; 		//(요청자ID)
	private String procUser; 	//(처리자)
	private String procId; 		//(처리자ID)
	private String regDttm; 	//(요청일시)
	private String procDttm; 	//(처리일시)
	private String appClientId;	//(앱 CLIENT_ID)

	private String userKey;
	private String userId;
	private String copNm;
	private String copRegNo;
	private String appKey;
	private String prdId;
	private String prdCd;
	private String serviceCd;
	private String mppCd1;
	private String mppCd2;
	private String mppCd3;
	private String aplvStat;
	private String mppUser;
	private String useFl;
}
