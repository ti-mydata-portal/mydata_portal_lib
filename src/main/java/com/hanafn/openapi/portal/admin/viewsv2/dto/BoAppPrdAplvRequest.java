package com.hanafn.openapi.portal.admin.viewsv2.dto;

import com.hanafn.openapi.portal.admin.viewsv2.vo.BoAppPrdVO;
import lombok.Data;

import java.util.List;

@Data
public class BoAppPrdAplvRequest {

    private String appKey;
    private String userKey;
    private String prdId;
    private String serviceCd;
    private String mppCd1;
    private String mppCd2;
    private String mppCd3;
    private String prdStatus;

    private String regUser;
    private String regUserId;
    private String modUserId;

}
