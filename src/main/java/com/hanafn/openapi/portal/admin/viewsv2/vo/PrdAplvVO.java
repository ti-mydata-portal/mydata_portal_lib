package com.hanafn.openapi.portal.admin.viewsv2.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("prdAplv")
public class PrdAplvVO {
	private String aplvSeqNo; 	//(승인일련번호)
	private String aplvReqCd; 	//(승인요청값-앱상품코드)
	private String prdCtgy; 	//(서비스카테고리)
	private String appClientId;	//(앱 CLIENT_ID)
	private String appNm; 		//(앱명)
	private int    prdId; 		//(상품아이디)
	private String prdCd; 		//(상품코드)
	private String prdNm; 		//(상품명)
	private String prdKind; 	//(상품종류)
	private String aplvDivCd; 	//(분류)
	private String regUser; 	//(요청자)
	private String regId; 		//(요청자ID)
	private String procUser; 	//(처리자)
	private String procId; 		//(처리자ID)
	private String regDttm; 	//(요청일시)
	private String procDttm; 	//(처리일시)
	private String aplvStatCd; 	//(승인상태)
	private String userEmail;   //요청자이메일
	private String appKey; 		//앱키
	private String rejectCtnt;  //반려사유

	private String appUserId;   //앱소유자ID
	private String appUserNm;   //앱소유자명
	private String appCtnt;     //(앱설명)

	private String copNm;       //(사업자명)
	private String copRegNO;    //(사업자등록번호)
	private String copAddr1;    //(사업장주소1)
	private String copAddr2;    //(사업장주소2)
	private String ownNm;       //(대표자명)
	private String bizCdtn;     //(업태)
	private String bizType;     //(종목)
	private String dirNm;       //정산담당자명
	private String dirTelNo1;   //정산담당자연락처1
	private String dirTelNo2;   //정산담당자연락처2
	private String dirEmail;    //정산담당자이메일
	private String dirDomain;   //정산담당자이메일도메인
	private String appSvcNm;    //(서비스명)
	private String appSvcUrl;   //(서비스URL)

    private String mppCd1;      //사이트코드
    private String mppCd2;      //계약번호
    private String mppCd3;      //청구번호
}
