package com.hanafn.openapi.portal.admin.views.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanafn.openapi.portal.admin.views.dto.CommCodeRequest;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeResponse;
import com.hanafn.openapi.portal.admin.views.service.CommService;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/comm")
@Slf4j
@RequiredArgsConstructor
public class CommController {

    private final CommService commService;

    @PostMapping("/selectCommCodeList")
    public ResponseEntity<?> selectCommCodeList(@Valid @RequestBody CommCodeRequest commCodeRequest, @CurrentUser UserPrincipal currentUser) {
        CommCodeResponse data = commService.selectCommCode(commCodeRequest);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectAppCodeList")
    public ResponseEntity<?> selectAppCodeList(@Valid @RequestBody CommCodeRequest.CommAppRequest prdCodeRequest, @CurrentUser UserPrincipal currentUser) {
    	CommCodeResponse.CommAppResponse data = commService.selectAppCodeList(prdCodeRequest);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectPrdCodeList")
    public ResponseEntity<?> selectPrdCodeList(@Valid @RequestBody CommCodeRequest.CommPrdReqeust prdCodeRequest, @CurrentUser UserPrincipal currentUser) {
    	CommCodeResponse.CommPrdResponse data = commService.selectPrdCodeList(prdCodeRequest);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectBaseUrl")
    public ResponseEntity<?> selectBaseUrl(HttpServletRequest request) {

        return ResponseEntity.ok(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
    }
    
    @PostMapping("/selectCommInfo")
    public ResponseEntity<?> selectCommInfo(@Valid @RequestBody CommCodeRequest commCodeRequest, @CurrentUser UserPrincipal currentUser) {
        CommCodeResponse.CommInforesponse data = commService.selectCommInfo(commCodeRequest);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/saveCommInfo")
    public ResponseEntity<?> saveCommInfo(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody CommCodeRequest request) {

    	commService.saveCommInfo(request);
        return ResponseEntity.ok(new SignUpResponse(true, "CoMM saved successfully"));
    }
    
    @PostMapping("/saveCommCode")
    public ResponseEntity<?> saveCommCode(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody CommCodeRequest request) {

    	commService.saveCommCode(request);
        return ResponseEntity.ok(new SignUpResponse(true, "CoMM saved successfully"));
    }
    
    @PostMapping("/deleteCommInfo")
    public ResponseEntity<?> deleteCommInfo(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody CommCodeRequest request) {

    	commService.deleteCommInfo(request);
        return ResponseEntity.ok(new SignUpResponse(true, "CoMM delete successfully"));
    }
    
    @PostMapping("/deleteCommCode")
    public ResponseEntity<?> deleteCommCode(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody CommCodeRequest request) {

    	commService.deleteCommCode(request);
        return ResponseEntity.ok(new SignUpResponse(true, "CoMM delete successfully"));
    }
}
