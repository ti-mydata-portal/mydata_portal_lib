package com.hanafn.openapi.portal.admin.viewsv2.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvApprovalRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvHisRegistRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRequest;
import com.hanafn.openapi.portal.admin.viewsv2.vo.BoApiVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.BoAuthVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.PrdAplvListVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.PrdAplvVO;

@Mapper
public interface AplvRepository {
	PrdAplvVO selectAplvOne(AplvApprovalRequest aplvDetailRequest);
	List<BoApiVO> selectPrdApiList(PrdAplvVO prdAplvVo);
	int countAplvList(AplvRequest aplvRequest);
	List<PrdAplvListVO> selectAplvList(AplvRequest aplvRequest);

	void insertAplvHis(AplvHisRegistRequest aplvRequest);

	void updateAplvInfo(AplvApprovalRequest aplvApprovalRequest);
	void updateAppPrdInfoForWait(AplvApprovalRequest aplvApprovalRequest);
	void updateAppPrdInfoForClose(AplvApprovalRequest aplvApprovalRequest);

	BoAuthVO selectOAuthClientDetail(PrdAplvVO prdAplvVO);
	void updateOauthClient(AdminRequest.AppsRequest appsRequest);

    PrdAplvVO selectAplvEmail(AplvApprovalRequest aplvRequest);
}
