package com.hanafn.openapi.portal.admin.viewsv2.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("boapps")
public class BoAppsVO {

    private String appKey;
    private String appClientId;
    private String newAppClientId;
    private String appNm;
    private String prdCtgy;
    private String prdKind;
    private String appStatCd;
    private String appAplvStatCd;
    private String appSvcStDt;
    private String appSvcEnDt;
    private String appCtnt;
    private String termEtdYn;
    private String appScrReisuYn;
    private String userKey;
    private String userNm;
    private String copRegNo;
    private String copNm;
    private String ownNm;
    private String copAddr1;
    private String copAddr2;
    private String copAddrNo;
    private String dirNm;
    private String dirTelNo1;
    private String dirTelNo2;
    private String dirEmail;
    private String dirDomain;
    private String appSvcNm;
    private String appSvcUrl;
}