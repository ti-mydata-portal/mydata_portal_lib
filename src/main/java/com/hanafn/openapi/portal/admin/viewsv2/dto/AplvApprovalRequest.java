package com.hanafn.openapi.portal.admin.viewsv2.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class AplvApprovalRequest {

    private String seqNo;
    private String rejectCtnt;
    private String aplvStatCd;
    private String regUserName;
    private String prdCtgy;
    private String mppCd1;
    private String mppCd2;
    private String mppCd3;

    @NotBlank
    private String aplvSeqNo;

    private String useFl;
	private String modUser;

    protected String regUser = "";
    protected String procUser = "";
    protected String procId = "";
}
