package com.hanafn.openapi.portal.admin.batch.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("iamAccount")
public class IamAccountVO {
    private String sdate;       // 서비스시작일
    private String edate;       // 서비스종료일
    private String userId;      // 아이디
    private String userName;    // 이름
    private String password;    // 비밀번
    private String deptNm;      // 부서
    private String deptCd;      // 부서코드
    private String email;       // 이메일
    private String phone;       // 전화번호
    private String respCd;      // 직책코드
    private String respNm;      // 직책명
    private String dutyCd;      // 직무코드
    private String dutyNm;      // 직무명
    private String titlCd;      // 직급코드
    private String titlNm;      // 직급명
    private String posiCd;      // 직위코드
    private String posiNm;      // 직위명
    private String famiCd;      // 직종코드
    private String famiNm;      // 직종명
    private String grouCd;      // 직군코드
    private String grouNm;      // 직군명
    private String updateDt;    // 최근업데이트일자
    private String useYn;       // 계정상태
    private String etc1;        // etc1 - 서비스카테고리(아이디, 마이데이터 등)
    private String etc2;        // etc2 - 권한(관리자, 서비스관리자, 개발자 등)
    private String etc3;
    private String etc4;
    private String etc5;
    private String etc6;
    private String etc7;
    private String etc8;
    private String etc9;
    private String etc10;
    private String etc11;
    private String etc12;
    private String etc13;
    private String etc14;
    private String etc15;
    private String etc16;
    private String etc17;
    private String etc18;
    private String etc19;
    private String etc20;
    private String empNo;       // 사번
    private String mobile;      // 핸드폰번호
    private String engId;       // 영문이름
    private String ipAddr;      // ip주소
}