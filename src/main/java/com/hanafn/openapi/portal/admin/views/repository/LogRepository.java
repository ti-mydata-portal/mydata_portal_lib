package com.hanafn.openapi.portal.admin.views.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.ApiLogRequest;
import com.hanafn.openapi.portal.admin.views.dto.ApiTrxCommInfoDTO;
import com.hanafn.openapi.portal.admin.views.dto.LogRequest;
import com.hanafn.openapi.portal.admin.views.dto.PortalLogRequest;
import com.hanafn.openapi.portal.admin.views.vo.ApiLogVO;
import com.hanafn.openapi.portal.admin.views.vo.ApiTrxCommInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.PortalLogVO;
import com.hanafn.openapi.portal.admin.views.vo.TrxInfoVO;

@Mapper
public interface LogRepository {
	PortalLogVO selectPortalLog(PortalLogRequest.PortalLogDetailRequest portalLogRequest);

	int countPortalLogList(PortalLogRequest portalLogRequest);
	List<PortalLogVO> selectPortalLogList(PortalLogRequest portalLogRequest);

	void insertPortalLog(LogRequest logRequest);

	ApiLogVO selectApiLog(ApiLogRequest.ApiLogDetailRequest apiLogDetailRequest);

	int countApiLogList(ApiLogRequest apiLogRequest);
	List<ApiLogVO> selectApiLogList(ApiLogRequest apiLogRequest);

	List<TrxInfoVO> selectTrxInfoAll();

	List<ApiTrxCommInfoVO> getApiTrxCommInfoList(ApiTrxCommInfoDTO.getListWithStatDTO dto);
	int getApiTrxCommInfoListTotCnt(ApiTrxCommInfoDTO.getListWithStatDTO dto);
}
