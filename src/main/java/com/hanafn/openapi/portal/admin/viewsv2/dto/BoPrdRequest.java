package com.hanafn.openapi.portal.admin.viewsv2.dto;

import lombok.Data;

@Data
public class BoPrdRequest {
    private String searchPrdCtgy;
    private String searchKind;
    private String searchServiceCd;
}
