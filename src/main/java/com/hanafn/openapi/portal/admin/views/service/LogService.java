package com.hanafn.openapi.portal.admin.views.service;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ApiRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.StatsRequest;
import com.hanafn.openapi.portal.admin.views.dto.ApiLogRequest;
import com.hanafn.openapi.portal.admin.views.dto.ApiLogRsponsePaging;
import com.hanafn.openapi.portal.admin.views.dto.ApiTrxCommInfoDTO;
import com.hanafn.openapi.portal.admin.views.dto.LogRequest;
import com.hanafn.openapi.portal.admin.views.dto.PortalLogRequest;
import com.hanafn.openapi.portal.admin.views.dto.PortalLogRsponsePaging;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.repository.ApiTrxCommInfoRepository;
import com.hanafn.openapi.portal.admin.views.repository.LogRepository;
import com.hanafn.openapi.portal.admin.views.vo.ApiLogVO;
import com.hanafn.openapi.portal.admin.views.vo.ApiTrxCommDetailWithStatVO;
import com.hanafn.openapi.portal.admin.views.vo.ApiTrxCommInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.ApiTrxCommInfoWithStatVO;
import com.hanafn.openapi.portal.admin.views.vo.ApiTrxCommInfoWithStatVOPaging;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.PortalLogVO;
import com.hanafn.openapi.portal.admin.views.vo.StatsDayInfoVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.service.ISecurity;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
@RequiredArgsConstructor
public class LogService {

    @Autowired
    MessageSourceAccessor messageSource;
    
    @Autowired
    AdminRepository adminRepository;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity aes256Util;
	private final LogRepository logRepository;
	private final ApiTrxCommInfoRepository apiTrxCommInfoRepository;
	private final ObjectMapper objectMapper;

	public PortalLogVO selectPortalLog(PortalLogRequest.PortalLogDetailRequest portalLogRequest){

        PortalLogVO vo = logRepository.selectPortalLog(portalLogRequest);
        if ("ROLE_PERSONAL".equals(vo.getRoleCd())) {
            if (vo.getUserNm() != null && !"".equals(vo.getUserNm())) {
                try {
                    vo.setUserNm(aes256Util.decrypt(vo.getUserNm()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

		return vo;
	}
	public PortalLogRsponsePaging selectPortalLogListPaging(PortalLogRequest portalLogRequest){
		if(portalLogRequest.getPageIdx() == 0)
			portalLogRequest.setPageIdx(portalLogRequest.getPageIdx() + 1);

		if(portalLogRequest.getPageSize() == 0){
			portalLogRequest.setPageSize(20);
		}

		portalLogRequest.setPageOffset((portalLogRequest.getPageIdx()-1)*portalLogRequest.getPageSize());

		int totCnt = logRepository.countPortalLogList(portalLogRequest);
		List<PortalLogVO> list = logRepository.selectPortalLogList(portalLogRequest);

        for (PortalLogVO vo : list) {
            try {
                if ("ROLE_PERSONAL".equals(vo.getRoleCd())) {
                    if (vo.getUserNm() != null && !"".equals(vo.getUserNm())) {
                        //String tmp = CommonUtil.maskingId(vo.getUserId())+"("+CommonUtil.maskingName(AES256Util.decrypt(vo.getUserNm()))+")";
                        vo.setUserNm(aes256Util.decrypt(vo.getUserNm()));
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage());
                throw new BusinessException("E026", messageSource.getMessage("E026"));
            }
        }

		PortalLogRsponsePaging pagingData = new PortalLogRsponsePaging();
		pagingData.setPageIdx(portalLogRequest.getPageIdx());
		pagingData.setPageSize(portalLogRequest.getPageSize());
		pagingData.setList(list);
		pagingData.setTotCnt(totCnt);
		pagingData.setSelCnt(list.size());

		return pagingData;
	}

	public void insertPortalLog(LogRequest logRequest){
		logRepository.insertPortalLog(logRequest);
	}

	public ApiLogVO selectApiLog(ApiLogRequest.ApiLogDetailRequest apiLogDetailRequest){
	    ApiLogVO data = logRepository.selectApiLog(apiLogDetailRequest);

        try {
            if (data.getUserNm() != null && !"".equals(data.getUserNm())) {
                String decryptedUserNm = aes256Util.decrypt(data.getUserNm());
                data.setUserNm(decryptedUserNm);
                log.debug("복호화 - 이름: {}", decryptedUserNm);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BusinessException("E026", messageSource.getMessage("E026"));
        }

	    return data;
	}

	public ApiLogRsponsePaging selectApiLogListPaging(ApiLogRequest request) throws Exception{
		if(request.getPageIdx() == 0)
            request.setPageIdx(request.getPageIdx() + 1);

		if(request.getPageSize() == 0){
            request.setPageSize(20);
		}

        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());

        if (StringUtils.isNotBlank(request.getSearchUserKey())) {
            try {
                String userName = aes256Util.encrypt(request.getSearchUserKey());
                request.setSearchUserKey(userName);
                log.debug("암호화 - 이용자 : {}", aes256Util.encrypt(request.getSearchUserKey()));
            } catch (GeneralSecurityException e) {
                log.error("암호화 오류1", e);
            } catch (UnsupportedEncodingException e) {
				log.error("암호화 오류2", e);
            }
        }

		int totCnt = logRepository.countApiLogList(request);
		List<ApiLogVO> list = logRepository.selectApiLogList(request);

        for (ApiLogVO apiLogVO: list) {
            try {
                if (apiLogVO.getUserNm() != null && !"".equals(apiLogVO.getUserNm())) {
                    String decryptedUserNm = aes256Util.decrypt(apiLogVO.getUserNm());
                    apiLogVO.setUserNm(decryptedUserNm);
                    log.debug("복호화 - 이름: {}", decryptedUserNm);
                }
            } catch (UnsupportedEncodingException e) {
                log.error(e.getMessage());
                throw new BusinessException("E026",messageSource.getMessage("E026"));
            } catch ( Exception e ) {
                log.error(e.getMessage());
                throw new BusinessException("E026",messageSource.getMessage("E026"));
            }
        }

		ApiLogRsponsePaging pagingData = new ApiLogRsponsePaging();
		pagingData.setPageIdx(request.getPageIdx());
		pagingData.setPageSize(request.getPageSize());
		pagingData.setList(list);
		pagingData.setTotCnt(totCnt);
		pagingData.setSelCnt(list.size());

		return pagingData;
	}

	public ApiTrxCommInfoWithStatVOPaging getApiTrxCommInfoListWithStat(ApiTrxCommInfoDTO.getListWithStatDTO dto){

		// 0. Get List [ API_TRX_COMM_INFO ] FROM TABLE [ API_TRX_COMM_INFO ] BY [ HFN_CD, ENTR_CD, APP_KEY, HFN_SVC_CD, DATE ]
		List<ApiTrxCommInfoVO> apiTrxCommInfoVOList = logRepository.getApiTrxCommInfoList(dto);

		// 1. 가져온 COMM_INFO 기반 [ ApiTrxCommInfoWithStatVO LIST ] 생성
		List<ApiTrxCommInfoWithStatVO> apiTrxCommInfoWithStatVOList = new ArrayList<ApiTrxCommInfoWithStatVO>(apiTrxCommInfoVOList.size());

		for( ApiTrxCommInfoVO item : apiTrxCommInfoVOList) {
			apiTrxCommInfoWithStatVOList.add(objectMapper.convertValue(item, ApiTrxCommInfoWithStatVO.class));
		}

		for( ApiTrxCommInfoWithStatVO item : apiTrxCommInfoWithStatVOList)
		{

			// 1-1. COMM_INFO의 HFN_SVC_CD 기준으로 API 목록 가져온다.
			ApiRequest apiRequest = new ApiRequest();
//			apiRequest.setSearchHfnSvcCd(item.getFileHfnSvcCd());
			List<ApiVO> apiListByHfnSvcCd = adminRepository.selectApiByHfnSvcCd(apiRequest);
			List<ApiTrxCommDetailWithStatVO> apiTrxCommDetailWithStatVOList = new ArrayList<ApiTrxCommDetailWithStatVO>(apiListByHfnSvcCd.size());

			// 1-2. 가져온 API 목록 기반으로 COMM_DETAIL 목록 생성
			for ( ApiVO api : apiListByHfnSvcCd ) {
				StatsRequest statsRequest = new StatsRequest();
				statsRequest.setSearchAppKey(item.getFileAppKey());
				statsRequest.setSearchApiId(api.getApiId());
				statsRequest.setSearchTrxDt(item.getFileTrxDt());

                StatsDayInfoVO statsDayInfoVO = adminRepository.getStatsDayInfoVO(statsRequest);
				if(statsDayInfoVO == null) {
					log.error("해당 APPKEY [{}] API_ID[{}] 의 {} ~ {} 간의 집계 내용은 없습니다.", item.getFileAppKey(), api.getApiId(), dto.getSearchStDt(), dto.getSearchEnDt());
					apiTrxCommDetailWithStatVOList.add(
							ApiTrxCommDetailWithStatVO.builder()
									.seqNo(item.getSeqNo())
									.apiId(api.getApiId())
									.appNm(item.getAppNm())
									.apiNm(api.getApiNm())
									.trxDt(item.getFileTrxDt())
									.apiTrxCnt("0")
									.gwErrorCnt("0")
									.apiErrorCnt("0")
									.svcErrorCnt("0")
									.platformApiTrxCnt("0")
									.build()
					);
					continue;
				}
				
				int platformApiTrxCnt = Integer.valueOf(statsDayInfoVO.getApiTrxCnt()) - Integer.valueOf(statsDayInfoVO.getGwErrorCnt()) - Integer.valueOf(statsDayInfoVO.getSvcErrorCnt());
				apiTrxCommDetailWithStatVOList.add(
						ApiTrxCommDetailWithStatVO.builder()
								.seqNo(item.getSeqNo())
								.apiId(api.getApiId())
								.appNm(item.getAppNm())
								.apiNm(api.getApiNm())
								.trxDt(item.getFileTrxDt())
								.apiTrxCnt(statsDayInfoVO.getApiTrxCnt())
								.gwErrorCnt(statsDayInfoVO.getGwErrorCnt())
								.apiErrorCnt(statsDayInfoVO.getApiErrorCnt())
								.svcErrorCnt(statsDayInfoVO.getSvcErrorCnt())
								.platformApiTrxCnt(Integer.toString(platformApiTrxCnt))
								.build()
				);
			}

			// 1-3. 상세 집계 목록 생성 종료 후 세팅
			item.setApiTrxCommDetailWithStatList(apiTrxCommDetailWithStatVOList);
			item.setTotApiTrxCnt( Integer.toString( apiTrxCommDetailWithStatVOList.stream().map( x -> Integer.valueOf(x.getApiTrxCnt()) ).reduce(0, Integer::sum) ) );
			item.setTotGwErrorCnt( Integer.toString( apiTrxCommDetailWithStatVOList.stream().map( x -> Integer.valueOf(x.getGwErrorCnt()) ).reduce(0, Integer::sum) ) );
			item.setTotApiErrorCnt( Integer.toString( apiTrxCommDetailWithStatVOList.stream().map( x -> Integer.valueOf(x.getApiErrorCnt()) ).reduce(0, Integer::sum) ) );
			item.setTotSvcErrorCnt( Integer.toString( apiTrxCommDetailWithStatVOList.stream().map( x -> Integer.valueOf(x.getSvcErrorCnt()) ).reduce(0, Integer::sum) ) );
			item.setTotPlatformApiTrxCnt( Integer.toString( apiTrxCommDetailWithStatVOList.stream().map( x -> Integer.valueOf(x.getPlatformApiTrxCnt()) ).reduce(0, Integer::sum) ) );
		}
		// 2. 최종 페이징 처리하여 리턴
		ApiTrxCommInfoWithStatVOPaging apiTrxCommInfoWithStatVOPaging = new ApiTrxCommInfoWithStatVOPaging();
		apiTrxCommInfoWithStatVOPaging.setList(apiTrxCommInfoWithStatVOList);

		int totCnt = logRepository.getApiTrxCommInfoListTotCnt(dto);
		apiTrxCommInfoWithStatVOPaging.setTotCnt(totCnt);
		apiTrxCommInfoWithStatVOPaging.setSelCnt(apiTrxCommInfoWithStatVOList.size());
		return apiTrxCommInfoWithStatVOPaging;
	}

	public Optional<?> getApiTrxCommInfo(ApiTrxCommInfoDTO.getDTO dto){
		return apiTrxCommInfoRepository.findBySeqNo(dto.getSeqNo());
	}
}
