package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("product")
public class ProductVO {
    private String prdId;
    private String prdCd;
    private String prdNm;
    private String prdCtgy;
    private String prdDesc;
    private String prdOrder;
    private String iconPath;
    private String filePath;
    private String iconNm;
    private String fileNm;
    private String htmlCtnt;
    private String persYn;
    private String openYn;
    private String scope;
    private String grantType;
    private String devHtmlCtnt;
    private String prdKind;
    private String serviceCd;
    private int feeAmount;
    private String applDoc;
    private String limitYn;
    private String prdMemo;
    private String regUser;
    private String regDttm;
    private String modUser;
    private String modDttm;
    private String useFl;
    private String appKey;
    private String prdStstCd;
    private String dispPrdNm;
    private String regUserId;
    private String modUserId;
    private String seqNo;
    private String batchMailYn;

    //매핑정보 - 백오피스등록 겸용
    private String mppCd1;
    private String mppCd2;
    private String mppCd3;
    private String mppUser;
    private String prdStatus;

    private String appPrdSeqNo;
}

