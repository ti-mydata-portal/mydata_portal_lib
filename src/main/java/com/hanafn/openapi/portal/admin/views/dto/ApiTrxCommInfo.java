package com.hanafn.openapi.portal.admin.views.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "PORTAL_API_TRX_COMM_INFO")
@Data
@EqualsAndHashCode(callSuper=false)
public class ApiTrxCommInfo {
    @Id
    @Column(name="SEQ_NO")
    private String seqNo;

    @NotBlank
    @Size(max = 8)
    @Column(name="FILE_TRX_DT")
    private String fileTrxDt;

    @NotBlank
    @Size(max = 3)
    @Column(name="FILE_HFN_CD")
    private String fileHfnCd;

    @NotBlank
    @Size(max = 10)
    @Column(name="FILE_HFN_SVC_CD")
    private String fileHfnSvcCd;

    @Column(name="FILE_API_TRX_CNT")
    private String fileApiTrxCnt;

    @Column(name="REG_DTTM")
    private String regDttm;
}
