package com.hanafn.openapi.portal.admin.batch;

import com.hanafn.openapi.portal.admin.batch.dto.BatchUserRequest;
import com.hanafn.openapi.portal.admin.batch.vo.IamAccountVO;
import com.hanafn.openapi.portal.admin.views.vo.HfnUserVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IamRepository {
	List<IamAccountVO> selectIamAccount();
	List<HfnUserVO> selectHfnUser(BatchUserRequest.HfnUserRequest request);
	void insertHfnUserInfo(BatchUserRequest.HfnUserRequest request);
	void insertHfnUserRole(BatchUserRequest.HfnUserRequest request);
	void updateHfnUserInfo(BatchUserRequest.HfnUserRequest request);
	void updateHfnUserRole(BatchUserRequest.HfnUserRequest request);
}