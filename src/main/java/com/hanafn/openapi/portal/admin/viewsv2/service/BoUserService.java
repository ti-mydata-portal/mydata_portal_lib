package com.hanafn.openapi.portal.admin.viewsv2.service;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.admin.viewsv2.dto.PortalUserRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.PortalUserRsponse;
import com.hanafn.openapi.portal.admin.viewsv2.repository.PortalUserRepository;
import com.hanafn.openapi.portal.admin.viewsv2.vo.PortalUserVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.service.ISecurity;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class BoUserService {
	@Autowired
	PortalUserRepository portalUserRepository;

	@Autowired
	MessageSourceAccessor messageSource;
	
	@Autowired
	@Qualifier("Crypto")
    ISecurity aes256Util;

	/**
	 * 포털사용자 목록 조회
	 * @param userRequest
	 * @return
	 * @throws GeneralSecurityException
	 */
	public PortalUserRsponse selectUserListPaging(PortalUserRequest userRequest) throws BusinessException {



		int totCnt = portalUserRepository.countUserList(userRequest);
		List<PortalUserVO> list = portalUserRepository.selectUserList(userRequest);

		// 복호화
		for (PortalUserVO portalUserVO : list) {
			try {
				// 이름
				if (portalUserVO.getUserNm() != null && !"".equals(portalUserVO.getUserNm())) {
					String decryptedUserNm = aes256Util.decrypt(portalUserVO.getUserNm());
					portalUserVO.setUserNm(decryptedUserNm);
					log.debug("복호화 - 이름: {}", decryptedUserNm);
				}
			} catch ( UnsupportedEncodingException e ) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch ( Exception e ) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			}

			try {
				// 전화번호
				if (portalUserVO.getUserTel() != null && !"".equals(portalUserVO.getUserTel())) {
					String decryptedUserTel = aes256Util.decrypt(portalUserVO.getUserTel());
					portalUserVO.setUserTel(decryptedUserTel);
					log.debug("복호화 - 이름: {}", decryptedUserTel);
				}
			} catch ( UnsupportedEncodingException e ) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch ( Exception e ) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			}

			try {
				// 이메일
				if (portalUserVO.getUserEmail() != null && !"".equals(portalUserVO.getUserEmail())) {
					String decryptedUserEmail = aes256Util.decrypt(portalUserVO.getUserEmail());
					portalUserVO.setUserEmail(decryptedUserEmail);
					log.debug("복호화 - 이름: {}", decryptedUserEmail);
				}
			} catch ( UnsupportedEncodingException e ) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch ( Exception e ) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			}
		}

		PortalUserRsponse response = new PortalUserRsponse();
		response.setList(list);
		response.setTotCnt(totCnt);

		return response;
	}
}
