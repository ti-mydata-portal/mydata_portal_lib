package com.hanafn.openapi.portal.admin.viewsv2.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.PrdAplvVO;

import lombok.Data;

@Data
public class PrdAplvDetailResponse {
    private String resultCode = "0000";
    private String message = "정상처리되었습니다.";

    private PrdAplvVO info;
    private List<ApiVO> apiList;
}
