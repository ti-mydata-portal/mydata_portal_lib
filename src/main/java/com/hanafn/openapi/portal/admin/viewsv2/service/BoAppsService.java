package com.hanafn.openapi.portal.admin.viewsv2.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsCnlKeyRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRegRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsScrRequest;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.AppPrdInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoAppPrdAplvRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoAppPrdRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoAppPrdRsponse;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoAppsRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoAppsRsponsePaging;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoPrdApiRsponse;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoPrdRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoPrdRsponse;
import com.hanafn.openapi.portal.admin.viewsv2.repository.AplvRepository;
import com.hanafn.openapi.portal.admin.viewsv2.repository.BoAppsRepository;
import com.hanafn.openapi.portal.admin.viewsv2.vo.BoApiVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.BoAppPrdVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.BoAppsVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.BoPrdVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.PrdAplvVO;
import com.hanafn.openapi.portal.cmct.GWCommunicater;
import com.hanafn.openapi.portal.cmct.RedisCommunicater;
import com.hanafn.openapi.portal.cmct.dto.GWResponse;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.DateUtil;
import com.hanafn.openapi.portal.util.service.ISecurity;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class BoAppsService {
	private static final Logger LOGGER = LoggerFactory.getLogger(BoAppsService.class);

	@Autowired
	AplvService aplvService;

	@Autowired
	BoAppsRepository boAppsRepository;

	@Autowired
	AplvRepository aplvRepository;
	@Autowired
	MessageSourceAccessor messageSource;
	@Autowired
    AdminRepository adminRepository;
	
	@Autowired
	@Qualifier("Crypto")
    ISecurity aes256Util;

	/**
	 * 백오피스용 앱목록 조회
	 * @param request
	 * @return
	 */
	public BoAppsRsponsePaging appListSelected(BoAppsRequest request) {

		int totCnt = boAppsRepository.countAppsList(request);
		List<BoAppsVO> list = boAppsRepository.selectAppsList(request);

		for (BoAppsVO appsVo: list) {
			try {
				// 이름
				if (appsVo.getUserNm() != null && !"".equals(appsVo.getUserNm())) {
					String decryptedUserNm = aes256Util.decrypt(appsVo.getUserNm());
					appsVo.setUserNm(decryptedUserNm);
					log.debug("복호화 - 이름: {}", decryptedUserNm);
				}
			} catch (UnsupportedEncodingException e) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch ( Exception e ) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			}
		}

		BoAppsRsponsePaging resp = new BoAppsRsponsePaging();
		resp.setList(list);
		resp.setTotCnt(totCnt);

		return resp;
	}

	/**
	 * 앱에 등록된 상품목록 조회
	 * @param request
	 * @return
	 */
	public BoAppPrdRsponse appPrdListSelected(BoAppsRequest request) {
		List<BoAppPrdVO> list = boAppsRepository.selectAppPrdList(request.getAppKey());

		BoAppPrdRsponse resp = new BoAppPrdRsponse();
		resp.setList(list);
		return resp;
	}

	/**
	 * 상품 api목록조회
	 */
	public BoPrdApiRsponse selectPrdApiList(AdminRequest.ProductRequest productRequest) {
		//API 목록 조회
		PrdAplvVO prdAplvVo = new PrdAplvVO();
		prdAplvVo.setPrdId(productRequest.getPrdId());
		prdAplvVo.setPrdCtgy(productRequest.getPrdCtgy());
		List<BoApiVO> apiList = aplvRepository.selectPrdApiList(prdAplvVo);

		BoPrdApiRsponse resp = new BoPrdApiRsponse();
		resp.setList(apiList);
		return resp;
	}

	/**
	 *
	 * @param appsRegRequest
	 */
	public void insertApp(AdminRequest.AppsRegRequest appsRegRequest) throws BusinessException {
		try {
			String strPrdKind = appsRegRequest.getPrdKind();
			String flKey = "WAIT";

			AdminRequest.AplvRegistRequest aplvRequest = new AdminRequest.AplvRegistRequest();
			aplvRequest.setRegUserId(appsRegRequest.getRegUserId());
			aplvRequest.setHfnCd(appsRegRequest.getHfnCd());

			List<String> lstGrantType = new ArrayList<String>() ; //기본
			List<String> lstScope = new ArrayList<String>();

			AdminRequest.ProductRequest productRequest ;
			ProductVO productDetailVo ;
			for (ProductVO vo : appsRegRequest.getPrdList()) {
				//서비스코드로 상품 검색
				ProductVO getVo = boAppsRepository.getPrdIdByServiceCode(vo);
				vo.setPrdId(getVo.getPrdId());
				vo.setRegUserId(appsRegRequest.getRegUserId());
				vo.setAppKey(appsRegRequest.getAppKey());

				String prdKind = getVo.getPrdKind();
				vo.setUseFl("N");
				adminRepository.insertAppPrdInfo(vo);

				//승인요청정보 생성.
				aplvRequest.setAplvReqCd(vo.getAppPrdSeqNo());
				aplvRequest.setAplvDivCd("APPPRDREG");

				aplvRequest.setAplvReqCtnt(getVo.getPrdNm());
				adminRepository.insertAplv(aplvRequest);

				//grant_type, scope 수집
				productRequest = new AdminRequest.ProductRequest();
				productRequest.setPrdId(Integer.parseInt(vo.getPrdId()));
				productDetailVo = adminRepository.selectPrdDetail(productRequest);
				if (getVo.getGrantType() != null && !getVo.getGrantType().equals("")) {
					lstGrantType.addAll(Arrays.asList(getVo.getGrantType().split(",")));
				}
				if (getVo.getScope() != null && !getVo.getScope().equals("")) {
					lstScope.addAll(Arrays.asList(getVo.getScope().split(",")));
				}
			}
			TreeSet<String> trsetGrantType = new TreeSet<String>(lstGrantType);
			ArrayList<String> arrGrantType = new ArrayList<String>(trsetGrantType);

			TreeSet<String> trScope = new TreeSet<String>(lstScope);
			ArrayList<String> arrScope = new ArrayList<String>(trScope);

			String strGrantTypes = String.join(",",trsetGrantType);
			String strScope = String.join(",",trScope);

			String copRegNo = appsRegRequest.getCopRegNo();
			String copNm = appsRegRequest.getCopNm();

			adminRepository.insertAppCopInfo(appsRegRequest);
			adminRepository.backupAppCopInfo(appsRegRequest);

			insertAppCnlKey(appsRegRequest, "Y");

			AdminRequest.AppsRequest appsRequest = new AdminRequest.AppsRequest();
			appsRequest.setAppKey(appsRegRequest.getAppKey());

			appsRegRequest.setAppSvcEnDt("20991231");
			GWResponse oAuthResponse = GWCommunicater.createClientInfo(strPrdKind, strGrantTypes, strScope, appsRegRequest.getAppSvcEnDt(), appsRegRequest.getRegUserId());
			Map<String, Object> oAuthBodyResponse = oAuthResponse.getDataBody();

			String encryptScr = "";
			String decryptId = (String) oAuthBodyResponse.get("clientId");
			String buf = (String) oAuthBodyResponse.get("clientSecret");

			encryptScr = aes256Util.encrypt(buf);


			appsRegRequest.setAppClientId(decryptId);
			appsRegRequest.setAppScr(encryptScr);
			appsRegRequest.setAppSvcStDt(appsRegRequest.getAppSvcStDt());
			appsRegRequest.setAppSvcEnDt(appsRegRequest.getAppSvcEnDt());

			// 앱등록 승인 후 처리
			// INSERT INTO PORTAL_APP_INFO
			// DELETE FROM PORTAL_APP_INFO_MOD
			// INSERT INTO PORTAL_APP_SCR_HIS
			// UPDATE PORTAL_APP_INFO (APP_STAT_CD, APP_APLV_STAT_CD)
			this.insertAppAfterAplv(appsRegRequest);
			aplvRequest.setAplvReqCd(appsRegRequest.getAppKey());
			aplvRequest.setAplvDivCd("APP");
			aplvRequest.setAplvReqCtnt(appsRegRequest.getAppNm());
			appsRegRequest.setUserKey(appsRegRequest.getRegUserId());
			adminRepository.insertAplv2(aplvRequest);
			adminRepository.backupAppInfo(appsRegRequest);
			adminRepository.backupAppOwnerInfo(appsRegRequest);

			//api정보, 채널정보를 사용중으로 상태변경
			AdminRequest.AppsApiInfoRequest aar = new AdminRequest.AppsApiInfoRequest();
			AdminRequest.AppsCnlKeyRequest acr = new AdminRequest.AppsCnlKeyRequest();
			aar.setAppKey(appsRequest.getAppKey());
			acr.setAppKey(appsRequest.getAppKey());

			// UPDATE PORTAL_APP_API_INFO
			// UPDATE PORTAL_APP_CHANNL_INFO
			adminRepository.updateAppCnlInfo(acr);

			//앱에 등록된 상품목록 조회.
			AplvRequest aplvReq = new AplvRequest();
			aplvReq.setAppKey(appsRequest.getAppKey());
			aplvReq.setSearchPrdCtgy(appsRegRequest.getHfnCd());
			aplvReq.setProcId(appsRegRequest.getRegUserId());
			aplvReq.setProcUser(appsRegRequest.getRegUser());
			//기본 구분값은 앱-상품승인 대기
			aplvReq.setSearchAplvStatCd("WAIT");
			aplvReq.setSearchFromDate(DateUtil.formatDateTime("yyyy-MM-dd"));
			aplvReq.setSearchToDate(DateUtil.formatDateTime("yyyy-MM-dd"));
			aplvReq.setPageIdx(1);
			aplvReq.setPageSize(20); //백오피스에서 전달하는 상품은 1개밖에 없음..
			aplvReq.setPageOffset((aplvReq.getPageIdx() - 1) * aplvReq.getPageSize());

			AdminRequest.ApiDetailRequest apiDetailRequest = new AdminRequest.ApiDetailRequest();
			apiDetailRequest.setAppKey(appsRegRequest.getAppKey());

			//Redis에 API SET
			List<ApiVO> apiList = adminRepository.selectAppApiList(apiDetailRequest);

			// Redis에 app 정보 세팅
			RedisCommunicater.appRedisSet(decryptId, "true");

			for (ApiVO apiInfo : apiList) {
				RedisCommunicater.appApiRedisSet(decryptId, apiInfo.getApiUrl(), "true");
			}

			//Redis에 APP IP 등록
			List<AdminRequest.AppsCnlKeyRequest> apiChannlList = appsRegRequest.getCnlKeyList();
			for (AdminRequest.AppsCnlKeyRequest appCnlInfo : apiChannlList) {
				RedisCommunicater.appIpRedisSet(decryptId, appCnlInfo.getCnlKey(), "true");
			}

		} catch (UnsupportedEncodingException e) {
			log.error(e.toString());
			throw new BusinessException("E075",messageSource.getMessage("E075"));
		} catch ( Exception e) {
			log.error(e.toString());
			throw new BusinessException("E075",messageSource.getMessage("E075"));
		}
	}

	/**
	 * 상품추가
	 * @param appsRegRequest
	 */
	public void addAppPrd(AdminRequest.AppsRegRequest appsRegRequest) throws BusinessException {
		try {
			AdminRequest.AplvRegistRequest aplvRequest = new AdminRequest.AplvRegistRequest();
			aplvRequest.setRegUserId(appsRegRequest.getRegUserId());
			aplvRequest.setHfnCd(appsRegRequest.getHfnCd());

			for (ProductVO vo : appsRegRequest.getPrdList()) {
				//서비스코드로 상품 검색
				ProductVO getVo = boAppsRepository.getPrdIdByServiceCode(vo);
				vo.setPrdId(getVo.getPrdId());
				vo.setRegUserId(appsRegRequest.getRegUserId());
				vo.setAppKey(appsRegRequest.getAppKey());

				String prdKind = getVo.getPrdKind();
				vo.setUseFl("N");
				adminRepository.insertAppPrdInfo(vo);


				//승인요청정보 생성.
				aplvRequest.setAplvReqCd(vo.getAppPrdSeqNo());
				aplvRequest.setAplvDivCd("APPPRDREG");

				aplvRequest.setAplvReqCtnt(getVo.getPrdNm());
				adminRepository.insertAplv(aplvRequest);

			}

			//앱에 등록된 상품목록 조회.
			com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRequest aplvReq = new com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRequest();
			aplvReq.setAppKey(appsRegRequest.getAppKey());
			aplvReq.setSearchPrdCtgy(appsRegRequest.getHfnCd());
			aplvReq.setProcId(appsRegRequest.getRegUserId());
			aplvReq.setProcUser(appsRegRequest.getRegUser());
			//기본 구분값은 앱-상품승인 대기
			aplvReq.setSearchAplvStatCd("WAIT");
			aplvReq.setSearchFromDate(DateUtil.formatDateTime("yyyy-MM-dd"));
			aplvReq.setSearchToDate(DateUtil.formatDateTime("yyyy-MM-dd"));
			aplvReq.setPageIdx(1);
			aplvReq.setPageSize(20); //백오피스에서 전달하는 상품은 1개밖에 없음..

			aplvReq.setPageOffset((aplvReq.getPageIdx() - 1) * aplvReq.getPageSize());

			//AplvRsponsePaging aplvRes = aplvService.selectPrdAplvListPaging(aplvReq);

			//app의 client_id 조회
			AdminRequest.AppsRequest appsRequest = new AdminRequest.AppsRequest();
			appsRequest.setAppKey(appsRegRequest.getAppKey());
			AppsVO appsVo = adminRepository.selectAppDetail(appsRequest);
			String decryptId = appsVo.getAppClientId();

			AdminRequest.ApiDetailRequest apiDetailRequest = new AdminRequest.ApiDetailRequest();
			apiDetailRequest.setAppKey(appsRegRequest.getAppKey());

			//Redis에 API SET. (new_apps_client_id가 있는 경우, 배치에서 일괄처리하므로 현재 client_id만 처리)
			List<ApiVO> apiList = adminRepository.selectAppApiList(apiDetailRequest);
			for (ApiVO apiInfo : apiList) {
				RedisCommunicater.appApiRedisSet(decryptId, apiInfo.getApiUrl(), "true");
			}
		} catch (BusinessException e) {
			log.error(e.toString());
			throw new BusinessException("E075","상품을 추가할 수 없습니다.");
		} catch ( Exception e) {
			log.error(e.toString());
			throw new BusinessException("E075","상품을 추가할 수 없습니다.");
		}
	}

	/**
	 * 상품정보 변경
	 * @param appsRegRequest
	 */
	public void modifyAppPrd(AdminRequest.AppsRegRequest appsRegRequest) {
		try {
			BoAppPrdVO boAppPrdVO;

			for (ProductVO vo : appsRegRequest.getPrdList()) {
				boAppPrdVO = new BoAppPrdVO();
				boAppPrdVO.setAppKey(appsRegRequest.getAppKey());
				boAppPrdVO.setPrdCtgy(vo.getPrdCtgy());
				boAppPrdVO.setPrdKind(vo.getPrdKind());
				boAppPrdVO.setPrdId(vo.getPrdId());
				boAppPrdVO.setServiceCd(vo.getServiceCd());

				AppPrdInfoVO appPrdInfoVO = boAppsRepository.selectAppPrdListBySvcCd(boAppPrdVO);
				if (appPrdInfoVO == null)
					throw new BusinessException("E075", "수정할 상품정보가 존재하지 않습니다.");

				vo.setSeqNo(appPrdInfoVO.getSeqNo());
				boAppsRepository.updateAppPrdMappingInfo(vo);

				try {
					//APP PRD Redis set - mppCd1
					AppsRequest appsRequest = new AppsRequest();
					appsRequest.setAppKey(appsRegRequest.getAppKey());
					AppsVO appsVo = adminRepository.selectAppDetail(appsRequest);
					String decryptId = appsVo.getAppClientId();

					PrdAplvVO prdAplvVo = new PrdAplvVO();
					prdAplvVo.setPrdId(Integer.parseInt(vo.getPrdId()));
					prdAplvVo.setPrdCtgy(vo.getPrdCtgy());
					List<BoApiVO> apiList = aplvRepository.selectPrdApiList(prdAplvVo);
					for (BoApiVO apiVo : apiList) {
						RedisCommunicater.appPrdMppCd1RedisSet(vo.getPrdCtgy(), decryptId, apiVo.getApiUrl(), vo.getMppCd1());
					}
				} catch (BusinessException be){
					LOGGER.error("redis set biz err..", be);
				} catch (Exception e){
					LOGGER.error("redis set err..", e);
				}
			}
		} catch (BusinessException be) {
			throw be;
		} catch (Exception e) {
			log.error(e.toString());
			throw new BusinessException("E075", "상품정보를 수정할 수 없습니다.");
		}
	}

	/**
	 * 앱상품상태 업데이트
	 * @param boAppPrdAplvRequest
	 */
	public void updateAppPrdStatus(BoAppPrdAplvRequest boAppPrdAplvRequest) {
		try {
			String useFl;

			BoAppPrdVO boAppPrdVO = new BoAppPrdVO();
			boAppPrdVO.setAppKey(boAppPrdAplvRequest.getAppKey());
			boAppPrdVO.setPrdId(boAppPrdAplvRequest.getPrdId());
			boAppPrdVO.setServiceCd(boAppPrdAplvRequest.getServiceCd());

			AppPrdInfoVO appPrdInfoVo = boAppsRepository.selectAppPrdListBySvcCd(boAppPrdVO);
			if (appPrdInfoVo == null)
				throw new BusinessException("E075", "수정할 상품정보가 존재하지 않습니다.");

			ProductVO vo = new ProductVO();
			vo.setSeqNo(appPrdInfoVo.getSeqNo());
			vo.setMppCd1(boAppPrdAplvRequest.getMppCd1());
			vo.setMppCd2(boAppPrdAplvRequest.getMppCd2());
			vo.setMppCd3(boAppPrdAplvRequest.getMppCd3());
			if (boAppPrdAplvRequest.getPrdStatus().equals("10")) {
				useFl = "Y";
			} else if (boAppPrdAplvRequest.getPrdStatus().equals("20")) {
				useFl = "N";
			} else {
				throw new BusinessException("E075", "알수없는 상태코드입니다.("+ boAppPrdAplvRequest.getPrdStatus()+")");
			}
			vo.setUseFl(useFl);
			vo.setModUserId(boAppPrdAplvRequest.getModUserId());
			//상태 업데이트.
			boAppsRepository.updateAppPrdStatus(vo);

			//app의 client_id 조회
			AppsRequest appsRequest = new AppsRequest();
			appsRequest.setAppKey(boAppPrdAplvRequest.getAppKey());
			AppsVO appsVo = adminRepository.selectAppDetail(appsRequest);
			String decryptId = appsVo.getAppClientId();
//			//APP PRD Redis set
//			RedisCommunicater.appPrdRedisSet(decryptId, appPrdInfoVo.getPrdId(), useFl.equals("Y") ? "true" : "false");
		} catch (BusinessException be) {
			throw be;
		} catch (Exception e) {
			log.error(e.toString());
			throw new BusinessException("E075", "상품 상태를 변경할 수 없습니다.");
		}
	}

	/**
	 * 상품잔여건수 통지
	 * Redis에 총건수, 잔여건수를 저장.
	 * @param appPrdRequest
	 */
	public void appPrdRemainCnt(BoAppPrdRequest appPrdRequest) {
		try {
			//만료일이 현재일 이전이면 redis 접근권한 false
			String useFl;
			for (BoAppPrdVO vo : appPrdRequest.getPrdList()) {
				vo.setAppKey(appPrdRequest.getAppKey());

				//db 갱신
				AppPrdInfoVO appPrdInfoVo = boAppsRepository.selectAppPrdListBySvcCd(vo);
				if (appPrdInfoVo == null)
					throw new BusinessException("E075", "수정할 상품정보가 존재하지 않습니다.");

				vo.setSeqNo(appPrdInfoVo.getSeqNo());
//				vo.setUseEndDt(vo.getEndDate());
//				vo.setRestCnt((vo.getRemainCount()));
				vo.setModUserId(appPrdRequest.getProdUserId());
				boAppsRepository.updateAppPrdEndDt(vo);

				AppsRequest appsRequest = new AppsRequest();
				appsRequest.setAppKey(appPrdRequest.getAppKey());
				AppsVO appsVo = adminRepository.selectAppDetail(appsRequest);
				String decryptId = appsVo.getAppClientId();

				String today = DateUtil.getCurrentDate();
				

				if (appPrdInfoVo.getLimitYn().equals("Y")) { //gw에서 한도측정 사용
					if (today.compareTo(vo.getUseEndDate()) > 0)
					{
						//잔여한도 없음.. 한도를 0으로
						RedisCommunicater.appPrdLimitRedisSet(decryptId, appPrdInfoVo.getPrdCd(), "0", "0");
					}else {
						//잔여한도만 반영. 현재 개수는 0으로. 갱신되는 잔여갯수만큼한 허용
						RedisCommunicater.appPrdLimitRedisSet(decryptId, appPrdInfoVo.getPrdCd(), String.valueOf(vo.getRestCnt()), "0");
					}
				}else { //gw에서 한도측정 미사용. 한도건수를 제거함.
					RedisCommunicater.appPrdLimitRedisDel(decryptId, appPrdInfoVo.getPrdCd());
				}
			}
			//redis 잔여한도 설정
		} catch (BusinessException be) {
			throw be;
		} catch (Exception e) {
			log.error(e.toString());
			throw new BusinessException("E075", "상품 상태를 변경할 수 없습니다.");
		}
	}

	private void insertAppCnlKey(AppsRegRequest appsRegRequest, String useFL) {
		for(AppsCnlKeyRequest cnlKey : appsRegRequest.getCnlKeyList()){
			cnlKey.setAppKey(appsRegRequest.getAppKey());
			cnlKey.setRegUser(appsRegRequest.getRegUser());
			cnlKey.setUseFl(useFL);
			cnlKey.setRegUserId(appsRegRequest.getRegUserId());
			adminRepository.insertAppCnlKey(cnlKey);
		}
	}

	private void insertAppAfterAplv(AppsRegRequest appsRegRequest) {

		adminRepository.insertAppInfoNiceAdmin(appsRegRequest);

		// app secret his 테이블에 시크릿 추가
		AppsScrRequest appsScrRequest = new AppsScrRequest();
		appsScrRequest.setAppKey(appsRegRequest.getAppKey());
		appsScrRequest.setAppClientId(appsRegRequest.getAppClientId());
		appsScrRequest.setAppScr(appsRegRequest.getAppScr());
		appsScrRequest.setAppScrVldDttm(appsRegRequest.getAppSvcEnDt());
		appsScrRequest.setRegUser(appsRegRequest.getRegUser());
		appsScrRequest.setRegUserId(appsRegRequest.getRegUserId());
		adminRepository.insertAppNewScrHis(appsScrRequest);

		// PORTAL_APP_INFO 테이블에 앱 이동 후 상태 변경
		AppsRegRequest r = new AppsRegRequest();
		r.setAppKey(appsRegRequest.getAppKey());
		r.setAppStatCd("OK");
		r.setAppAplvStatCd("APLV");
		adminRepository.appStatChange(r);
	}

	/**
	 * 상품목록 조회
	 * @param request
	 * @return
	 */
	public BoPrdRsponse prdList(BoPrdRequest request) {
		List<BoPrdVO> list = boAppsRepository.selectPrdList(request);

		BoPrdRsponse resp = new BoPrdRsponse();
		resp.setList(list);
		return resp;
	}
}
