package com.hanafn.openapi.portal.admin.views.vo;

import java.util.List;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("apiTrxCommInfoWithStat")
public class ApiTrxCommInfoWithStatVO {
    private String seqNo;
    private String fileTrxDt;
    private String fileHfnCd;
    private String fileEntrCd;
    private String fileAppKey;
    private String fileHfnSvcCd;
    private String fileApiTrxCnt;
    private String regDttm;

    // JOIN 통하여 얻은 정보
    private String appNm;
    private String useorgNm;

    // 상세 정보
    List<ApiTrxCommDetailWithStatVO> apiTrxCommDetailWithStatList;

    // 상세 정보로부터 나온 집계 VALUE
    private String totApiTrxCnt;
    private String totGwErrorCnt;
    private String totApiErrorCnt;
    private String totSvcErrorCnt;
    private String totPlatformApiTrxCnt;
}
