package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("appBlackList")
public class AppBlackListVO {

	private String seqNo;
	private String appKey;
	private String useFl;
	private String regUser;
	private String modUser;
	private String cnlKey;
}
