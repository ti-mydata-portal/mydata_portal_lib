package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.List;

@Data
@Alias("tokenPolicy")
public class TokenPolicyVO {

	// 인증실패제어 - 공통
	private String hfnCd;
	private String useorgNm;
	private String userNm;
	private String useSetting;
	private String limitCount;
	private String failCount;
	private String gubun;
	private String regUser;
	private String regDttm;
	private String modUser;
	private String modDttm;

	// 인증실패제어 - 토큰발급
	private String appKey;
	private String appNm;
	private String clientId;

	// 인증실패제어 - 토큰검증
	private String userKey;
	private String entrCd;

}
