package com.hanafn.openapi.portal.admin.viewsv2.dto;

import lombok.Data;

@Data
public class CommonResponse {
	private String resultCode;
	private String message;

	public CommonResponse(String errorCode, String message) {
		this.resultCode = errorCode;
		this.message = message;
	}
}
