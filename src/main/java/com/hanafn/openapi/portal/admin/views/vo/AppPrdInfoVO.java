package com.hanafn.openapi.portal.admin.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("appPrdInfo")
public class AppPrdInfoVO {

	private String seqNo;
	private String appKey;
	private String prdId;
	private String prdCd;
	private String prdNm;
	private String prdCtgy;
	private int buyCnt;
	private String remaining;
	private String aplvDttm;
	private String regDttm;
	private String regUser;
	private String modDttm;
	private String modUser;
	private String prdCtgyNm;
	private String useStDt;
	private String useEndDt;
	private String useTeam;
	private String aplvYn;
	private String dispPrdNm;
	private int feeAmount; 
	private int restCnt;
	private int planAmt;
	private String planCd;
	private String term;
	private String useFl;
	private String limitYn;
}
