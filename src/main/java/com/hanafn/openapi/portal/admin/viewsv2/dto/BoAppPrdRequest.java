package com.hanafn.openapi.portal.admin.viewsv2.dto;

import com.hanafn.openapi.portal.admin.viewsv2.vo.BoAppPrdVO;
import lombok.Data;
import java.util.List;
@Data
public class BoAppPrdRequest {

    private String appKey;
    private String regUser;
    private String regUserId;
    private String prodUser;
    private String prodUserId;
    private List<BoAppPrdVO> prdList;
}
