package com.hanafn.openapi.portal.admin.viewsv2.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.vo.AppPrdInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoAppsRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.BoPrdRequest;
import com.hanafn.openapi.portal.admin.viewsv2.vo.BoAppPrdVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.BoAppsVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.BoPrdVO;

@Mapper
public interface BoAppsRepository {

	List<BoAppsVO> selectAppsList(BoAppsRequest appsRequest);
	List<BoAppPrdVO> selectAppPrdList(String appKey);
	List<BoPrdVO> selectPrdList(BoPrdRequest boPrdRequest);
	int countAppsList(BoAppsRequest appsRequest);
	AppPrdInfoVO selectAppPrdListBySvcCd(BoAppPrdVO boAppPrdVO);

	ProductVO getPrdIdByServiceCode(ProductVO vo);

	void updateAppPrdMappingInfo(ProductVO vo);
	void updateAppPrdStatus(ProductVO vo);
	void updateAppPrdEndDt(BoAppPrdVO vo);
}