package com.hanafn.openapi.portal.admin.views.dto;

import lombok.Data;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.ApiLogVO;

@Data
public class ApiLogRsponsePaging {
    private int pageIdx;
    private int pageSize;
    
    private int totCnt;
    private int selCnt;
    private List<ApiLogVO> list;
}
