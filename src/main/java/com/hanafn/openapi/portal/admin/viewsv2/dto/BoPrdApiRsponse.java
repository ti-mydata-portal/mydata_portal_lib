package com.hanafn.openapi.portal.admin.viewsv2.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.viewsv2.vo.BoApiVO;

import lombok.Data;

@Data
public class BoPrdApiRsponse {

    private String resultCode = "0000";
    private String message = "정상처리되었습니다.";

    private List<BoApiVO> list;
}
