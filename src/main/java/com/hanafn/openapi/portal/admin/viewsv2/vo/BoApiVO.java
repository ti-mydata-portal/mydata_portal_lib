package com.hanafn.openapi.portal.admin.viewsv2.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("boapi")
public class BoApiVO {
	private String apiId;
	private String apiNm;
	private String ctgrCd;
	private String apiVer;
	private String apiUri;
	private String apiUrl;
	private String apiMthd;
	private String apiCtnt;
	private String lgcyCd;
	private String apiProcUrl;
}
