package com.hanafn.openapi.portal.admin.views.dto;

import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.AppApiInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppBlackListVO;
import com.hanafn.openapi.portal.admin.views.vo.AppCnlInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppPrdInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppReadInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.PolicyVO;
import com.hanafn.openapi.portal.admin.views.vo.QnaVO;
import com.hanafn.openapi.portal.admin.views.vo.TokenPolicyVO;

import lombok.Data;

@Data
public class AdminResponse {
	
	@Data
    public static class QnaResponse {
        private List<QnaVO> qnaList;
        private QnaVO qna;

        private int totCnt;
        private int selCnt;
        private int pageIdx;
        private int pageSize;
    }
	
	@Data
	public static class CommonApiResponse {
	    private String requestApiUrl;
	    private String statCd;
	    private HttpStatus apiResultStatCd;
	    private HttpEntity<String> requestEntity;
	    private ResponseEntity<String> responseEntity;
	    private Map<String,Object> responseData;
	}
	
	@Data
	public static class PolicyResponse {
	    private int pageIdx;
	    private int pageSize;
	    private int totCnt;

	    private List<PolicyVO> redisList;
	    private List<TokenPolicyVO> list;
	    private List<TokenPolicyVO> useorgList;
	}

	@Data
	public static class AppsResponse {

	    private String appKey;
	    private String appClientId;
	    private String newAppClientId;
	    private String appNm;
	    private String appStatCd;
	    private String appAplvStatCd;
	    private String appScr;
	    private String newAppScr;
	    private String appSvcStDt;
	    private String appSvcEnDt;
	    private String appCtnt;
	    private String userKey;
	    private String useorgNm;
	    private String termEtdYn;
	    private String dlDttm;
	    private String appScrReisuYn;
	    private String regDttm;
	    private String regUser;
	    private String modDttm;
	    private String modUser;
	    private String appScrVldDttm;
	    private String regUserNm;
	    private String modUserNm;
	    private String appSvcNm;
	    private String appSvcUrl;
	    private String prdKind;
	    private String hfnCd;
	    private String userNm;
	    private String appMemo;
	    private String appAnyCnlYn;
	    
	    private String copRegNo;
	    private String copNm;
	    private String ownNm;
	    private String copAddr1;
	    private String copAddr2;
	    private String copAddrNo;
	    private String bizType;
	    private String bizCdtn;
	    private String dirNm;
	    private String dirTelNo1;
	    private String dirTelNo2;
	    private String dirEmail;
	    private String dirDomain;
	    
	    private String appDiv;
	    
	    private String managerNm;
	    private String managerId;

	    private List<AppCnlInfoVO> cnlKeyList;  // 앱 채널 정보  //appCnlInfo
	    private List<AppApiInfoVO> appApiInfo;  // 앱 API 정보
	    private List<AppPrdInfoVO> appPrdInfo;  // 앱 API 정보
	    private List<ApiVO> apiList;            // 등록된 앱 API의 API정보+API카테고리정보
	    private List<AppReadInfoVO> appReadInfo;  // 조회자 정보 조회
	    private List<AppBlackListVO> blockKeyList;
	}
	
	@Data
    public static class UserDupCheckResponse{
        private String userIdDupYn;
        private String hfnIdDupYn;
        private String userEmailDupYn;
    }
}
