package com.hanafn.openapi.portal.enums;

import org.apache.commons.lang3.StringUtils;
import reactor.util.annotation.Nullable;

public enum etcEnum {

    // 앱상태
    UNUSED("UNUSED","", "대기"),
    WAIT("WAIT","", "승인중"),
    OK("OK","", "운영"),
    CLOSE("CLOSE","", "중지"),
    EXPIRE("EXPIRE","", "기간만료"),
    DEL("DEL","", "삭제"),

    // 앱승인상태
    REQ("REQ","", "승인요청"),
    APLV("APLV","", "승인완료"),
    CANCEL("CANCEL","", "등록취소"),
    REJECT("REJECT", "", "반려"),

    // 상품종류
    PRD10("10", "선납", "선결제"),
    PRD11("11", "선납", "선결제백오피스연계"),
    PRD20("20", "후납", "후납포털승인"),
    PRD21("21", "후납", "후납백오피스승인");
    //PRD30("30", "", "백오피스생성(폐쇄형)");

    private String value;
    private String name;
    private String kor;

    etcEnum(String value, String name, String kor ) {
        this.name = name;
        this.value = value;
        this.kor = kor;
    }

    /**
     * Return the String value of HfnCompanyInfo
     */
    public String value() {
        return this.value;
    }
    public String getName() { return this.name; }
    public String getKor() { return this.kor; }

    @Nullable
    public static etcEnum resolve(String name) {
        for (etcEnum roleInfo : etcEnum.values()) {
            if (StringUtils.equals(name, roleInfo.getName())) {
                return roleInfo;
            }
        }
        return null;
    }

    @Nullable
    public static etcEnum resolveByCd (String value) {
        for (etcEnum roleInfo : etcEnum.values()) {
            if (StringUtils.equals(value, roleInfo.value())) {
                return roleInfo;
            }
        }
        return null;
    }
}
